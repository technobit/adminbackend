var data_grid = "";
$(function(){
    var overlayHtml = getLoadingOverlay();
    $('#box-list-buku').append(overlayHtml);

    data_grid = $('#tableList').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
          "processing": true,
           ajax : {
           url : global_url+"invoice/getDataInvoice/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intPublisherID = $('#intPublisherID').val();
                d.intStatusID = $('#intStatusID').val();
            }
          }
    });
    data_grid.on('draw.dt',function(){
        $('#loading-bar').remove();
    });

    $('#btnInvoiceFilter').click(function(){
        data_grid.ajax.reload();
    });

});


function updateStatusInvoice(intPublisherID , intYear , intMonth){
    $.ajax({
        url : global_url+"invoice/updateStatusInvoice/",
        type : "POST",
        dataType : "html",
        data : "intPublisherID="+intPublisherID+"&intYear="+intYear+"&intMonth="+intMonth,
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status  = data['status'];
            var message = data['message'];
            if(status==true){
                bootbox.alert({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    className : "modal-success",
                    callback : function(){
                        data_grid.ajax.reload();
                    }
                });
            }else{
                bootbox.alert({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    className : "modal-danger",
                });
                
            }
        }
    });
}