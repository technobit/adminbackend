$(function(){
    $('#btnFileExcel').click(function(){
        $('#fileExcel').click();
    });

    $('#fileExcel').change(function(){
        var names = $('#fileExcel').prop("files")[0].name;
        $('#fileExcelName').val(names);
        $('#form-import-buku').valid();
        ///uploadFileToConvert($('#ntxtFrontCover') , 'ntxtFrontCoverTxtDir');
    });

    $('#form-import-buku').validate({
        ignore : "",
        rules : {
            "fileExcel" : {
                required : true,
                extension : "xls|xlsx",
            },
        } 
    });

    $('#btnStartImport').click(function(){
        if($('#form-import-buku').valid()){
            uploadFile();
        }
    });
    
});

function uploadFile(){
    var loading = getLoadingOverlay();
    $('#box-list-buku').append(loading);
    var file_data = $('#fileExcel').prop("files")[0];
    var form_data = new FormData();  
    form_data.append("fileExcel", file_data);
    form_data.append("intPublisherID",$('#intPublisherID').val());
    form_data.append("txtPublisherID",$('#intPublisherID').text());
    $.ajax({
        url : global_url+"master-data/import-buku/upload-import-data/",
        type : "POST",
        contentType: false,
        processData: false,
        cache: true,
        data : form_data,
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['html'];
            if(status==true){
                $('#listBookData').html(message);
            }else{
                alertPopUp(status,message,"");
            }
            $('#loading-bar').remove();
        }
    });
}