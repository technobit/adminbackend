var base_url_penerbit = global_url+'master-data/kategori/';
var intCatalogId = "";
var intCategoryId = "";
var intStatusId = "";
var txtBookTitle = "";
var data_grid = "";
$(function(){
    data_grid = $('#tableCategory').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
          "processing" : true,
           ajax : {
           url : global_url+"master-data/kategori/get-data/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intCatalogID = $('#intCatalogID').val();
            }
          }
    });
    
    $('#btnAddKategori').click(function(){
        $.ajax({
            url : global_url + 'master-data/kategori/get-form',
            success : function(response) {
               var data = jQuery.parseJSON(response);
               bootbox.dialog({
                title : data['title'],
                message : data['form'],
                size : 'large'
              });
            }
        }); 
    });

});

function refreshData(){
    data_grid.ajax.reload();
}

function editData(intCatalogID , intCategoryID){
    $.ajax({
            url : global_url + 'master-data/kategori/get-form/'+intCatalogID+'/'+intCategoryID,
            success : function(response) {
               var data = jQuery.parseJSON(response);
               bootbox.dialog({
                title : data['title'],
                message : data['form'],
                size : 'large'
              });
            }
        });
}

function hapusData(intCatalogID , intCategoryID, txtCatalog){
    var messagePublisher = "Apakah Anda akan Menghapus Kategori "+txtCatalog+"?";
    bootbox.confirm({
        size : 'small',
        title : "Peringatan",
        message : messagePublisher,
        callback : function(response){
            if(response==true){
                $.ajax({
                    url : global_url+"master-data/kategori/hapus",
                    type : "POST",
                    data : "intCatalogID="+intCatalogID + "&intCategoryID="+intCategoryID,
                    dataType : "html",
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status , message , "");
                        data_grid.ajax.reload();    
                    }
                });
            }
        } 
    });

    
}