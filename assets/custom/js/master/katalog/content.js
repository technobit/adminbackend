var base_url_penerbit = global_url+'penerbit/';
var intCatalogId = "";
var intCategoryId = "";
var intStatusId = "";
var txtBookTitle = "";
var data_grid = "";
$(function(){
    data_grid = $('#tableCatalog').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
          "processing" : true,
           ajax : {
           url : global_url+"master-data/katalog/get-katalog/",
           type : "POST",
           dataSrc: 'data',
          }
    });
    
    $('#btnAddKatalog').click(function(){
        $.ajax({
            url : global_url + 'master-data/katalog/get-form-katalog',
            success : function(response) {
               var data = jQuery.parseJSON(response);
               bootbox.dialog({
                title : data['title'],
                message : data['form'],
                size : 'large'
              });
            }
        }); 
    });

});

function refreshData(){
    data_grid.ajax.reload();
}

function editCatalog(intCatalogID){
    $.ajax({
            url : global_url + 'master-data/katalog/get-form-katalog/'+intCatalogID,
            success : function(response) {
               var data = jQuery.parseJSON(response);
               bootbox.dialog({
                title : data['title'],
                message : data['form'],
                size : 'large'
              });
            }
        });
}

function hapusCatalog(intCatalogID , txtCatalog){
    var messagePublisher = "Apakah Anda akan Menghapus katalog "+txtCatalog+"?";
    bootbox.confirm({
        size : 'small',
        title : "Peringatan",
        message : messagePublisher,
        callback : function(response){
            if(response==true){
                $.ajax({
                    url : global_url+"master-data/katalog/hapus-katalog",
                    type : "POST",
                    data : "intCatalogID="+intCatalogID,
                    dataType : "html",
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status , message , "");
                        data_grid.ajax.reload();    
                    }
                });
            }
        } 
    });

    
}