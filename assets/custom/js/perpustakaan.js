var base_url_perpustakaan = global_url+'perpustakaan/';
var loading_html = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
var success_html = '<div class="overlay"><i class="fa fa-check"></i></div>';

$(document).ready(function() {
    
    $('#submit_perpustakaan').click(function(e){
        e.preventDefault();
        
        if ($('#form-insert-perpustakaan').valid()) {
            //code
            saveDataPerpustakaan();
        }
    });
    
    $('#form-insert-perpustakaan').validate({
        ignore : "",
        rules : {
                     txtLibraryName : {
                            required : true,
                     },
                     txtLibraryAddress : {
                            required : true,
                     },
                     txtLibraryEmail : {
                            required : true,
                            email : true
                     },
                     txtLibraryPhone : {
                            required : true,
                     },
                     curBuy1Token : {
                        required : true,
                     }
              }
    });
    
    
     
    
} );

function getDataPerpustakaan() {
    //code
    var loading_html = '<div id="overlay-data-perpus" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-result-perpus').append(loading_html);
    $.ajax({
              url : base_url_perpustakaan+"get_data_perpustakaan/",
              type : "POST",
              data : $('#form-filter-perpus').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list-data-perpustakaan').html(html);
                            
                   }else{
                            $('#list-data-perpustakaan').html("");
                   }
                   
              }
       });
    $('#overlay-data-perpus').remove();
}

function getDetailTipe(id) {
    //code
    $.ajax({
        url : base_url_perpustakaan+"detail-tipe/",
        type : "POST",
        data : "id="+id,
        dataType : "html",
        success: function msg(res){
             var data = jQuery.parseJSON(res);
             $('#id-tipe-kategori').val(data[0].intLibraryType);
             $('#tipe-kategori').val(data[0].txtLibraryType);
        }
    });
}

function saveDataPerpustakaan() {
    //code
    $('#box-bagi-hasil').append(loading_html);
    $('#box-info-perpus').append(loading_html);
    $('#box-token-berlangganan').append(loading_html);
    $.ajax({
              url : base_url_perpustakaan+"save_perpustakaan/",
              type : "POST",
              data : $('#form-insert-perpustakaan').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   if (status==true) {
                     //code
                            var redirectUrl = base_url_perpustakaan;
                            $('#msg-error-post').html("<div class='alert alert-success'>"+message+"</div>")
                            window.location.href = redirectUrl;
                   }else{
                            $('#msg-error-post').html("<div class='alert alert-danger'>"+message+"</div>")
                   }
                   
              }
       });
}

function hapusTipe(id) {
    var r = confirm("Apakah Anda Akan Menghapus Data Ini??");
    if (r==true) {
        //code
        window.location = base_url_perpustakaan+'delete-tipe/'+id;
    }

}

function changeRevShare() {
    //code
    var bitRevShare = $('#bitRevShare').val();
    if (bitRevShare==0) {
        //code
        $('#decRevBuqu').attr("disabled" ,"disabled");
        $('#txtPartnerName1').attr("disabled" ,"disabled");
        $('#txtPartnerName2').attr("disabled" ,"disabled");
        $('#decRevPartner1').attr("disabled" ,"disabled");
        $('#decRevPartner2').attr("disabled" ,"disabled");
    }else{
        $('#decRevBuqu').removeAttr("disabled");
        $('#txtPartnerName1').removeAttr("disabled");
        $('#txtPartnerName2').removeAttr("disabled");
        $('#decRevPartner1').removeAttr("disabled");
        $('#decRevPartner2').removeAttr("disabled");
    }
}
