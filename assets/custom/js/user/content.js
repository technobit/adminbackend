var base_url_user = global_url+'user/';

function nonaktif(email, userid){
	document.getElementById("nonaktifTxtEmail").innerHTML = email;
	document.getElementById("nonaktifUserID").innerHTML = userid;
	//alert(userid);
  	$('#nonaktif').modal('show');
}
function aktif(email, userid){
	document.getElementById("aktifTxtEmail").innerHTML = email;
	document.getElementById("aktifUserID").innerHTML = userid;
  	$('#aktif').modal('show');
}function ubah(email, username, userid){
	document.getElementById("ubahTxtUserName").value = username;
	document.getElementById("ubahTxtEmail").value = email;
	document.getElementById("ubahUserID").innerHTML = userid;
  	$('#ubah').modal('show');
}
function hapus(email, userid){
	document.getElementById("hapusTxtEmail").innerHTML = email;
	document.getElementById("hapusUserID").innerHTML = userid;
  	$('#hapus').modal('show');
}function ubahPassword(email, username, userid){
	document.getElementById("ubahPassTxtUserName").value = username;
	document.getElementById("ubahPassTxtEmail").value = email;
	document.getElementById("ubahPassUserID").innerHTML = userid;
  	$('#ubahPass').modal('show');
}
var data_grid = "";
$(function(){
	data_grid = $('#tableUserList').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
		  "processing" : true,
           ajax : {
           url : global_url+"user/getUserList/",
           type : "POST",
           dataSrc: 'data',
           }
    });

	$('#btnAddUser').click(function(){
		frmUser("" , "insert");
	});

	$('#btnRefreshUser').click(function(){
		refreshUser();
	});
});

function refreshUser(){
	data_grid.ajax.reload();
}

function frmUser(intBuquStaffID , mode){
	$.ajax({
		url : base_url_user+"getFormUser/",
		type : "POST",
		data : "intBuquStaffID="+intBuquStaffID+"&mode="+mode,
		success : function(response){
			var data = jQuery.parseJSON(response);
			var title = data['title'];
			var html = data['htmlRes'];
			bootbox.dialog({
				'title' : title,
				 message : html,
			})
		}
	})
}

function deletUser(intBuquStaffID){
	bootbox.confirm({
		title : "Peringatan",
		message : "Apakah Data Akan Di Hapus?",
		callback : function(result){
			if(result==true){
				$.ajax({
					url : base_url_user+"deleteDataUser/",
					type : "POST",
					data : "intBuquStaffID="+intBuquStaffID,
					success : function(response){
						var data = jQuery.parseJSON(response);
						var status = data['status'];
						var message = data['message'];	
						alertPopUp(status, message , "");
						refreshUser();	
					}
				});
			}
		}
	});
}

function nonActiveUser(intBuquStaffID){
	bootbox.confirm({
		title : "Peringatan",
		message : "Apakah User Akan Di Non Aktifkan?",
		callback : function(result){
			if(result==true){
				$.ajax({
					url : base_url_user+"nonActivateDataUser/",
					type : "POST",
					data : "intBuquStaffID="+intBuquStaffID,
					success : function(response){
						var data = jQuery.parseJSON(response);
						var status = data['status'];
						var message = data['message'];	
						alertPopUp(status, message , "");
						refreshUser();	
					}
				});
			}
		}
	});
}

function nonaktifUser(){
    var nonaktifUserID = $('#nonaktifUserID').html();
	$.ajax({
		url : base_url_user+"nonactiveUser/",
		type : "POST",
		data : "nonaktifUserID="+nonaktifUserID,
		dataType : "html", 
		success : function msg(res){
			var data = jQuery.parseJSON(res);
			dataAlert = data['0'].bitSuccess;
			if (dataAlert == 1){
				msgAlert = 'Pengguna berhasil dinonaktifkan';
				document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#success').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
			}else{
				msgAlert = 'Pengguna gagal dinonaktifkan';
				document.getElementById("alertMsgFailed").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#failed').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
				
			}
		}            
	});
}

function aktifUser(){
    var aktifUserID = $('#aktifUserID').html();
	$.ajax({
		url : base_url_user+"activeUser/",
		type : "POST",
		data : "aktifUserID="+aktifUserID,
		dataType : "html", 
		success : function msg(res){
			var data = jQuery.parseJSON(res);
			dataAlert = data['0'].bitSuccess;
			if (dataAlert == 1){
				msgAlert = 'Pengguna berhasil diaktifkan';
				document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#success').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
			}else{
				msgAlert = 'Pengguna gagal diaktifkan';
				document.getElementById("alertMsgFailed").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#failed').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
				
			}
		}            
	});
}

function addUser(){
    var addTxtUserName = $('#addTxtUserName').val();
    var addTxtEmail = $('#addTxtEmail').val();
    var addTxtPassword = $('#addTxtPassword').val();
    var addTxtPassword2 = $('#addTxtPassword2').val();
    if(addTxtPassword == addTxtPassword2){
        $.ajax({
            url : base_url_user+"addUser/",
            type : "POST",
            data : "addTxtUserName="+addTxtUserName+"&addTxtEmail="+addTxtEmail+"&addTxtPassword="+addTxtPassword,
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
				dataAlert = data['0'].bitSuccess;
				if (dataAlert == 1){
					msgAlert = 'Pengguna berhasil ditambahkan';
					document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
					$('#tambah').modal('hide');
					$('#success').modal('show');
						setTimeout(function () {
							location.reload();
							}, 1000);
				}else{
					msgAlert = 'Pengguna gagal ditambahkan';
					document.getElementById("alertMsgFailed").innerHTML = msgAlert;
					$('#tambah').modal('hide');
					$('#failed').modal('show');
						setTimeout(function () {
							location.reload();
							}, 1000);
				}
			}            
        });
    }else{
		msgAlert = 'Password tidak sama';
		document.getElementById("alertMsgFailed").innerHTML = msgAlert;
		$('#failed').modal('show');
	}
}

function removeUser(){
    var hapusUserID = $('#hapusUserID').html();
	$.ajax({
		url : base_url_user+"removeUser/",
		type : "POST",
		data : "hapusUserID="+hapusUserID,
		dataType : "html", 
		success : function msg(res){
			var data = jQuery.parseJSON(res);
			dataAlert = data['0'].bitSuccess;
			if (dataAlert == 1){
				msgAlert = 'Pengguna berhasil dihapus';
				document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#success').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
			}else{
				msgAlert = 'Pengguna gagal dihapus';
				document.getElementById("alertMsgFailed").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#failed').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
				
			}
		}            
	});
}
function editUser(){
    var ubahUserID = $('#ubahUserID').html();
    var ubahTxtUserName = $('#ubahTxtUserName').val();
    var ubahTxtEmail = $('#ubahTxtEmail').val();
	$.ajax({
		url : base_url_user+"editUser/",
		type : "POST",
        data : "ubahUserID="+ubahUserID+"&ubahTxtUserName="+ubahTxtUserName+"&ubahTxtEmail="+ubahTxtEmail,
		dataType : "html", 
		success : function msg(res){
			var data = jQuery.parseJSON(res);
			dataAlert = data['0'].bitSuccess;
			if (dataAlert == 1){
				msgAlert = 'Data pengguna berhasil diubah';
				document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#success').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
			}else{
				msgAlert = 'Data pengguna gagal diubah';
				document.getElementById("alertMsgFailed").innerHTML = msgAlert;
				$('#hapus').modal('hide');
				$('#failed').modal('show');
					setTimeout(function () {
						location.reload();
						}, 1000);
				
			}
		}            
	});
}

function editPass(){
    var ubahPassUserID = $('#ubahPassUserID').html();
    var ubahPassTxtEmail = $('#ubahPassTxtEmail').val();
    var txtPasswordOld = $('#txtPasswordOld').val();
    var txtPasswordNew = $('#txtPasswordNew').val();
    var txtPasswordNew2 = $('#txtPasswordNew2').val();
    if(txtPasswordNew == txtPasswordNew2){
        $.ajax({
            url : base_url_user+"editPass/",
            type : "POST",
            data : "ubahPassUserID="+ubahPassUserID+"&ubahPassTxtEmail="+ubahPassTxtEmail+"&txtPasswordOld="+txtPasswordOld+"&txtPasswordNew="+txtPasswordNew,
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
				dataAlert = data['0'].bitSuccess;
				if (dataAlert == 1){
					msgAlert = 'Password pengguna berhasil diubah';
					document.getElementById("alertMsgSuccess").innerHTML = msgAlert;
					$('#tambah').modal('hide');
					$('#success').modal('show');
						setTimeout(function () {
							location.reload();
							}, 1000);
				}else{
					msgAlert = 'Password pengguna gagal diubah';
					document.getElementById("alertMsgFailed").innerHTML = msgAlert;
					$('#tambah').modal('hide');
					$('#failed').modal('show');
						setTimeout(function () {
							location.reload();
							}, 1000);
				}
			}            
        });
    }else{
		msgAlert = 'Password tidak sama';
		document.getElementById("alertMsgFailed").innerHTML = msgAlert;
		$('#failed').modal('show');
	}
}
