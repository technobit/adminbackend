function alertMessage(message, success) {
    //code
    if (success==true) {
        //code
        var html = "<div class='alert alert-success alert-dismissable'><i class='fa fa-check'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+message+"</div>";
    }else{
        var html = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+message+"</div>";
    }
    return html;
}

function alertPopUp(status,message,urlreturn){
    var classDiv = "modal-success";
    if(!status){
        classDiv = "modal-danger";
    }

    bootbox.alert({
        size : "small",
        title : "Peringatan",
        message : message,
        className : classDiv,
        callback : function(){
            if(urlreturn!="" && status==true){
                window.location = urlreturn;
            }
            
        }
    });
}

function getLoadingOverlay(txtIdLoading){
    if(txtIdLoading==""){
        txtIdLoading = "loading-bar";
    }
    var html = '<div class="overlay" id="loading-bar"><i class="fa fa-refresh fa-spin"></i></div>';
    return html;

}