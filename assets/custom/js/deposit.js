var base_url_deposit = global_url+'deposit/';

$(document).ready(function(){
$('#deposit-perpustakaan').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
    });
});

function getDataDeposit() {
    
    //code
    $.ajax({
              url : base_url_deposit+"get-data-deposit/",
              type : "POST",
              data : $('#frm-deposit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list-deposit-perpustakaan').html(html);
                            
                   }else{
                            $('#list-deposit-perpustakaan').html("");
                   }
                   
              }
       });
    
}