var base_url_laporan = global_url+'laporan/';

var data_grid = "";
$(document).ready(function(){
     data_grid = $('#tableBookStatus').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
          "ajax" : {
            url : base_url_laporan+"getBookStatusReport/",
            type : "POST",
            dataSrc: 'data',
            data : function(d){
                d.intPublisherID = $('#intPublisherID').val();
            }
          }
    });

    $('#btnRekapStatus').click(function(){
        data_grid.ajax.reload();
    });

    $('#btnExportExcel').click(function(){
        downloadLink();
    });

});

function downloadLink(){
    var downloadLink = base_url_laporan+"DownloadDataExcel/1/";
    var intPenerbit = $('#intPublisherID').val();
    window.location = downloadLink+intPenerbit+"/";
}