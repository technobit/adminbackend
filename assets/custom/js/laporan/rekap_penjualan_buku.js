var base_url_laporan = global_url+'laporan/';

var data_grid = "";
$(document).ready(function(){
     data_grid = $('#tableBookStatus').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
          "ajax" : {
            url : base_url_laporan+"getRecapBookSold/",
            type : "POST",
            dataSrc: 'data',
            data : function(d){
                d.intPublisherID = $('#intPublisherID').val();
                d.dtStart = $('#dtStart').val();
                d.dtEnd = $('#dtEnd').val();
            }
          }
    });

    $('#btnRekapStatus').click(function(){
        data_grid.ajax.reload();
    });

    $('#btnExportExcel').click(function(){
        downloadLink();
    });

    $('#dtStart').datepicker({
        autoclose: true,
        format : 'yyyy-mm-dd'
    });
    $('#dtEnd').datepicker({
        autoclose: true,
        format : 'yyyy-mm-dd'
    });

});

function downloadLink(){
    var downloadLink = base_url_laporan+"DownloadDataExcel/3/";
    var intPenerbit = $('#intPublisherID').val();
    var dtStart = $('#dtStart').val();
    var dtEnd = $('#dtEnd').val();

    if(intPenerbit!=0){
        window.location = downloadLink+intPenerbit+"/0/"+dtStart+"/"+dtEnd;
    }
}


