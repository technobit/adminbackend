var link_penerbit = global_url+"penerbit/";

$(document).ready(function() {
    $('#btnUpdatePenerbit').click(function(e){
       e.preventDefault();
       if($('#frm-detail-penerbit').valid()){
           saveUpdatePenerbit();
       } 
    });

    $('#frm-detail-penerbit').validate({
        ignore : "",
        rules : {
            txtPublisherGroup : {
                required : true,
            },
            txtPublisherName : {
                required : true,
            },
            txtEmail : {
                required : true,
            },
            txtUserName : {
                required : true,
            },
            txtEmailAdmin : {
                required : true,
            },
            txtPassword : {
                required : true,
            },
            txtReTypePassword : {
                equalTo : '#txtPassword',
            }
        }
    })
    $('#txtAnggota3').click(function(){
        if($('#txtAnggota3').is(':checked')){
            $('#txtOtherAnggota').removeAttr("readonly");
        }else{
            $('#txtOtherAnggota').attr("readonly","true");
        }
    });
});

function saveUpdatePenerbit(){
    var loadingOverlay = getLoadingOverlay();
    $('#box-form-input').append(loadingOverlay);
    $('#box-rekomendasi-bank').append(loadingOverlay);
    $('#box-lain-bank').append(loadingOverlay);
    $('#box-save').append(loadingOverlay);
    $.ajax({
              url : link_penerbit+"simpan-data/",
              type : "POST",
              data : $('#frm-detail-penerbit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   alertPopUp(status , message , link_penerbit);
                   $('.overlay').remove();
              }
       });
    
}