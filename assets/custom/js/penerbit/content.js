var base_url_penerbit = global_url+'penerbit/';
var intCatalogId = "";
var intCategoryId = "";
var intStatusId = "";
var txtBookTitle = "";
var data_grid = "";
$(function(){
    var overlayHtml = getLoadingOverlay();
    $('#box-list-publisher').append(overlayHtml);
    data_grid = $('#tablePublisherList').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
           ajax : {
           url : global_url+"penerbit/getPublisherList/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.txtPublisherName = $('#txtPublisherName').val();
            }
          }
    });
    
    data_grid.on('draw.dt',function(){
        $('#loading-bar').remove();
    });
    
    $('#btnSearchPenerbit').click(function(){
        $('#box-list-publisher').append(overlayHtml);
        data_grid.ajax.reload();
    });

});

function getCategory(){
    var catalogId = $('#intCatalogID').val();
    if(catalogId!=""){
        $.ajax({
            url : global_url+"buku/get-list-category/",
            type : "POST",
            data : "id="+catalogId+"&mode=index",
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
                $('#intCategoryID').html(data);
            }            
        });
    }
}

function updateStatusPublisher(status,intPublisherID){
    var messagePublisher = "Apakah Anda akan menonaktifkan penerbit ini?";
    if(status==0) {
        var messagePublisher = "Apakah Anda akan mengeaktifkan penerbit ini?";
    }
    bootbox.confirm({
        size : 'small',
        message : messagePublisher,
        callback : function(response){
            if(response==true){
                $.ajax({
                    url : global_url+"penerbit/updateStatusPublisher/",
                    type : "POST",
                    data : "intPublisherID="+intPublisherID+"&txtStatusNow="+status,
                    dataType : "html",
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        var urlReturn = global_url+"penerbit/";
                        alertPopUp(status , message , "");
                        data_grid.ajax.reload();    
                    }
                });
            }
        } 
    });

    
}