var link_penerbit = global_url+"penerbit/";

$(document).ready(function() {
    $('#btnSaveRegistration').click(function(e){
       e.preventDefault();
       if($('#frm-registrasi-penerbit').valid()){
           saveRegisterPenerbit();
       } 
    });
    $('#frm-registrasi-penerbit').validate({
        ignore : "",
        rules : {
            txtPublisherGroup : {
                required : true,
            },
            txtPublisherName : {
                required : true,
            },
            txtUserName : {
                required : true,
            },
            txtEmail : {
                required : true,
            },
            txtPassword : {
                required : true,
            },
        }
    })
});

function saveRegisterPenerbit(){
    $.ajax({
              url : link_penerbit+"simpan-data/",
              type : "POST",
              data : $('#frm-registrasi-penerbit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   var htmlRes = alertMessage(message,status);
                   $('#result-registration-container').html(htmlRes);
                   window.location = global_url+"login/";
              }
       });
    
}

function alertMessage(message, success) {
    //code
    if (success==true) {
        //code
        var html = "<div class='alert alert-success alert-dismissable'><i class='fa fa-check'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+message+"</div>";
    }else{
        var html = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"+message+"</div>";
    }
    return html;
}