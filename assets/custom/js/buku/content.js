var base_url_buku = global_url+'buku/';
var intCatalogId = "";
var intCategoryId = "";
var intStatusId = "";
var txtBookTitle = "";
var data_grid = "";
$(function(){
    $('#intCatalogID').change(function(e){
        //console.log(e.target.value);
        getCategory();
    });

    var overlayHtml = getLoadingOverlay();
    $('#box-list-buku').append(overlayHtml);

    data_grid = $('#tableBookList').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
          "processing": true,
           ajax : {
           url : base_url_buku+"getBookList/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intPublisherID = $('#intPublisherID').val();
                d.intCatalogID = $('#intCatalogID').val();
                d.intCategoryID = $('#intCategoryID').val();
                d.intStatusID = $('#intStatusID').val();
                d.txtBookTitle = $('#txtBookTitle').val();
            }
          }
    });
    data_grid.on('draw.dt',function(){
        $('#loading-bar').remove();
    });

    $('#btnSearchBuku').click(function(){
        data_grid.ajax.reload();
    });

});

function getCategory(){
    var catalogId = $('#intCatalogID').val();
    if(catalogId!=""){
        $.ajax({
            url : global_url+"buku/get-list-category/",
            type : "POST",
            data : "id="+catalogId+"&mode=index",
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
                $('#intCategoryID').html(data);
            }            
        });
    }
}

function addBuku() {
    //code
        var file_front = $("#ntxtFrontCover").prop("files")[0];
        var file_back = $("#ntxtBackCover").prop("files")[0]; 
        var form_data = new FormData();               
        form_data.append("frontCover", file_data);
        form_data.append("frontBack", file_back);  
	    $.ajax({
              url : base_url_buku+"simpan-data/",
              type : "POST",
              data : form_data + $('#'),
              //dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
              }
       }); 
}

function uploadPDF(mode,intPublisherID,intBookPublisherID){
    var urlUploadBuku = global_url+"buku/form-upload-buku/"+mode+"/"+intPublisherID+"/"+intBookPublisherID;
    var loading = getLoadingOverlay();
    $('#box-list-buku').append(loading);
    $.ajax({
        url : global_url+"buku/validasi-upload/",
        type : "POST",
        data : "mode="+mode+"&intPublisherID="+intPublisherID+"&intBookPublisherID="+intBookPublisherID,
        dataType : "html",
        success : function msg(response){
            var data = jQuery.parseJSON(response);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                window.location = urlUploadBuku;
            }else{
                bootbox.confirm({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    callback : function(result){
                        if(result==true){
                            window.location = urlUploadBuku;
                        }
                    }
                });
            }
            $('#loading-bar').remove();
        }

    });
}

function updateFiling(mode,intPublisherID,intBookPublisherID){
    $.ajax({
        url : global_url+"buku/pengajuan-review-konversi/",
        type : "POST",
        data : "mode="+mode+"&intPublisherID="+intPublisherID+"&intBookPublisherID="+intBookPublisherID,
        dataType : "html",
        success : function msg(response){
            var data = jQuery.parseJSON(response);
            console.log(data);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status,message,"");
            data_grid.ajax.reload();
            ///var status = data['status'];
            
        }
    });
}

function deleteBooksData(intPublisherID,intBookPublisherID){
    bootbox.confirm({
        size : 'small',
        message : "Apakah Anda Akan Menghapus Data?",
        callback : function(response){
            if(response==true){
                $.ajax({
                    url : global_url+"buku/hapus-data-buku/",
                    type : "POST",
                    data : "intPublisherID="+intPublisherID+"&intBookPublisherID="+intBookPublisherID,
                    dataType : "html",
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        var urlReturn = global_url+"buku/";
                        alertPopUp(status , message , "");
                        data_grid.ajax.reload();    
                    }
                });
            }
        } 
    });
}

function checkDownloadBuku(txtTypeBuku, intPublisherID, intPublisherBookID){
    $.ajax({
            url : global_url+"download/checkDataBuku/",
            type : "POST",
            data : "txtTypeBuku="+txtTypeBuku+"&intPublisherID="+intPublisherID+"&intPublisherBookID="+intPublisherBookID,
            dataType : "html",
            success : function msg(response){
                var data = jQuery.parseJSON(response);
                var status = data['status'];
                var message = data['message'];
                if(status==false){
                    alertPopUp(status , message , "");
                }else{
                    window.location = global_url+"buku/download-buku/"+txtTypeBuku+"/"+intPublisherID+"/"+intPublisherBookID+"/"; 
                }
            }
    });
}