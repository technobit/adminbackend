$(function(){
    $('#btnUploadFile').click(function(){
        $('#filePDF').click();
    });

    $('#filePDF').change(function(){
        var names = $('#filePDF').prop("files")[0].name;
        ///uploadFileToConvert($('#ntxtFrontCover') , 'ntxtFrontCoverTxtDir');
        uploadFileToTemporary();
    });
});

function uploadFileToTemporary(){
    $('#progress-container').css("display" , "block");
    $('#btnUploadFile').attr("disabled" , "disabled");
    $('#btnBack').attr("disabled" , "disabled");
    var file_data = $('#filePDF').prop("files")[0];
    var form_data = new FormData();  
    form_data.append("filePdf", file_data);
    form_data.append("intBooksPublisherID",$('input[name=intBooksPublisherID]').val());
    form_data.append("txtBooksTitle",$('input[name=txtBooksTitle]').val());
    form_data.append("txtModeForm" , $('input[name=txtModeForm]').val());
    $.ajax({
        url : global_url+"buku/upload-buku-temp/",
        type : "POST",
        contentType: false,
        processData: false,
        cache: true,
        data : form_data,
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                $('#progress-container').removeClass('active');
                $('#msg-file-uploading').html(message);
                $('#progress-container').css("display" , "none");
                
            }else{
                $('#progress-container').removeClass('active');
                $('#progress-container div').removeClass('progress-bar-success');
                $('#progress-bar-volume').addClass('progress-bar-danger');
                $('#progress-container').css("display" , "none");
                $('#msg-file-uploading').html(message);
            }
            resultFileUpload(data);
            $('#btnUploadFile').removeAttr("disabled");
            $('#btnBack').removeAttr("disabled");
        }
    });
}

function resultFileUpload(data){
    $('#file-information').css("display" , "block");
    var status = data['status'];
    if(status==true){
        $('#file-information').addClass('alert alert-success');
    }else{
        $('#file-information').addClass('alert alert-danger');
    }

    $('#fileName').html(data['real_name_file']);
    $('#fileRename').html(data['new_name_file']);
    $('#fileMode').html(data['file_mode']);
    $('#fileStatus').html(data['message']);
    
}