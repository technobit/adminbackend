
$(function(){
    $('#btnDaftar').click(function(){
        if($('#form-tambah-buku').valid()){
            saveData();
        }
    });

    
    $('#form-tambah-buku').validate({
        ignore : "",
        rules : {
            intPublisherID : {
                required : true,
            },
            txtBookTitle : {
                required : true,
            },
        }
    });

    $('#btnUploadBukti').click(function(){
        if($("#form-upload-bukti").valid()){
            uploadBookProof();
            
        }
    });

    $('#form-upload-bukti').validate({
        ignore : "",
        rules : {
            "ntxtBookProofQualification1" : {
                required : true,
                accept : "image/*",
            },
            "ntxtBookProofQualification2" : {
                accept : "image/*",
            },
            "ntxtBookProofQualification3" : {
                accept : "image/*",
            }
        } 
    });

    
    
    $('#btnImgFrontCover').click(function(){
        $('#ntxtFrontCover').click();
    });
    $('#btnImgBackCover').click(function(){
        $('#ntxtBackCover').click();
    });
    $('#ntxtBackCover').change(function(){
        var names = $('#ntxtBackCover').prop("files")[0].name;
        $('#imgBackCover').val(names);
        uploadFileToConvert($('#ntxtBackCover') , 'ntxtBackCoverDir');
    });
    $('#ntxtFrontCover').change(function(){
        
        var names = $('#ntxtFrontCover').prop("files")[0].name;
        $('#imgFrontCover').val(names);
        uploadFileToConvert($('#ntxtFrontCover') , 'ntxtFrontCoverTxtDir');
    });

    $('select[name=intPublisherID]').change(function(){
        publisher = $('select[name=intPublisherID] :selected').text();
        $('#txtPublisherName').val(publisher);
    });

    $('#btnInternalCode').click(function(){
        $.ajax({
            url : global_url+"buku/getFormInternalCode/",
            success : function(msg){
                bootbox.dialog({
                    title : 'Pengkodean Internal Buku',
                    size : "large",
                    message : msg,
                });
            }
        })
    });

    $('#btnUpdateStatusBuku').click(function(){
        updateBookStatus();
    });

    $('#btnBookPoop1').click(function(){
        $('#ntxtBookProofQualification1').click();
    });
    $('#btnBookPoop2').click(function(){
        $('#ntxtBookProofQualification2').click();
    });
    $('#btnBookPoop3').click(function(){
        $('#ntxtBookProofQualification3').click();
    });
    
    $('#ntxtBookProofQualification1').change(function(){
        var names = $('#ntxtBookProofQualification1').prop("files")[0].name;
        $('#imgTxtBookProofQualification1').val(names);
    });

    $('#ntxtBookProofQualification2').change(function(){
        var names = $('#ntxtBookProofQualification2').prop("files")[0].name;
        $('#imgTxtBookProofQualification2').val(names);
    });
    
    $('#ntxtBookProofQualification3').change(function(){
        var names = $('#ntxtBookProofQualification3').prop("files")[0].name;
        $('#imgTxtBookProofQualification3').val(names);
    });

    $('#bitAvailableOnline1').click(function(){
        if($('#bitAvailableOnline1').is(':checked')){
            $('#curBookPriceOnline').removeAttr('readonly');
        }else{
            $('#curBookPriceOnline').attr('readonly' , 'readonly');
        }
    });

    $('#bitAvailableOffline1').click(function(){
        if($('#bitAvailableOffline1').is(':checked')){
            $('#curBookPriceOffline').removeAttr('readonly');
        }else{
            $('#curBookPriceOffline').attr('readonly' , 'readonly');
        }
    });
    
});

function getCategory(numberDiv){
    var catalogId = $('#intCatalogID'+numberDiv).val();
    if(catalogId!=""){
        $.ajax({
            url : global_url+"buku/get-list-category/",
            type : "POST",
            data : "id="+catalogId+"&mode=form",
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
                $('#intCategoryID'+numberDiv).html(data);
            }            
        });
    }
}

function setInternalCode(txtCodeInternalPublisher , txtCodeInternalBook){
    $('#txtCodeInternalPublisher').val(txtCodeInternalPublisher);
    $('#txtCodeInternalBook').val(txtCodeInternalBook);
    $('.bootbox').hide("");
}

function uploadFileToConvert(uploadDiv , imageDirSave){
    var file_data = uploadDiv.prop("files")[0];
    var form_data = new FormData();  
    form_data.append("fileImage", file_data);
    $.ajax({
        url : global_url+"buku/uploadToConvert/",
        type : "POST",
        contentType: false,
        processData: false,
        cache: true,
        data : form_data,
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                var imageFile = data['temp_image_dir']; 
                $('input[name='+imageDirSave+']').val(imageFile);
            }
        }
    });
}

function saveData(){
    $('#box-form-tambah-buku').append('<div class="overlay" id="loading-bar"><i class="fa fa-refresh fa-spin"></i></div>');
    $('#box-form-deskripsi-buku').append('<div class="overlay" id="loading-bar2"><i class="fa fa-refresh fa-spin"></i></div>');
    $('#box-form-kategori-buku').append('<div class="overlay" id="loading-bar3"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url : global_url+"buku/simpan-data/",
        type : "POST",
        dataType : "html",
        data : $('#form-tambah-buku').serialize(),
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status  = data['status'];
            var message = data['message'];
            ///var class = status==true ? 'modal-success' : 'modal';
            if(status==true){
                bootbox.alert({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    className : "modal-success",
                    callback : function(){
                        window.location.reload();
                    }
                });
            }else{
                bootbox.alert({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    className : "modal-danger",
                });
                window.location.reload();
            }
            $('#loading-bar').remove();
            $('#loading-bar2').remove();
            $('#loading-bar3').remove();
        }
    });
    
}

function updateBookStatus(){
    $('#box-update-status-buku').append('<div class="overlay" id="loading-bar-status"><i class="fa fa-refresh fa-spin"></i></div>');
    $.ajax({
        url : global_url+"buku/updateBookStatus/",
        type : "POST",
        dataType : "html",
        data : $('#form-update-status-buku').serialize(),
        success : function(response){
            var data = jQuery.parseJSON(response);
            var status  = data['status'];
            var message = data['message'];
            alertPopUp(status,message,"");
            $('#loading-bar-status').remove();
            window.location.reload();
        } 
    });
}

function uploadBookProof(){
   $('#box-upload-bukti-buku').append('<div class="overlay" id="loading-bar-upload"><i class="fa fa-refresh fa-spin"></i></div>');
    var file_data1 = $('#ntxtBookProofQualification1').prop("files")[0];
    var file_data2 = $('#ntxtBookProofQualification2').prop("files")[0];
    var file_data3 = $('#ntxtBookProofQualification3').prop("files")[0];
    var form_data = new FormData();  
    if(file_data1!=null || file_data2!=null || file_data3!=null){
        form_data.append("fileProof1", file_data1);
        form_data.append("fileProof2", file_data2);
        form_data.append("fileProof3", file_data3);
        form_data.append("intPublisherID", $('input[name=intPublisherID]').val());
        form_data.append("intPublisherBookID", $('input[name=intPublisherBookID]').val());
        
        $.ajax({
            url : global_url+"buku/uploadBookProofs/",
            type : "POST",
            contentType: false,
            processData: false,
            cache: true,
            data : form_data,
            success: function msg(res){
                var data = jQuery.parseJSON(res);
                var status = data['status'];
                var message = data['message'];
                alertPopUp(status,message,"");
                $('#loading-bar-upload').remove();
                window.location.reload();
            }
        });
    }
}

function checkDownloadBuku(txtTypeBuku, intPublisherID, intPublisherBookID){
    $.ajax({
            url : global_url+"download/checkDataBuku/",
            type : "POST",
            data : "txtTypeBuku="+txtTypeBuku+"&intPublisherID="+intPublisherID+"&intPublisherBookID="+intPublisherBookID,
            dataType : "html",
            success : function msg(response){
                var data = jQuery.parseJSON(response);
                var status = data['status'];
                var message = data['message'];
                if(status==false){
                    alertPopUp(status , message , "");
                }else{
                    window.location = global_url+"download-buku/"+txtTypeBuku+"/"+intPublisherID+"/"+intPublisherBookID+"/"; 
                }
            }
    });
}



