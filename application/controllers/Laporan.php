<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {
    
    var $meta_title = "Laporan";
    var $meta_desc = "Laporan";
	var $main_title = "Laporan";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "B01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."laporan/";
        $this->load->model("report_model");
		$this->load->model("penerbit_model");
		$this->load->model("buku_model");
		$this->load->library('excel');
    } 
    
	public function index(){
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => '',
			"custom_js" => array(),
            "custom_css" => array(),
		);	
		$this->_render("default",$dt);	
	}
	public function rekap_status_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_rekap_status_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    private function _rekap_status_buku(){
		
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Rekap Status Buku" => "#",
								);

		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
		$listPublisher = $this->penerbit_model->getDropdownPublisher();
		$arrListPublisher = array("0" => "-Semua Penerbit-");
		foreach ($listPublisher as $valuePublisher) {
			# code...
			$arrListPublisher[$valuePublisher['intPublisherID']] = $valuePublisher['txtPublisherName'];
		}
		//$dt['cmbPublisher'] = form_dropdown("intPublisherID" , $arrListPublisher , "" , "id='intPublisherID' class='form-control'");
		$dt['cmbPublisher'] = $this->form_builder->inputDropdown("Penerbit" , "intPublisherID" , "" , $arrListPublisher);
        $ret = $this->load->view("laporan/rekap_status_buku" , $dt , true);
        return $ret;
    }

	public function getBookStatusReport(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		
		$intPublisherID = $this->input->post("intPublisherID");
		$dataRekap = $this->report_model->getRecapStatusBuku($intPublisherID);
		///echopre($dataRekap);
		$retData = array();
		if(!empty($dataRekap)){
			foreach ($dataRekap as $rowRekap) {
				# code...
				$arrData = array(
					$rowRekap['txtPublisherName'],
					$rowRekap['txtBukuDiAjukan'],
					$rowRekap['txtSiapDiReview'],
					$rowRekap['txtSedangDiReview'],
					$rowRekap['txtLolosSeleksi'],
					$rowRekap['txtTidakLolosSeleksi'],
					$rowRekap['txtSiapDiKonversi'],
					$rowRekap['txtSedangDiKonversi'],
					$rowRekap['txtSiapDiBeli'],
				);
			 $retData[] = $arrData;
			}
		}
		$retVal['data'] = $retData;
		echo json_encode($retVal);
	}

	
	public function detail_status_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_status_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/detail_status_buku.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    private function _build_detail_status_buku(){
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Detail Status Buku" => "#",
								);

        $listPublisher = $this->penerbit_model->getDropdownPublisher();
		$listBookStatus = $this->buku_model->getListBookStatus();
		$arrListPublisher = array("0" => "-Pilih Penerbit-");
		foreach ($listPublisher as $valuePublisher) {
			# code...
			$arrListPublisher[$valuePublisher['intPublisherID']] = $valuePublisher['txtPublisherName'];
		}
		$arrListStatusBuku = array();
		foreach ($listBookStatus as $valueStatus) {
			# code...
			$arrListStatusBuku[$valueStatus['intStatusID']] = $valueStatus['txtStatus'];
		}

		$dt['cmbPublisher'] = $this->form_builder->inputDropdown("Penerbit" , "intPublisherID" , "" , $arrListPublisher);
		$dt['cmbStatus'] = $this->form_builder->inputDropdown("Status" , "intStatusID" , "" , $arrListStatusBuku);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/detail_status_buku" , $dt , true);
        return $ret;
    }

	public function getDetailStatusBuku(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$intPublisherID = $this->input->post("intPublisherID");
		$intStatusID = $this->input->post("intStatusID");
		$dataResult = $intPublisherID!=0 ? $this->report_model->getDetailStatusBuku($intPublisherID , $intStatusID) : array();
		///echopre($dataResult);die;
		///echopre($dataRekap);
		$retData = array();
		if(!empty($dataResult)){
			foreach ($dataResult as $rowData) {
				# code...
				$arrData = array(
					$rowData['txtPublisherName'],
					$rowData['txtISBN'],
					$rowData['txtBookTitle'],
					$rowData['txtStatus'],
				);
			 $retData[] = $arrData;
			}
		}
		$retVal['data'] = $retData;
		echo json_encode($retVal);
	}


	public function rekap_penjualan_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_rekap_penjualan_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/rekap_penjualan_buku.js",
				ASSETS_URL."plugins/datepicker/bootstrap-datepicker.js"
			),
            "custom_css" => array(
				ASSETS_URL."plugins/datepicker/datepicker3.css"

			),
		);	
		$this->_render("default",$dt);	
	}
    private function _rekap_penjualan_buku(){
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Rekap Penjualan Buku" => "#",
								);
		$listPublisher = $this->penerbit_model->getDropdownPublisher();
		$arrListPublisher = array("0" => "-Pilih Penerbit-");
		foreach ($listPublisher as $valuePublisher) {
			# code...
			$arrListPublisher[$valuePublisher['intPublisherID']] = $valuePublisher['txtPublisherName'];
		}
		$dt['cmbPublisher'] = $this->form_builder->inputDropdown("Penerbit <span class='text-danger'>*</span>" , "intPublisherID" , "" , $arrListPublisher);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/rekap_penjualan_buku" , $dt , true);
        return $ret;
    }

	public function getRecapBookSold(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$intPublisherID = $this->input->post("intPublisherID");
		$dtStart = $this->input->post("dtStart");
		$dtEnd = $this->input->post("dtEnd");
		$dataResult = $intPublisherID!=0 ? $this->report_model->getRekapPenjualanBuku($intPublisherID , $dtStart , $dtEnd) : array();
		////echopre($dataResult);die;
		///echopre($dataRekap);
		$retData = array();
		if(!empty($dataResult)){
			foreach ($dataResult as $rowData) {
				# code...
				$arrData = array(
					$rowData['txtPublisherName'],
					$rowData['intYear'],
					$rowData['intMonth'],
					$rowData['intCopyLicense'],
					$rowData['curTotalPurchase'],
				);
			 $retData[] = $arrData;
			}
		}
		$retVal['data'] = $retData;
		echo json_encode($retVal);
	}

	public function DownloadDataExcel($idJenisLaporan , $intPublisherID="" , $intStatusID="0" , $dtStart="" , $dtEnd = ""){
			
			$this->excel->setActiveSheetIndex(0);	
			switch($idJenisLaporan){
				case 1 : $titleRecap = "Rekap Status Buku";
						 $filename = "Laporan_Rekap_Status_Buku";
						 $this->getDataRekapStatusBuku($intPublisherID);
						 break;
				case 2 : $titleRecap = "Detail Status Buku";
						 $filename = "Laporan_Detail_Status_Buku";
						 $this->getDownloadDetailStatusBuku($intPublisherID , $intStatusID);
						 break;
				case 3 : $titleRecap = "Rekap Penjualan Buku";
						 $filename = "Laporan_Rekap_Penjualan_Buku";
						 $this->getDownloadRekapPenjualanStatusBuku($intPublisherID , $dtStart , $dtEnd);
						 break;    
			}
			
            $this->setHeaderExcel($titleRecap);
		    $this->getOutput($filename);
	}

	private function setHeaderExcel($titleRecap ){
        
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Laporan '.$titleRecap);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        
    }

	private function getDataRekapStatusBuku($intPublisherID){
		$no = 1;
        $this->excel->getActiveSheet()->setCellValue('A3', "Penerbit");
		$this->excel->getActiveSheet()->mergeCells('A3:A4');
        $this->excel->getActiveSheet()->setCellValue('B3', "Status Buku");
		$this->excel->getActiveSheet()->setCellValue('B4', "Buku Masuk");
		$this->excel->getActiveSheet()->setCellValue('C4', "Siap Di Review");
		$this->excel->getActiveSheet()->setCellValue('D4', "Sedang Di Review");
		$this->excel->getActiveSheet()->setCellValue('E4', "Lolos Seleksi");
		$this->excel->getActiveSheet()->setCellValue('F4', "Tidak Lolos Seleksi");
		$this->excel->getActiveSheet()->setCellValue('G4', "Siap Di Konversi");
		$this->excel->getActiveSheet()->setCellValue('H4', "Sedang Di Konversi");
		$this->excel->getActiveSheet()->setCellValue('I4', "Siap Di Beli");
		$this->excel->getActiveSheet()->mergeCells('B3:I3');

		$indexNo = 5;
		$dataRekap = $this->report_model->getRecapStatusBuku($intPublisherID);
		///echopre($dataRekap);
		$retData = array();
		if(!empty($dataRekap)){

			foreach ($dataRekap as $rowRekap) {
				
				$this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $rowRekap['txtPublisherName']);
				$this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowRekap['txtBukuDiAjukan']);
				$this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rowRekap['txtSiapDiReview']);
				$this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowRekap['txtSedangDiReview']);
				$this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $rowRekap['txtLolosSeleksi']);
				$this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $rowRekap['txtTidakLolosSeleksi']);
				$this->excel->getActiveSheet()->setCellValue('G'.$indexNo, $rowRekap['txtSiapDiKonversi']);
				$this->excel->getActiveSheet()->setCellValue('H'.$indexNo, $rowRekap['txtSedangDiKonversi']);
				$this->excel->getActiveSheet()->setCellValue('I'.$indexNo, $rowRekap['txtSiapDiBeli']);
				$indexNo++;
			}
		}

		
	}

	private function getDownloadDetailStatusBuku($intPublisherID , $intStatusID){
		$this->excel->getActiveSheet()->setCellValue('A3', "Penerbit");
        $this->excel->getActiveSheet()->setCellValue('B3', "ISBN");
		$this->excel->getActiveSheet()->setCellValue('C3', "Judul Buku");
		$this->excel->getActiveSheet()->setCellValue('D3', "Status");
		$indexNo = 4;
		$dataResult = $intPublisherID!=0 ? $this->report_model->getDetailStatusBuku($intPublisherID , $intStatusID) : array();
		if(!empty($dataResult)){
			foreach ($dataResult as $rowData) {
				# code...
				$this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $rowData['txtPublisherName']);
				$this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowData['txtISBN']);
				$this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rowData['txtBookTitle']);
				$this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowData['txtStatus']);
			 	$indexNo++;
			}
		}
	}

	private function getDownloadRekapPenjualanStatusBuku($intPublisherID , $dtStart , $dtEnd){
		$this->excel->getActiveSheet()->setCellValue('A3', "Penerbit");
        $this->excel->getActiveSheet()->setCellValue('B3', "Tahun");
		$this->excel->getActiveSheet()->setCellValue('C3', "Bulan");
		$this->excel->getActiveSheet()->setCellValue('D3', "Jumlah DRM Di Beli");
		$this->excel->getActiveSheet()->setCellValue('E3', "Sub Total Nilai");
		$indexNo = 4;

		$dataResult = $intPublisherID!=0 ? $this->report_model->getRekapPenjualanBuku($intPublisherID , $dtStart , $dtEnd) : array();
		if(!empty($dataResult)){
			foreach ($dataResult as $rowData) {
				# code...
				$this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $rowData['txtPublisherName']);
				$this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowData['intYear']);
				$this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rowData['intMonth']);
				$this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowData['intCopyLicense']);
				$this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $rowData['curTotalPurchase']);
			 	$indexNo++;
			}
		}
	}

	private function getOutput( $filename){
        ///
        $filename = date('YmdHis')."-".$filename.".xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        $objWriter->save('php://output');
    }
}
