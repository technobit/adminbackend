<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends MY_Controller {
    
    var $meta_title = "Image";
    var $meta_desc = "Image";
	var $main_title = "Image";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "K01";

    public function __construct(){
        parent::__construct();
        $this->load->model("image_model");
        $this->load->model("invoice_model");
        $this->base_url = $this->base_url_site."image/";
    } 
    
	public function getImageBookProf($file,$intPublisherID , $intPublisherBookID , $urlTitle=""){
        $arrResultImage = array(
            "bookproof1"=>"ntxtBookProofQualification1",
            "bookproof2"=>"ntxtBookProofQualification2",
            "bookproof3"=>"ntxtBookProofQualification3",
        );
        $fieldImage = $arrResultImage[$file];
        $resQuery = $this->image_model->getImageBookProof($intPublisherID , $intPublisherBookID);
        if(empty($resQuery)){
            echo "No Image Available";die;
        }
        $resVal = $resQuery[0];
        $dataImage = $resVal[$fieldImage];
        $titleImage = $resVal['txtBookTitle'];
        $fileExtension = ".".str_replace("image/" , '' , $dataImage); 
        $fileImage = explode(";" , $dataImage);       
        header('Content-Type: '.$fileImage[0].'');
        ///header("Content-Disposition: attachment; filename=".urlGenerator($urlTitle)."-".$file.$fileExtension);
        $decodedFile = base64_decode($fileImage[1]);
        echo $decodedFile;die;
    }

    public function getImageCover($file,$intPublisherID , $intPublisherBookID , $urlTitle=""){

        $arrResultImage = array(
            "front"=>"ntxtFrontCover",
            "back"=>"ntxtBackCover",
        );
        $fieldImage = $arrResultImage[$file];
        $resQuery = $this->image_model->getImageBookCover($intPublisherID , $intPublisherBookID);
        if(empty($resQuery)){
            echo "No Image Available";die;
        }
        $resVal = $resQuery[0];
        $dataImage = $resVal[$fieldImage];
        ///$titleImage = $resVal['txtBookTitle'];
        $fileExtension = ".".str_replace("image/" , '' , $dataImage); 
        $fileImage = explode(";" , $dataImage);       
        header('Content-Type: '.$fileImage[0].'');
        ///header("Content-Disposition: attachment; filename=".urlGenerator($urlTitle)."-".$file.$fileExtension);
        $decodedFile = base64_decode($fileImage[1]);
        echo $decodedFile;die;
    }

    private function setWriteImage($ntxtImage , $isDownload = false , $fileName = ""){
        $fileExtension = ".".str_replace("image/" , '' , $ntxtImage); 
        $fileImage = explode(";" , $ntxtImage);       
        header('Content-Type: '.$ntxtImage[0].'');
        if($isDownload){
            header("Content-Disposition: attachment; filename=".$fileName.$fileExtension);
        }
        ///
        $decodedFile = base64_decode($ntxtImage[1]);
        echo $decodedFile;die;
    }

    public function downloadImageBookProof($file,$intPublisherID , $intPublisherBookID , $urlTitle=""){
        
        $arrResultImage = array(
            "bookproof1"=>"ntxtBookProofQualification1",
            "bookproof2"=>"ntxtBookProofQualification2",
            "bookproof3"=>"ntxtBookProofQualification3",
        );
        $fieldImage = $arrResultImage[$file];
        $resQuery = $this->image_model->getImageBookProof($intPublisherID , $intPublisherBookID);
        if(empty($resQuery)){
            echo "No Image Available";die;
        }
        $resVal = $resQuery[0];
        $dataImage = $resVal[$fieldImage];
        $titleImage = $resVal['txtBookTitle'];
        $fileExtension = ".".str_replace("image/" , '' , $dataImage); 
        $fileImage = explode(";" , $dataImage);       
        header('Content-Type: '.$fileImage[0].'');
        header("Content-Disposition: attachment; filename=".urlGenerator($urlTitle)."-".$file.$fileExtension);
        $decodedFile = base64_decode($fileImage[1]);
        echo $decodedFile;die;
    }

    public function downloadImageCover($file,$intPublisherID , $intPublisherBookID , $urlTitle=""){
        $arrResultImage = array(
            "front"=>"ntxtFrontCover",
            "back"=>"ntxtBackCover",
        );
        $fieldImage = $arrResultImage[$file];
        $resQuery = $this->image_model->getImageBookCover($intPublisherID , $intPublisherBookID);
        if(empty($resQuery)){
            echo "No Image Available";die;
        }
        $resVal = $resQuery[0];
        $dataImage = $resVal[$fieldImage];

        $this->setWriteImage($dataImage);
    }

    public function downloadInvoice($intPublisherID , $intYear , $intMonth , $title){
        
        $resQuery = $this->invoice_model->getDetailInvoice($intPublisherID, $intYear, $intMonth);
        $fieldImage = "ntxtInvoiceProof";
        $resVal = $resQuery[0];
        $dataImage = $resVal[$fieldImage];
        ///echo $dataImage;die;
        $fileName = date("YmdHis")."-".$intPublisherID."-".$intYear."-".$intMonth."-".$title;
        $fileImage = explode(";" , $dataImage);
        $fileExtension = ".".str_replace("image/" , '' , $fileImage[0]);
        header('Content-Type: '.$fileImage[0].'');
        header("Content-Disposition: attachment; filename=".$fileName.$fileExtension);
        $decodedFile = base64_decode($fileImage[1]);
        echo $decodedFile;die;
        ///$this->setWriteImage($dataImage , false , $fileName);
        ///echopre($resInvoice);
    }
	

}
