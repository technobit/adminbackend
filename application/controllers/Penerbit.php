<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerbit extends MY_Controller {
    var $meta_title = "Penerbit";
    var $meta_desc = "Penerbit Perpustakaan";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."penerbit/";
		$this->load->model("user_model");
        $this->load->model("penerbit_model");
		$this->lang->load('id_site', 'id');
    }
    
	public function index()
	{
        /// Profil Penerbit
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_publisher(),
			"custom_js" => array(
                
				ASSETS_JS_URL."penerbit/content.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

    public  function form($intIdPublisher=""){
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_form_publisher($intIdPublisher),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."penerbit/form.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }

    private function _build_publisher(){
        $arrBreadcrumbs = array(
								"Penerbit" => $this->base_url,
								"Profil Penerbit" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['frmPublisherName'] = form_input( "txtPublisherName" , "" , 'class="form-control" id="txtPublisherName"');
        $ret = $this->load->view("penerbit/content" , $dt , true);
        return $ret;
    }
    
    private function _build_form_publisher($intIdPublisher){
        
        $idPenerbit = $intIdPublisher;
        $mode = "insert";
        $detailPenerbit = array();
        if(!empty($intIdPublisher)){
            $detailPenerbit = $this->penerbit_model->getDetailPublisher($idPenerbit);
            $detailPenerbit = $detailPenerbit[0];
            $mode = "edit";    
        }
        ///echopre($detailPenerbit);
        $arrForm = array(
            "intPublisherID",
            "txtPublisherGroup",
            "txtPublisherName",
            "txtPublisherServicesID",
            "txtCountry",
            "txtProvince",
            "txtCity",
            "txtAddress",
            "txtPostalCode",
            "txtEmail",
            "txtPhoneNumber",
            "txtFax",
            "txtURLWebsite",
            "txtContactOwner",
            "txtPhoneNumberOwner",
            "txtEmailOwner",
            "txtNPWP",
            "txtPhoneNumber1",
            "txtContact1",
            "txtEmail1",
            "txtPhoneNumber2",
            "txtContact2",
            "txtEmail2",
            "txtBankName",
            "txtBankAccount",
            "txtBankUserName",
            "txtBankNameOps",
            "txtNoBankAccountOps",
            "txtBankUserNameOps",
            "bitIKAPI",
            "bitAPPTI",
            "bitOther",
            "txtOther",
            "bitPublisherContractSign"
        );
        
        foreach ($arrForm as $keyForm) {
            # code...
            $$keyForm = isset($detailPenerbit[$keyForm]) ? $detailPenerbit[$keyForm] : "";
        }

        $listBankAccount = $this->penerbit_model->getListBank();
        $arrBank = array();
        foreach ($listBankAccount as $key => $value) {
            # code...
            $arrBank[$value['txtBankName']] = $value['txtBankName'];
        }

        /// List Internal Code
        $listInternalCode = $this->penerbit_model->getListInternalCode();
        $arrInternalCode = array();
        foreach ($listInternalCode as $key => $valueInternal) {
            # code...
            $arrInternalCode[$valueInternal['txtPublisherServicesID']] = $valueInternal['txtPublisherServicesID'];
        }
        $dt = array();
        
        $dt['intPublisherID'] = $this->form_builder->inputHidden('intPublisherID' , $intPublisherID);
        $dt['txtPublisherGroup'] = $this->form_builder->inputText('Nama Perusahaan / Badan','txtPublisherGroup' , $txtPublisherGroup);
        $dt['txtPublisherName'] = $this->form_builder->inputText('Nama Penerbit','txtPublisherName' , $txtPublisherName);
        $dt['txtInternalCode'] = $this->form_builder->inputDropdown('Kode Internal Penerbit' ,'txtPublisherServicesID' ,  $txtPublisherServicesID,$arrInternalCode);

        
        $dt['txtCountry'] = $this->form_builder->inputText('Negara' ,'txtCountry' ,  "Indonesia" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtProvince'] = $this->form_builder->inputText('Provinsi' ,'txtProvince' , $txtProvince);
        $dt['txtCity'] = $this->form_builder->inputText('Kota' ,'txtCity' ,  $txtCity);
        $dt['txtAddress'] = $this->form_builder->inputTextArea('Alamat' ,'txtAddress' ,  $txtAddress);

        $dt['txtPostalCode'] = $this->form_builder->inputText('Kode Pos' ,'txtPostalCode' ,  $txtPostalCode);
        $dt['txtEmail'] = $this->form_builder->inputText( 'Email' ,'txtEmail' , $txtEmail);
        $dt['txtPhoneNumber'] = $this->form_builder->inputText('Nomor Telpon' ,'txtPhoneNumber' ,  $txtPhoneNumber);
        $dt['txtFax'] = $this->form_builder->inputText('No Fax' ,'txtFax' , $txtFax);
        $dt['txtURLWebsite'] = $this->form_builder->inputText('Website' ,'txtURLWebsite' ,  $txtURLWebsite);
        
        $dt['txtNPWPOwner'] = $this->form_builder->inputText( 'NPWP' ,'txtNPWPOwner' , $txtNPWP);
        $dt['txtContactOwner'] = $this->form_builder->inputText( 'Kontak Pimpinan' ,'txtContactOwner' , $txtContactOwner);
        $dt['txtPhoneNumberOwner'] = $this->form_builder->inputText( 'Nomor Telpon Pimpinan' ,'txtPhoneNumberOwner' , $txtPhoneNumberOwner);
        $dt['txtEmailOwner'] = $this->form_builder->inputText( 'Email Pimpinan' ,'txtEmailOwner' , $txtEmailOwner);

        $dt['txtContact1'] = $this->form_builder->inputText( 'Kontak 1' ,'txtContact1' , $txtContact1);
        $dt['txtPhoneNumber1'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumber1' , $txtPhoneNumber1);
        $dt['txtEmail1'] = $this->form_builder->inputText( 'Email' ,'txtEmail1' , $txtEmail1);
        
        $dt['txtContact2'] = $this->form_builder->inputText( 'Kontak 2' ,'txtContact2' , $txtContact2);
        $dt['txtPhoneNumber2'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumber2' , $txtPhoneNumber2);
        $dt['txtEmail2'] = $this->form_builder->inputText( 'Email' ,'txtEmail2' , $txtEmail2);
        
        $dt['bitIKAPICheck'] = form_checkbox("txtAnggota[]",'bitIKAPI' , $bitIKAPI==1 ? TRUE : FALSE , "id='txtAnggota1'"). " IKAPI";
        $dt['bitAPPTICheck'] = form_checkbox("txtAnggota[]",'bitAPPTI' , $bitAPPTI==1 ? TRUE : FALSE , "id='txtAnggota2'" ). " APPTI" ;
        $dt['bitOtherCheck'] = form_checkbox("txtAnggota[]",'bitOther' , $bitOther==1 ? TRUE : FALSE , "id='txtAnggota3'"). " Lainnya";
        $attrOther = $bitOther==1 ? "" : 'readonly="readonly"';
        $txtOtherValue = $bitOther==1 ? $txtOther : ""; 
        $dt['bitOtherText'] = form_input("txtOther" , $txtOtherValue , "class='form-control' id='txtOtherAnggota' ".$attrOther."");
        
        
        $dt['txtBankName'] = $this->form_builder->inputText('Nama Bank' ,'txtBankName' ,  "Mandiri" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtBankAccount'] = $this->form_builder->inputText( 'No Rekening' ,'txtBankAccount' , $txtBankAccount);
        $dt['txtBankUserName'] = $this->form_builder->inputText( 'Atas Nama' ,'txtBankUserName' , $txtBankUserName);
        
        $dt['txtBankNameOps'] = $this->form_builder->inputDropdown('Nama Bank' ,'txtBankNameOps' ,  $txtBankNameOps,$arrBank );
        $dt['txtNoBankAccountOps'] = $this->form_builder->inputText( 'No Rekening' ,'txtNoBankAccountOps' , $txtNoBankAccountOps);
        $dt['txtBankUserNameOps'] = $this->form_builder->inputText( 'Atas Nama' ,'txtBankUserNameOps' , $txtBankUserNameOps);
        $arrContract = array("1" => "Sudah" , "0" => "Belum");
        $dt['bitPublisherContractSign'] = $this->form_builder->inputRadioGroup('Persetujuan Kerjasama Kontrak' ,'bitPublisherContractSign' ,  $bitPublisherContractSign,$arrContract);


        /// Form Add User

        $dt['txtUserName'] = $this->form_builder->inputText('Username' ,'txtUserName' ,  "" , "col-sm-3" );
        $dt['txtEmailAdmin'] = $this->form_builder->inputText('Email' ,'txtEmailAdmin' ,  "" , "col-sm-3" );
        $dt['txtPassword'] = $this->form_builder->inputPassword('Password' ,'txtPassword' ,  "" , "col-sm-3" );
        $dt['txtReTypePassword'] = $this->form_builder->inputPassword('Ketik Ulang Password' ,'txtReTypePassword' ,  "" , "col-sm-3" );
        $dt['txtFormMode'] = $mode;
        $arrBreadcrumbs = array(
								"Penerbit" => $this->base_url,
								"Profil Penerbit" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("penerbit/form" , $dt , true);
        return $ret;
    }
	
    public function registrasi(){
        $dt = array();
        $this->form_builder->form_type = "form-inline";
        $dt['txtPublisherGroup'] = $this->form_builder->inputText('Nama Perusahaan / Badan','txtPublisherGroup' , "");
        $dt['txtPublisherName'] = $this->form_builder->inputText('Nama Penerbit','txtPublisherName' , "");
        $dt['txtCountry'] = $this->form_builder->inputText('Negara' ,'txtCountry' ,  "Indonesia" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtProvince'] = $this->form_builder->inputText('Provinsi' ,'txtProvince' , "");
        $dt['txtCity'] = $this->form_builder->inputText('Kota' ,'txtCity' ,  "");
        $dt['txtAddress'] = $this->form_builder->inputTextArea('Alamat' ,'txtAddress' ,  "");
        $dt['txtPostalCode'] = $this->form_builder->inputText('Kode Pos' ,'txtPostalCode' ,  "");
        $dt['txtEmail'] = $this->form_builder->inputText( 'Email' ,'txtEmail' , "");
        $dt['txtPhoneNumber'] = $this->form_builder->inputText('Nomor Telpon' ,'txtPhoneNumber' ,  "");
        $dt['txtFax'] = $this->form_builder->inputText('No Fax' ,'txtFax' , "");
        $dt['txtURLWebsite'] = $this->form_builder->inputText('Website' ,'txtURLWebsite' ,  "");
        $arrForm = array("bitAPPTI" => "APPTI" , "bitIKAPI" => "IKAPI" , "bitOther"=>"Other");
        $dt['txtAnggota'] = $this->form_builder->inputCheckboxGroup('Anggota' ,'txtAnggota' , array()  , $arrForm);
        /// Login Admin
        $dt['txtUsername'] = $this->form_builder->inputText('Username' ,'txtUserName' ,  "");
        $dt['txtEmailAdmin'] = $this->form_builder->inputText('Email' ,'txtEmailAdmin' , "");
        $dt['txtPassword'] = $this->form_builder->inputText('Password' ,'txtPassword' , "");
        $dt['cancel_link'] = $this->base_url_site."login/";
        $this->load->view("penerbit/registrasi" , $dt);
    }
    
    public function registerPenerbit(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Registrasi Penerbit Gagal";
        
        $statusRes = $resVal[0]['bitSuccess'];
        
        if($statusRes==1){
            $status = true;
            $message = "Registrasi Penerbit Berhasil";
        }else{
            $status = false;
            $message = "Registrasi Penerbit Gagal";
        }
        $retVal['status'] = $status;
        $retVal['messsage'] = $message;
        echo json_encode($retVal);
    }
    
    public function savePenerbit(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Data Gagal Di Gagal";
        $intIdPublisher = $this->input->post("intPublisherID");
        $txtAnggota = !empty($this->input->post('txtAnggota')) ? $this->input->post('txtAnggota') : array();
        
        $mode = "insert";
        if(empty($intIdPublisher)){
            /// Insert Mode
            
            $arrInsert = array(
                "txtPublisherGroup" => $this->input->post("txtPublisherGroup"),
                "txtPublisherName" => $this->input->post("txtPublisherName"),
                "txtPublisherServicesID" => $this->input->post("txtPublisherGroup"),
                "txtCountry" => $this->input->post("txtCountry"),
                "txtProvince" => $this->input->post("txtProvince"),
                "txtCity" => $this->input->post("txtCity"),
                "txtAddress" => $this->input->post("txtAddress"),
                "txtPostalCode" => $this->input->post("txtPostalCode"),
                "txtEmail" => $this->input->post("txtEmail"),
                "txtPhoneNumber" => $this->input->post("txtPhoneNumber"),
                "txtFax" => $this->input->post("txtFax"),
                "txtURLWebsite" => $this->input->post("txtURLWebsite"),
                "txtContact1" => $this->input->post("txtContact1"),
                "txtPhoneNumber1" => $this->input->post("txtPhoneNumber1"),
                "txtEmail1" => $this->input->post("txtEmail1"),
                "txtContact2" => $this->input->post("txtContact2"),
                "txtPhoneNumber2" => $this->input->post("txtPhoneNumber2"),
                "txtEmail2" => $this->input->post("txtEmail2"),
                "txtBankName" => $this->input->post("txtBankName"),
                "txtBankAccount" => $this->input->post("txtBankAccount"),
                "txtBankUserName" => $this->input->post("txtBankUserName"),
                "txtBankNameOps" => $this->input->post("txtBankNameOps"),
                "txtNoBankAccountOps" => $this->input->post("txtNoBankAccountOps"),
                "txtBankUserNameOps" => $this->input->post("txtBankUserNameOps"),
                "txtBankNameOps" => $this->input->post("txtBankNameOps"),
                "txtNoBankAccountOps" => $this->input->post("txtNoBankAccountOps"),
                "txtBankUserNameOps" => $this->input->post("txtBankUserNameOps"),
                "bitAPPTI" => in_array('bitAPPTI',$txtAnggota) ? 1 : 0,
                "bitIKAPI" => in_array('bitIKAPI',$txtAnggota) ? 1 : 0,
                "bitOther" => in_array('bitOther',$txtAnggota) ? 1 : 0,
                "txtOther" => $this->input->post("txtOther"),
                "txtUserName" => $this->input->post("txtUserName"),
                "txtEmailAdmin" => $this->input->post("txtEmailAdmin"),
                "txtPassword" => $this->input->post("txtPassword"),
                "txtContactOwner" => $this->input->post("txtContactOwner"),
                "txtPhoneNumberOwner" => $this->input->post("txtPhoneNumberOwner"),
                "txtEmailOwner" => $this->input->post("txtEmailOwner"),
                "txtNPWP" => $this->input->post("txtNPWPOwner"),
            );
            $resVal = $this->penerbit_model->registrationPubliser($arrInsert);   
        }else{
            /// Update Mode
            $mode = "update";
            $arrInsert = array(
                "intPublisherID" => $intIdPublisher,
                "txtPublisherGroup" => $this->input->post("txtPublisherGroup"),
                "txtPublisherName" => $this->input->post("txtPublisherName"),
                "txtPublisherServicesID" => $this->input->post("txtPublisherGroup"),
                "txtCountry" => $this->input->post("txtCountry"),
                "txtProvince" => $this->input->post("txtProvince"),
                "txtCity" => $this->input->post("txtCity"),
                "txtAddress" => $this->input->post("txtAddress"),
                "txtPostalCode" => $this->input->post("txtPostalCode"),
                "txtEmail" => $this->input->post("txtEmail"),
                "txtPhoneNumber" => $this->input->post("txtPhoneNumber"),
                "txtFax" => $this->input->post("txtFax"),
                "txtURLWebsite" => $this->input->post("txtURLWebsite"),
                "txtContact1" => $this->input->post("txtContact1"),
                "txtPhoneNumber1" => $this->input->post("txtPhoneNumber1"),
                "txtEmail1" => $this->input->post("txtEmail1"),
                "txtContact2" => $this->input->post("txtContact2"),
                "txtPhoneNumber2" => $this->input->post("txtPhoneNumber2"),
                "txtEmail2" => $this->input->post("txtEmail2"),
                "txtBankName" => $this->input->post("txtBankName"),
                "txtBankAccount" => $this->input->post("txtBankAccount"),
                "txtBankUserName" => $this->input->post("txtBankUserName"),
                "txtBankNameOps" => $this->input->post("txtBankNameOps"),
                "txtNoBankAccountOps" => $this->input->post("txtNoBankAccountOps"),
                "txtBankUserNameOps" => $this->input->post("txtBankUserNameOps"),
                "txtBankNameOps" => $this->input->post("txtBankNameOps"),
                "txtNoBankAccountOps" => $this->input->post("txtNoBankAccountOps"),
                "txtBankUserNameOps" => $this->input->post("txtBankUserNameOps"),
                "bitAPPTI" => in_array('bitAPPTI',$txtAnggota) ? 1 : 0,
                "bitIKAPI" => in_array('bitIKAPI',$txtAnggota) ? 1 : 0,
                "bitOther" => in_array('bitOther',$txtAnggota) ? 1 : 0,
                "txtOther" => $this->input->post("txtOther"),
                "bitPublisherContractSign" => $this->input->post("bitPublisherContractSign"),
                "txtContactOwner" => $this->input->post("txtContactOwner"),
                "txtPhoneNumberOwner" => $this->input->post("txtPhoneNumberOwner"),
                "txtEmailOwner" => $this->input->post("txtEmailOwner"),
                "txtNPWP" => $this->input->post("txtNPWPOwner"),
            );
            $resVal = $this->penerbit_model->updatePenerbit($arrInsert);
        }
        $statusRes = $resVal[0]['bitSucces'];
        if($statusRes==1){
            $status = true;
            $message = $mode=="insert" ? "Data Berhasil Di Simpan" : "Data Berhasil Di Update";
        }else{
            $status = false;
            $message = $mode=="insert" ? "Data Gagal Di Simpan" : "Data Gagal Di Update";
        }
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }

    public function getPublisherList(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}

        $status = false;
		$html = "";
		$post = $_POST;
        $txtPublisherName = $this->input->post("txtPublisherName");
        $dataListPublisher = $this->penerbit_model->getListPublisher($txtPublisherName);
        $retVal['data'] = array();
        foreach ($dataListPublisher as $rowPublisher) {
            # code...
            $intIdPublisher = $rowPublisher['intPublisherID'];
            $bitStatusPublisher = $rowPublisher['bitActive']==1 ? 1 : 0;
            $txtStatus = $bitStatusPublisher==1 ? "Aktif" : "Non Aktif";
            
            $btnEdit = '<a class="btn btn-primary btn-sm btn-flat" href="'.$this->base_url.'detail/'.$intIdPublisher.'"><i class="fa fa-edit"></i> Ubah</a>';
            $btnStatus = '<button type="button" class="btn btn-success btn-sm btn-flat" onclick="updateStatusPublisher(\''.$bitStatusPublisher.'\' , \''.$intIdPublisher.'\')"><i class="fa fa-check-square-o"></i> '.$txtStatus.'</button>';
            $btnAksi = $btnEdit.$btnStatus;
            $arrData = array(
                $rowPublisher['txtPublisherName'],
                $rowPublisher['txtProvince'],
                $rowPublisher['txtCity'],
                $rowPublisher['txtAnggota'],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        echo json_encode($retVal);
    }

    public function updateStatusPublisher(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}

        $intPublisherID = $this->input->post("intPublisherID");
        $txtStatusNow = $this->input->post("txtStatusNow");

        $txtStatusUpdate = $txtStatusNow==1 ? false : true;
        $resVal = $this->penerbit_model->setStatusActivePublisher($intPublisherID , $txtStatusUpdate);
        $statusRes = $resVal[0]['bitSuccess'];
        $messageRes = $resVal[0]['txtInfo'];
        $status = $statusRes==1 ? true : false;
        $message = $status==true ? "Penerbit Berhasil Di Non Aktifkan" : "Penerbit Gagal Di Non AKtifkan";
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal); 
    }

}
