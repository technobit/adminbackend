<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {
    var $meta_title = "Invoice";
    var $meta_desc = "Invoice Perpustakaan";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = TEMP_UPLOAD_DIR;
	var $upload_url = URL_UPLOAD_DIR;
	var $limit = "10";
	var $status_list = "";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."invoice/";
		$this->status_list = $this->config->item("status_invoice_list");
        $this->load->model("invoice_model");
		$this->load->model("penerbit_model");
		$this->lang->load('id_site', 'id');
		$this->load->library('upload'); 
    }
    
	public function index()
	{
        /// invoice
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_invoice(),
			"custom_js" => array(
				ASSETS_JS_URL."invoice/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_invoice(){
        
		
        ///$idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");
		$arrListPublisher = array();
		$listPublisher = $this->penerbit_model->getDropdownPublisher();
		foreach ($listPublisher as $rowPublisher) {
			# code...
			$arrListPublisher[$rowPublisher['intPublisherID']] = $rowPublisher['txtPublisherName'];
		}

		$frmListPublisher = $this->form_builder->inputDropdown("Penerbit" , "intPublisherID" , "" , $arrListPublisher);
        $arrBreadcrumbs = array(
								"Invoice" => $this->base_url,
								"List Invoice" => "#",
								);
		$dt['url'] = $this->base_url;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);        
		$dt['frmListPublisher'] = $frmListPublisher;
		$dt['frmListStatus'] = $this->form_builder->inputDropdown("Status" , "intStatusID" , "" , $this->status_list);
        $ret = $this->load->view("invoice/list" , $dt , true);
        return $ret;
    }

	public function getDataInvoice(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$status = false;
		$html = "";
		$intPublisherID = !empty($this->input->post("intPublisherID")) ? $this->input->post("intPublisherID") : 0;
		$intStatusID = !empty($this->input->post("intStatusID")) ? $this->input->post("intStatusID") : 0;
		$invoiceList = array();
		if(!empty($intPublisherID)){
			$invoiceList = $this->invoice_model->getListInvoice($intPublisherID,$intStatusID);
		}

		$retVal['data'] = array();
		if(!empty($invoiceList)){
			# code...
			///echopre($invoiceList);die;
			
			foreach ($invoiceList as $rows) {
			$status = $rows['intStatus'];
			$intPublisherID = $rows['intPublisherID'];
			$intYear = $rows['intYear'];
			$intMonth = $rows['intMonth'];
			///$ntxtInvoiceProof = $rows['ntxtInvoiceProof'];
		 	
			$link_detail = $this->base_url."detail/".$intPublisherID."/".$intYear."/".$intMonth."/";
			$link_download = "";
			$btnDownload = "";
			/*
			if(!empty($ntxtInvoiceProof)){
				$link_download = $this->base_url_site."image/download-invoice/".$intPublisherID."/".$intYear."/".$intMonth."/".$intYear."-".$intMonth."-invoice-proof.jpg";
				$btnDownload = '<a class="btn btn-sm btn-warning btn-flat" href="'.$link_download.'" target="_blank"><i class="fa fa-download"></i> Download Invoice</a>';				
			}
			*/
			switch ($status) {
				case '2':
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$link_detail.'"><i class="fa fa-edit"></i> Detail</a>
								'.$btnDownload.'	
								<button class="btn btn-sm btn-info btn-flat" type="button" onclick="updateStatusInvoice('.$intPublisherID.','.$intYear.','.$intMonth.')"><i class="fa fa-money"></i> Sudah Di Bayarkan</button>
								'; 
				break;
				case '3':
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$link_detail.'"><i class="fa fa-edit"></i> Detail</a>
								'.$btnDownload.'
								'; 
				break;
				default:
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$link_detail.'"><i class="fa fa-edit"></i> Detail</a>'; 
					break;
			}
			///$btnAksi = '<a class="btn btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>';
			$monthList = $this->config->item("month_list");
			$bulan = $monthList[$rows['intMonth']];
			$statusList = $this->status_list;
			$statusInvoice = $statusList[$rows['intStatus']]; 		
			$arrData = array(
				$rows['intYear'],
				$bulan,
				$rows['txtPublisherName'],
				$statusInvoice,
				$btnAksi
			);	
			$retVal['data'][] = $arrData;	
			}
		}
		echo json_encode($retVal);
	}
	
	public function detail($intPublisherID,$intYear, $intMonth){
        /// invoice
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_invoice($intPublisherID,$intYear, $intMonth),
			"custom_js" => array(
				ASSETS_JS_URL."invoice/detail.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_detail_invoice($intPublisherID,$intYear, $intMonth){
        $resInvoice = $this->invoice_model->getDetailInvoice($intPublisherID, $intYear, $intMonth);
		///$detailPublisher = $this->penerbit_model->getDetailPublisher($intPublisherID);
		///$namaPenerbit = $detailPublisher[0]['txtPublisherName'];
		$monthList = $this->config->item("month_list");
		$bulan = $monthList[$intMonth];
		$statusList = $this->status_list;
		if(!empty($resInvoice)){
			$detailInvoice = $resInvoice[0];
			$ntxtInvoiceProof = $detailInvoice['ntxtInvoiceProof'];
			
			$link_download = ""; 
			if(!empty($ntxtInvoiceProof)){
				$link_download = $this->base_url_site."image/download-invoice/".$intPublisherID."/".$intYear."/".$intMonth."/".$intYear."-".$intMonth."-invoice-proof.jpg";
			}
			
			$status = $detailInvoice['intStatus'];
			
			$dtInvoice = explode('T',$detailInvoice['dtInvoice']);
			$tanggalInvoice = indonesian_date($dtInvoice[0]);
			$detailInvoice['txtMonth'] = $bulan;
			$detailInvoice['txtIndonesianDate'] = $tanggalInvoice;
			$detailInvoice['txtStatus'] = $statusList[$detailInvoice['intStatus']];
			$detailInvoice['linkDownload'] = $link_download;
			
		}else{
			$status = "1";
			$detailInvoice = array(
				"txtMonth" => $bulan,
				"intYear" => $intYear,
				"txtStatus"=> $statusList[1]
			);
		}
		
        $arrBreadcrumbs = array(
								"Invoice" => $this->base_url,
								"Detail Invoice" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['detailInvoice'] = $detailInvoice;
		$dt['statusInvoice'] = $status;
        $ret = $this->load->view("invoice/detail" , $dt , true);
        return $ret;
    }
	
	public function updateStatusInvoice(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		$intPublisherID = !empty($this->input->post("intPublisherID")) ? $this->input->post("intPublisherID") : 0;
		$intYear = $this->input->post("intYear");
		$intMonth = $this->input->post("intMonth");

		$resQuery = $this->invoice_model->updateInvoice($intPublisherID , $intYear , $intMonth);
		$resVal = $resQuery[0];
		$status = $resVal['bitSuccess'] == 1 ? true : false;
		$message = $status==true ? "Status Invoice Berhasil Di Update" : "Status Invoice Gagal Di Update";
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	public function getDataSoldBook(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		$intPublisherID = !empty($this->input->post("intPublisherID")) ? $this->input->post("intPublisherID") : 0;
		$intYear = $this->input->post("intYear");
		$intMonth = $this->input->post("intMonth");
		$retVal['data'] = array();
		if(empty($intPublisherID)){
			echo json_encode($retVal);
			die;
		}
		
		$resVal = $this->invoice_model->getInvoicePurchaseDetail($intPublisherID , $intYear , $intMonth);
		
		if(!empty($resVal)){
			foreach ($resVal as $rows) {
				# code...
				$dtTransaksi = explode("T" , $rows['dtBookPurchase']);
				$tanggalTransaksi = indonesian_date($dtTransaksi[0]);
				$arrData = array(
					$tanggalTransaksi , 
					$rows['txtLibraryName'],
					$rows['txtBookTitle'],
					$rows['curBookLicensePrice'],
					$rows['intCopyLicense'],
					$rows['curTotalPurchase'],
				);
				$retVal['data'][] = $arrData;
			}
		}
		echo json_encode($retVal);
	}
}