<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends MY_Controller {
    
    var $meta_title = "Buku";
    var $meta_desc = "Buku";
	var $main_title = "Buku";
    var $base_url = "";
	var $front_cover_url = "";
	var $back_cover_url = "";
	var $upload_dir = TEMP_UPLOAD_DIR;
	var $upload_url = URL_UPLOAD_DIR;
	var $download_dir = TEMP_DOWNLOAD_DIR;
	var $download_url = URL_DOWNLOAD_DIR;
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "B01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."buku/";
		$this->front_cover_url = $this->base_url_site."image/get-image-cover/front/";
		$this->back_cover_url = $this->base_url_site."image/get-image-cover/back/";
        $this->load->model("buku_model");
		$this->load->library('ftp');
		$this->load->library('upload');
    } 
    
	public function index(){
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_data_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."buku/content.js",
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

	public function form($intPublisherID="",$intPublisherBookID=""){
		
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_form_buku($intPublisherID,$intPublisherBookID),
			"custom_js" => array(
				
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/validate/accept.js",
				ASSETS_JS_URL."buku/form.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

	public function uploadBuku($mode , $intPublisherID , $intPublisherBookID){
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_upload_buku($mode , $intPublisherID , $intPublisherBookID),
			"custom_js" => array(
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."buku/upload.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);
	}

	private function _data_buku(){
		$listPublisher = $this->buku_model->getListPublisher();
        $listCatalog = $this->buku_model->getListCatalog();
        $listBookStatus = $this->buku_model->getListBookStatus();

		$arrListPublisher = array("" => "-Pilih Penerbit-");
		foreach ($listPublisher as $valuePublisher) {
			# code...
			$arrListPublisher[$valuePublisher['intPublisherID']] = $valuePublisher['txtPublisherName'];
		}
		$arrListCatalogBuku = array(""=>"-Pilih Katalog-");	
		foreach ($listCatalog as $valueCatalog) {
			# code...
			$arrListCatalogBuku[$valueCatalog['intCatalogID']] = $valueCatalog['txtCatalog'];
		}
		$arrListCatalogBuku[0] = "-Semua-";
		$arrListStatusBuku = array();
		
		foreach ($listBookStatus as $valueStatus) {
			# code...
			$arrListStatusBuku[$valueStatus['intStatusID']] = $valueStatus['txtStatus'];
		}
		$arrListStatusBuku[0] = "-Semua-";

		$arrListCategory = array(""=>"-Pilih Kategori-");
		$arrListCategory[0] = "-Semua-";
		$dt['cmbPublisher'] = form_dropdown("intPublisherID" , $arrListPublisher , "" , "id='intPublisherID' class='form-control'");
        $dt['cmbDropdownCatalog'] = form_dropdown("intCatalogID" , $arrListCatalogBuku , "" , "id='intCatalogID' class='form-control'");
		$dt['cmbDropdownCategory'] = form_dropdown("intCategoryID" , $arrListCategory , "" , "id='intCategoryID' class='form-control'");
		$dt['cmbDropdownStatus'] = form_dropdown("intStatusID" , $arrListStatusBuku , "" , "id='intStatusID' class='form-control'");

		$arrBreadcrumbs = array("Data Buku" => "#");
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
		$dt['link_registrasi_buku'] = $this->base_url."tambah-buku/";
        $ret = $this->load->view("buku/content" , $dt , true);
        return $ret;
    }

	private function _build_upload_buku($mode , $intPublisherID , $intPublisherBookID){
		$dt = array();

		$getDetailBuku = $this->buku_model->getBookDetail($intPublisherID,$intPublisherBookID);
		
		$detailBuku = $getDetailBuku[0];
		$title = $mode=="contoh" ? "Contoh" : "Lengkap";
		$arrBreadcrumbs = array(
							"Daftar Buku" => $this->base_url,
							"Upload Buku ".$title => "#"  	
								);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
		$dt['title'] = $title;
		$dt['detail_buku'] = $detailBuku;

		$dt['frm_id_publisher'] = $this->form_builder->inputHidden("intPublisherID" , $intPublisherID);
		$dt['frm_id_buku'] = $this->form_builder->inputHidden("intBooksPublisherID" , $intPublisherBookID);
		$dt['frm_title_buku'] = $this->form_builder->inputHidden("txtBooksTitle" , $detailBuku['txtBookTitle']);
		$dt['frm_mode_form'] = $this->form_builder->inputHidden("txtModeForm" , $mode);
		$dt['link_back']= $this->base_url;
		$ret = $this->load->view("buku/upload" , $dt , true);
        return $ret;
	}
    
	private function _form_buku($intPublisherID="" , $intPublisherBookID=""){
		
		$mode = "insert";
		$detailBuku = array();
		$arrOptionsCategory1 = array("0"=>"-Pilih Kategori-");
		$arrOptionsCategory2 = array("0"=>"-Pilih Kategori-");
		$arrOptionsCategory3 = array("0"=>"-Pilih Kategori-");
		$imageUrlFront = "";
		$imageDirFront = "";
		$imageUrlBack = "";
		$imageDirBack = "";
		$btnDownload = "";
		$link_donwload_pdf = "";
		$donwload_caption = "";
		$arrBreadcrumbs = array(
							"Daftar Buku" => $this->base_url,
							"Registrasi Buku" => "#"
							);
		$titlePage = "Registrasi Buku";
		$booksDetailInformation = array();
		if(!empty($intPublisherBookID) && !empty($intPublisherID)){

			$mode = "update";
			$getDetailBuku = $this->buku_model->getBookDetail($intPublisherID,$intPublisherBookID);
			
			if(empty($getDetailBuku)){
				show_404();
			}

			///$booksDetailInformation = $getBookInformation[0];
			$detailBuku = $getDetailBuku[0];

			/// Link Download PDF
			$bookStatus = $detailBuku['intStatusID'];
			$bookTitle = $detailBuku['txtBookTitle'];
			
			if(($bookStatus==3) || ($bookStatus==4)){
				$link_donwload_pdf = $this->base_url.'download-buku/parsial/'.$detailBuku['intPublisherID'].'/'.$detailBuku['intPublisherBookID'].'/';
				$donwload_caption = "Download PDF Contoh";
				$btnDownload = '<button class="btn btn-sm btn-info btn-flat" onclick="checkDownloadBuku(\'parsial\' , '.$intPublisherID.' , '.$intPublisherBookID.')"><i class="fa fa-download"></i> Download PDF Contoh</button>';  
			}else if($bookStatus==7){
				$link_donwload_pdf = $this->base_url.'download-buku/lengkap/'.$detailBuku['intPublisherID'].'/'.$detailBuku['intPublisherBookID'].'/';
				$donwload_caption = "Download PDF Lengkap";
				$btnDownload = '<button class="btn btn-sm btn-info btn-flat" onclick="checkDownloadBuku(\'lengkap\' , '.$intPublisherID.' , '.$intPublisherBookID.')"><i class="fa fa-download"></i> Download PDF Lengkap</button>';
			}

			$dt['link_donwload_pdf'] = $link_donwload_pdf;
			$dt['donwload_caption'] = $donwload_caption;
			$dt['btnDownload'] = $btnDownload;

			/// List Status
			$listBookStatus = $this->buku_model->getListBookStatus();
			$arrListStatusBuku = array();
		
			foreach ($listBookStatus as $valueStatus) {
				# code...
				if(($valueStatus['intStatusID']!="1") && ($valueStatus['intStatusID']!=$bookStatus)){
					$arrListStatusBuku[$valueStatus['intStatusID']] = $valueStatus['txtStatus'];
				}
				
			}
			$dt['txtCurrentStatus'] = $this->form_builder->inputText("Status Buku Saat Ini" , "txtCurrentStatus",$detailBuku['txtStatus'],"col-sm-3",array("readonly"=>"readonly"));
			$dt['cmbDropdownStatus'] = $this->form_builder->inputDropdown("Ubah Status Ke" , "txtStatusBuku","",$arrListStatusBuku);

			/// Link Download Book Proof Qualification
			$link_donwload_bookproof1 = "";
			if(!empty($detailBuku['ntxtBookProofQualification1'])){
				$link_donwload_bookproof1 = $this->base_url_site."image/download-image-bookprof/bookproof1/".$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg";
			}

			$link_donwload_bookproof2 ="";
			if(!empty($detailBuku['ntxtBookProofQualification2'])){
				$link_donwload_bookproof2 = $this->base_url_site."image/download-image-bookprof/bookproof2/".$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg";
			}
			$link_donwload_bookproof3 = "";
			if(!empty($detailBuku['ntxtBookProofQualification3'])){
				$link_donwload_bookproof3 = $this->base_url_site."image/download-image-bookprof/bookproof3/".$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg";
			}

			$dt['linkDownloadBookProof1'] = $link_donwload_bookproof1;
			$dt['linkDownloadBookProof2'] = $link_donwload_bookproof2;
			$dt['linkDownloadBookProof3'] = $link_donwload_bookproof3;
			
			/// Selected Category 
			if($detailBuku['intCategoryID1']!=0){
				///$arrOptionsCategory1 = array($detailBuku['intCategoryID1'] => $detailBuku['txCategory1']);
				$listCategory1 = $this->buku_model->getListCategory($detailBuku['intCategoryID1']);
				if(!empty($listCategory1)){
					foreach ($listCategory1 as $rowCategory1) {
					# code...
						$arrOptionsCategory1[$rowCategory1['intCategoryID']] = $rowCategory1['txtCategory'];
					}
				}
			}
			
			if($detailBuku['intCategoryID2']!=0){
				///$arrOptionsCategory2 = array($detailBuku['intCategoryID2'] => $detailBuku['txCategory2']);
				$listCategory2 = $this->buku_model->getListCategory($detailBuku['intCategoryID2']);
				if(!empty($listCategory2)){
					foreach ($listCategory2 as $rowCategory2) {
					# code...
						$arrOptionsCategory2[$rowCategory2['intCategoryID']] = $rowCategory2['txtCategory'];
					}
				}
			}

			if($detailBuku['intCategoryID3']!=0){
				///$arrOptionsCategory3 = array($detailBuku['intCategoryID3'] => $detailBuku['txCategory3']);
				$listCategory3 = $this->buku_model->getListCategory($detailBuku['intCategoryID3']);
				if(!empty($listCategory3)){
					foreach ($listCategory3 as $rowCategory3) {
					# code...
						$arrOptionsCategory3[$rowCategory3['intCategoryID']] = $rowCategory3['txtCategory'];
					}
				}
			}

			/// Image Front Rename
			$imageFront = $detailBuku['ntxtFrontCover'];
			if(!empty($imageFront)){
				$imageUrlFront = $this->front_cover_url.$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg";
				
			}
			
			/// Image Back
			$imageBack = $detailBuku['ntxtBackCover'];
			if(!empty($imageBack)){
				$imageUrlBack = $this->back_cover_url.$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg";
				///$imageUrlBack = $this->download_url.$imageName;
			}
			
			$arrBreadcrumbs = array(
								"Daftar Buku" => $this->base_url,
								"Detail Buku \"".$detailBuku['txtBookTitle']."\"" => "#"
							);
			$titlePage = "Detail Buku \"".$detailBuku['txtBookTitle']."\"";
		}

		$arrInputVal = array(
			"intPublisherID",
			"intPublisherBookID",
			"txtBookTitle",
			"txtSubBookTitle",
			"txtLanguage",
			"txtPublisherName",
			"txtImprint",
			"intYear",
			"txtEdition",
			"intBookPage",
			"txtISBN",
			"txtShortDescription",
			"txtSynopsis",
			"txtKeyWords",
			"bitAvailableOnline",
			"curBookPriceOnline",
			"bitAvailableOffline",
			"curBookPriceOffline",
			"intCatalogID1",
			"intCategoryID1",
			"intCatalogID2",
			"intCategoryID2",
			"intCatalogID3",
			"intCategoryID3",
			"txtCodeInternalPublisher",
			"txtCodeInternalBook",
			"txtFrontCoverFileName",
			"txtBackCoverFileName",
			"intBookRentDays",
			"txtAuthor",
		);

		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
		$dt['title'] = $titlePage;
		$arrCatalog = array("0"=>"-Pilih Katalog-");
		$listCatalog = $this->buku_model->getListCatalog();
		foreach ($listCatalog as $key => $value) {
			# code...
			$arrCatalog[$value['intCatalogID']] = $value['txtCatalog'];
		}

		foreach ($arrInputVal as $inputVal) {
			# code...
			$$inputVal = isset($detailBuku[$inputVal]) ? $detailBuku[$inputVal] : ""; 
		}
		

		$listPublisher = $this->buku_model->getListPublisher();
		///echopre($listPublisher);die;
		$arrPublisher = array(""=>"-Pilih Penerbit-");
		foreach ($listPublisher as $rowPublisher) {
			# code...
			$arrPublisher[$rowPublisher['intPublisherID']] = $rowPublisher['txtPublisherName'];
		}

		////$dt['booksDetailInformation'] = $booksDetailInformation;
		///$dt['intPublisherID'] = $this->form_builder->inputHidden("intPublisherID",$this->session->userdata("pcw_penerbit_publisher_id"));
		$dt['intPublisherID'] = $mode=="insert" ? $this->form_builder->inputDropdown("Penerbit" , "intPublisherID" , $intPublisherID , $arrPublisher) : $this->form_builder->inputHidden("intPublisherID",$intPublisherID);
		$dt['intPublisherBookID'] = $this->form_builder->inputHidden("intPublisherBookID",$intPublisherBookID);
		
		$dt['txtBookTitle'] = $this->form_builder->inputText("Judul Buku" , "txtBookTitle" ,$txtBookTitle , "col-sm-3");
		$dt['txtSubBookTitle'] = $this->form_builder->inputText("Sub Judul Buku" , "txtSubBookTitle" ,$txtSubBookTitle , "col-sm-3");

		$arrListLanguage = array("Indonesia" => "Indonesia" , "Inggris" => "Inggris");
		$dt['txtLanguage'] = $this->form_builder->inputDropdown("Bahasa" , "txtLanguage" ,$txtLanguage , $arrListLanguage , array() , "col-sm-3");
		$dt['txtPublisherName'] = $this->form_builder->inputText("Penerbit" , "txtPublisherName" ,$txtPublisherName , "col-sm-3",array("readonly" =>""));
		$dt['txtAuthor'] = $this->form_builder->inputText("Penulis" , "txtAuthor" ,$txtAuthor , "col-sm-3");
		$dt['txtImprint'] = $this->form_builder->inputText("Imprint" , "txtImprint" ,$txtImprint , "col-sm-3");
		$year = range(1900 , date("Y"));
		$arrYear = array();
		foreach ($year as $years) {
			# code...
			$arrYear[$years] = $years;
		}
		$arrYear = array_reverse($arrYear , true);

		$day = range(1,30);
		$arrDays = array();
		foreach ($day as $days) {
			# code...
			$arrDays[$days] = $days;
		}

		$dt['txtYear'] = $this->form_builder->inputDropdown("Tahun" , "txtYear" ,$intYear , $arrYear , array() , "col-sm-3");
		$dt['intBookRentDays'] = $this->form_builder->inputDropdown("Lama Peminjaman Buku (Hari)" , "intBookRentDays" ,$intYear , $arrDays , array() , "col-sm-3");
		$dt['txtEdition'] = $this->form_builder->inputText("Edisi" , "txtEdition" ,$txtEdition , "col-sm-3");
		$dt['intBookPage'] = $this->form_builder->inputText("Jumlah Halaman" , "intBookPage" ,$intBookPage , "col-sm-3");
		$dt['txtISBN'] = $this->form_builder->inputText("ISBN" , "txtISBN" ,$txtISBN , "col-sm-3");
		$dt['txtShortDescription'] = $this->form_builder->inputTextArea("Deskripsi" , "txtShortDescription" ,$txtShortDescription , "col-sm-3" , array("rows" => 3, "cols" => 4));
		$dt['txtSynopsis'] = $this->form_builder->inputTextArea("Synopsis" , "txtSynopsis" ,$txtSynopsis , "col-sm-3" , array("rows" => 3, "cols" => 4));
		$dt['txtKeyWords'] = $this->form_builder->inputTextArea("Kata Kunci" , "txtKeyWords" ,$txtKeyWords , "col-sm-3" , array("rows" => 3, "cols" => 4));		
		///$dt['curBookPrice'] = $this->form_builder->inputText("Harga DRM/Satuan" , "curBookPrice" ,$curBookPrice , "col-sm-3");

		$arrbitAvailableOnline = $bitAvailableOnline==1 ? array(1) : array();
		if(!empty($curBookPriceOnline)){
			$arrbitAvailableOnline = array(1);
		} 
		$dt['bitAvailableOnline'] = $this->form_builder->inputCheckboxGroup("Akuisisi Perpustakaan Online" , "bitAvailableOnline" , $arrbitAvailableOnline , array("1" => "Akuisisi"));
		$arrDisabledOnline = $bitAvailableOnline==0 ? array("readonly" => "readonly") : array();
		if(!empty($curBookPriceOnline)){
			$arrDisabledOnline = array();
		}		
		$dt['curBookPriceOnline'] = $this->form_builder->inputText("Harga 1 DRM (Online)" , "curBookPriceOnline" ,$curBookPriceOnline , "col-sm-3" , $arrDisabledOnline);

		$arrbitAvailableOffline = $bitAvailableOffline==1 ? array(1) : array();
		if(!empty($curBookPriceOffline)){
			$arrbitAvailableOffline = array(1);
		} 
		$dt['bitAvailableOffline'] = $this->form_builder->inputCheckboxGroup("Akuisisi Perpustakaan Offline" , "bitAvailableOffline" , $arrbitAvailableOffline , array("1" => "Akuisisi"));
		$arrDisabledOffline = $bitAvailableOffline==0 ? array("readonly" => "readonly") : array();
		if(!empty($curBookPriceOffline)){
			$arrDisabledOffline = array();
		}		 		
		$dt['curBookPriceOffline'] = $this->form_builder->inputText("Harga 1 DRM (Offline)" , "curBookPriceOffline" ,$curBookPriceOffline , "col-sm-3", $arrDisabledOffline);
		
		
		$dt['intCatalogID1'] = $this->form_builder->inputDropdown("Katalog 1" , "intCatalogID1" ,$intCatalogID1 , $arrCatalog , array("onchange"=>"getCategory(1)") , "col-sm-3");
		$dt['intCategoryID1'] = $this->form_builder->inputDropdown("Kategori 1" , "intCategoryID1" ,$intCategoryID1 , $arrOptionsCategory1 , array() , "col-sm-3");

		$dt['intCatalogID2'] = $this->form_builder->inputDropdown("Katalog 2" , "intCatalogID2" ,$intCatalogID2 , $arrCatalog , array("onchange"=>"getCategory(2)") , "col-sm-3");
		$dt['intCategoryID2'] = $this->form_builder->inputDropdown("Kategori 2" , "intCategoryID2" ,$intCategoryID2 , $arrOptionsCategory2 ,  array(), "col-sm-3");

		$dt['intCatalogID3'] = $this->form_builder->inputDropdown("Katalog 3" , "intCatalogID3" ,$intCatalogID3 , $arrCatalog , array("onchange"=>"getCategory(3)") , "col-sm-3");
		$dt['intCategoryID3'] = $this->form_builder->inputDropdown("Kategori 3" , "intCategoryID3" ,$intCategoryID3 , $arrOptionsCategory3 ,  array(), "col-sm-3");

		$dt['txtCodeInternalPublisher'] = $this->form_builder->inputText("Kode Internal Penerbit" , "txtCodeInternalPublisher" ,$txtCodeInternalPublisher , "col-sm-3",array("readonly"=>"readonly"));
		$dt['txtCodeInternalBook'] = $this->form_builder->inputText("Kode Internal Buku" , "txtCodeInternalBook" ,$txtCodeInternalBook , "col-sm-3",array("readonly"=>"readonly"));

		$dt['ntxtFrontCover'] = $this->form_builder->inputHidden("ntxtFrontCoverTxtDir", $imageDirFront);
		$dt['ntxtBackCover'] = $this->form_builder->inputHidden("ntxtBackCoverDir", $imageDirBack);
		$dt['txtFrontCoverFileName'] = $this->form_builder->inputHidden("txtFrontCoverFileName", $txtFrontCoverFileName);
		$dt['txtBackCoverFileName'] = $this->form_builder->inputHidden("txtBackCoverFileName", $txtBackCoverFileName);
		$dt['frmMode'] = $this->form_builder->inputHidden("txtFormMode", $mode);
		$dt['mode'] = $mode;
		$dt['imageUrlFront'] = !empty($imageUrlFront) ? $imageUrlFront : "http://placehold.it/200x200/";
		$dt['btnUpdate'] = $mode=='insert' ? "Daftarkan" : "Simpan";
		$ret = $this->load->view("buku/form" , $dt , true);
        return $ret;

	}
    
	//// Dependicies Function, Throw From Ajax Not From Your Heart :p
	public function getListCategory(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$intCatalogID = $this->input->post("id");
		$modeForm = $this->input->post("mode");
		
		$listCategory = array();
		if($intCatalogID!=0){
		$listCategory = $this->buku_model->getListCategory($intCatalogID);
		}
		
		///echopre($listCategory);
		$html = "";
		if(!empty($listCategory)){
			if($modeForm!="form"){
				$html .= "<option value='0'>-Semua-</option>";
			}
			foreach ($listCategory as $key => $value) {
				# code...
				$html .= "<option value='".$value['intCategoryID']."'>".$value['txtCategory']."</option>";
			}
			
			
		}else{
			if($modeForm!="form"){
				$html .= "<option value='0'>-Semua-</option>";
			}
		}
		
		//$this->setJsonOutput($html);
		echo json_encode($html);
	}

	public function getBookList(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }

		$status = false;
		$html = "";
		$intPublisherID = !empty($this->input->post("intPublisherID")) ? $this->input->post("intPublisherID") : 0;
		$intCatalogID = !empty($this->input->post("intCatalogID")) ? $this->input->post("intCatalogID") : 0;
		$intCategoryID = !empty($this->input->post("intCategoryID")) ? $this->input->post("intCategoryID") : 0;
		$intStatusID = !empty($this->input->post("intStatusID")) ? $this->input->post("intStatusID") : 0;
		$txtBookTitle = $this->input->post("txtBookTitle");
		$bookList = array();
		if(!empty($intPublisherID)){
			$bookList = $this->buku_model->getBookList($intPublisherID,$intCatalogID,$intCategoryID,$intStatusID,$txtBookTitle);
		}
		///echopre($bookList);

		$retVal['data'] = array();
		if(!empty($bookList)){
			# code...
			foreach ($bookList as $bookValue) {
			$status = $bookValue['intStatusID'];
			$intPublisherID = $bookValue['intPublisherID'];
			$intPublisherBookID = $bookValue['intPublisherBookID'];
			switch ($status) {
				case '3':
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>
								<button class="btn btn-sm btn-warning btn-flat" onclick="checkDownloadBuku(\'parsial\' , '.$intPublisherID.' , '.$intPublisherBookID.')"><i class="fa fa-download"></i> Download PDF Contoh</button>
								'; 
				break;
				case '4':
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>
								<button class="btn btn-sm btn-warning btn-flat" onclick="checkDownloadBuku(\'parsial\' , '.$intPublisherID.' , '.$intPublisherBookID.')"><i class="fa fa-download"></i> Download PDF Contoh</button>
								'; 
				break;
				case '7':
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>
								<button class="btn btn-sm btn-warning btn-flat" onclick="checkDownloadBuku(\'lengkap\' , '.$intPublisherID.' , '.$intPublisherBookID.')"><i class="fa fa-download"></i> Download PDF Lengkap</button>
								'; 
				break;
				default:
					# code...
					$btnAksi = '<a class="btn btn-sm btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>'; 
					break;
			}
			///$btnAksi = '<a class="btn btn-primary btn-flat" href="'.$this->base_url.'edit-buku/'.$bookValue['intPublisherID'].'/'.$bookValue['intPublisherBookID'].'/"><i class="fa fa-edit"></i> Ubah</a>';
			$intPublisherID = $bookValue['intPublisherID'];
			$intPublisherBookID = $bookValue['intPublisherBookID'];
			$bookTitle = $bookValue['txtBookTitle'];
			$bookStatus = $bookValue['intStatusID'];
			$imageFile = $bookValue['ntxtFrontCover'];
			
			$kukuJempol = !empty($imageFile) ? $this->front_cover_url.$intPublisherID."/".$intPublisherBookID."/".urlGenerator($bookTitle).".jpg" : 'http://placehold.it/100x100/';
			$ImgkukuJempol = "<img src='".$kukuJempol."' height='100' style='height:60px;' class='img-responsive'>";			
			$arrData = array(
				$ImgkukuJempol,
				$bookValue['txtCatalog'],
				$bookValue['txCategory'],
				$bookValue['txtAuthor'],
				$bookValue['txtBookTitle'],
				$bookValue['txtStatus'],
				$btnAksi
			);	
			$retVal['data'][] = $arrData;	
			}
		}
		echo json_encode($retVal);
	}

	public function uploadToConvert(){

		if(!$this->input->is_ajax_request()){
			echo "Ilegal Upload";die;
		}

		$retVal = array();
		///$this->load->library("upload");
		$fileImage = $_FILES['fileImage'];
		$nameFile = $fileImage["name"];
		$imagefile = explode("." , $nameFile);
        $extImage = "." .end($imagefile);
        $postName = str_replace($extImage ,"",$nameFile);
		$postName = $postName.date("YmdHis");

		$config['upload_path'] = ASSETS_UPLOAD_DIR;
		$config['allowed_types'] = 'png|jpg';
		$config['file_name'] = $postName;
		$this->upload->initialize($config);
		$resUpload = $this->upload->do_upload("fileImage");
		if(!$resUpload){
			$retVal['status'] = $resUpload;
			$retVal['message'] = $this->upload->display_errors();
		}else{
			$data_upload = $this->upload->data();
			$file_dir = $data_upload['full_path'];
			$image_temp_url = str_replace(TEMP_UPLOAD_DIR , URL_UPLOAD_DIR , $file_dir);
			$retVal['status'] = true;
			$retVal['message'] = "File Berhasil Di Upload";
			$retVal['temp_image_url'] = $image_temp_url;
			$retVal['temp_image_dir'] = $file_dir;
		}
		echo json_encode($retVal);
	}

	public function uploadFileValidation(){
		if(!$this->input->is_ajax_request()){
			echo "Method Ilegal";die;
		}

		$mode = $this->input->post("mode");
		$intPublisherID = $this->input->post("intPublisherID");
		$intBookPublisherID = $this->input->post("intBookPublisherID");

		$resUpload = $mode=="contoh" ? $this->buku_model->checkPartialUploadPDF($intPublisherID,$intBookPublisherID) : $this->buku_model->checkCompleteUploadPDF($intPublisherID,$intBookPublisherID);
		$dataVal = $resUpload[0];
		$status = $dataVal['bitSuccess']==1 ? true : false;
		$message = "";
		if($mode=="lengkap"){
			$message = $status==true ? "File Dapat Upload" : "File PDF Lengkap Telah Di Upload. Apakah anda akan mengganti File Tersebut?";
		}else{
			$message = $status==true ? "File Dapat Upload" : "File PDF Parsial Telah Di Upload. Apakah anda akan mengganti File Tersebut?";
		}

		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	public function filingReviewConversionBooks(){
		if(!$this->input->is_ajax_request()){
			echo "Method Ilegal";die;
		}

		$status = false;
		$message = "Pengajuan Gagal";
		$mode = $this->input->post("mode");
		$intPublisherID = $this->input->post("intPublisherID");
		$intBookPublisherID = $this->input->post("intBookPublisherID");
		if($mode=="review"){
			$resVal = $this->buku_model->updateStatusToReview($intPublisherID , $intBookPublisherID);
			$status = $resVal[0]['bitSucces']==1 ? true : false;
			$message = $status == true ? "Pengajuan Review Sukses" : "Pengajuan Review Gagal"; 
		}else if($mode=="konversi"){
			$resVal = $this->buku_model->updateStatusToConvert($intPublisherID , $intBookPublisherID);
			$status = $resVal[0]['bitSucces']==1 ? true : false;
			$message = $status == true ? "Pengajuan Konversi Sukses" : "Pengajuan Konversi Gagal";
		}else{
			$message = "Ilegal Method";
		}
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	public function cancelBooks(){
		if(!$this->input->is_ajax_request()){
			echo "Method Ilegal";die;
		}
		$intPublisherID = $this->input->post("intPublisherID");
		$intBookPublisherID = $this->input->post("intBookPublisherID");
		$resVal = $this->buku_model->deleteBooks($intPublisherID , $intBookPublisherID);
		$status = $resVal[0]['bitSucces']==1 ? true : false;
		$message = $status == true ? "Data Berhasil Di Hapus" : "Data Gagal Di Hapus";
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	public function uploadTempFilePDF(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegall"; die;
		}

		$status = false;
		$message = "";
		$filePDF = $_FILES['filePdf'];
		$real_name = $filePDF['name'];
		$modeForm = $this->input->post("txtModeForm");
		$txtBooksTitle = $this->input->post("txtBooksTitle");
		$intBooksPublisherID = $this->input->post("intBooksPublisherID");
		$txtPublisherName = $this->session->userdata("pcw_penerbit_publisher_name");
		$modeName = $modeForm=="contoh" ? "Parsial" : "Complete";

		///$newFileName = $modeForm."-".$txtBooksTitle."-".$intBooksPublisherID;
		///$newFileName = urlGenerator($newFileName);
		$newFileName = $txtPublisherName."_".$modeName."_".$real_name;
		$config['upload_path'] = TEMP_UPLOAD_DIR;
		$config['allowed_types'] = 'pdf';
		$config['file_name'] = $newFileName;
		$this->upload->initialize($config);
		$resUpload = $this->upload->do_upload("filePdf");
		if(!$resUpload){
			$retVal['message'] = $this->upload->display_errors();
			$retVal['status'] = $resUpload;
			$retVal['real_name_file'] = $real_name;
			$retVal['new_name_file'] = $newFileName;
			echo json_encode($retVal);die;
			///echopre($data_upload);
		}else{
			$data_upload = $this->upload->data();
			$retVal['message'] = "File Sesuai Dengan Format";
			$retVal['file_temp_dir'] = $data_upload['full_path'];
			$retVal['real_name_file'] = $data_upload['client_name'];
			$retVal['new_name_file'] = $data_upload['file_name'];
			$retVal['type_file'] = $data_upload['file_type'];
			$retVal['mode'] = $data_upload['file_type'];
			$fileToUpload = $data_upload['full_path'];
			$newFileName = $data_upload['file_name'];
			$retSendToServer = $this->sendToServerPDF($fileToUpload , $newFileName , $modeForm);
			if($retSendToServer){
				/// Update Status TO Activate This Shit
				$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
				if($modeForm=="contoh"){
					$resUpdateFile = $this->buku_model->updatePartialUploadPDF($intPublisherID , $intBooksPublisherID , $newFileName);
				}else{
					$resUpdateFile = $this->buku_model->updateCompleteUploadPDF($intPublisherID , $intBooksPublisherID , $newFileName);
				}
				$retVal['message'] = "File Berhasil Di Upload";
				$retVal['status'] = true;
			}else{
				$retVal['message'] = "File Gagal Di Upload";
				$retVal['status'] = false;
			}
			$retVal['file_mode'] = $modeName;	
		}
		echo json_encode($retVal);
	}

	public function sendToServerPDF($filePDF , $fileName , $fileMode){

		$this->load->library('ftp');
		$ftpFolderUpload = $this->config->item("ftp_folder_upload");
		$destinationFolder = $ftpFolderUpload[$fileMode]."/";
		$config['hostname'] = $this->session->userdata("pcw_penerbit_ftp_host");
		$config['username'] = $this->session->userdata("pcw_penerbit_ftp_username");
		$config['password'] = $this->session->userdata("pcw_penerbit_ftp_password");
		$config['debug'] = TRUE;
		
		$this->ftp->connect($config);
		
		$retValUpload = $this->ftp->upload($filePDF , $destinationFolder . $fileName);
		
		$this->ftp->close();
		return $retValUpload;
	}

	public function getFormInternalCode(){
		
		///$dt['frmPenerbit'] = $this->form_builder->input
		$listInternalCode = $this->buku_model->getInternalCodePublisher();
		$arrInternalCode = array();
		foreach ($listInternalCode as $rowInternal) {
			# code...
			$arrInternalCode[$rowInternal['txtPublisherServicesID']] = $rowInternal['txtPublisherServicesID'];
		}
		$dt['frmInternalCode'] = $this->form_builder->inputDropdown("Penerbit", "txtPublisherServicesIDInternalCode" , "" , $arrInternalCode);
		$dt['frmISBN'] = $this->form_builder->inputText("ISBN", "txtISBNInternalCode" , "" );
		$dt['frmJudulBuku'] = $this->form_builder->inputText("Judul Buku", "txtBookTitleInternalCode" , "" );

		$this->load->view("buku/form_internal_code" , $dt);
	}

	public function getBooksList(){
		$rowsPerPage = 10;
		$pageNumber = !empty($this->input->post("page")) ? $this->input->post("page") : 1;
		$txtPublisherServicesID = $this->input->post("txtPublisherServicesIDInternalCode");
		$txtISBN = $this->input->post("txtISBNInternalCode");
		$txtBookTitle = $this->input->post("txtBookTitleInternalCode");

		$dataBuku = $this->buku_model->getDataBukuInternalStore($pageNumber , $rowsPerPage, $txtPublisherServicesID , $txtISBN , $txtBookTitle);
		$retVal = array();
		$status = false;
		$htmlRes = "";
		$jumlah = count($dataBuku); 
		if($jumlah > 0){
			$status = true;
			$htmlRes = "";
			foreach ($dataBuku as $rowBuku) {
						# code...
				$htmlRes .='<li class="item">
                  <div class="product-img">
                    <img alt="'.$rowBuku['txtBookTitle'].'" src="'.$rowBuku['txtUrlImage'].'">
                  </div>
                  <div class="product-info">
                    <a class="product-title" href="javascript:void(0)">'.$rowBuku['txtBookTitle'].'</a>
                        <span class="product-description">
                          	<dl class="dl-horizontal">
								<dt>Tahun Terbit</dt>
								<dd>'.$rowBuku['intPublishedYear'].'</dd>
								<dt>ISBN</dt>
								<dd>'.$rowBuku['txtISBN'].'</dd>
								<dt>Bahasa</dt>
								<dd>'.$rowBuku['txtLanguage'].'</dd>
								<dt>Kode Penerbit</dt>
								<dd>'.$rowBuku['txtCodeInternalPublisher'].'</dd>
								<dt>Kode Buku</dt>
								<dd>'.$rowBuku['txtCodeInternalBook'].'</dd>
              				</dl>
                        </span>
						<button class="btn btn-primary" onclick="setInternalCode(\''.$rowBuku['txtCodeInternalPublisher'].'\',\''.$rowBuku['txtCodeInternalBook'].'\')" type="button"><i class="fa fa-check"></i> Pilih</button>
                  </div>
                </li>';
			}
		}
		$retVal['status'] = $status;
		$retVal['htmlRes'] = $htmlRes;
		$retVal['page_next'] = $pageNumber;
		$retVal['count_data'] = $jumlah;
		echo json_encode($retVal);
	}
	
	public function saveBooksData(){
		///Parameter To Post
		if(!$this->input->is_ajax_request()){
			echo "Method Ilegal";die;
		}
		////echopre($_POST);die;
		$intPublisherID = $this->input->post("intPublisherID");
		$txtPublisherName = $this->input->post("txtPublisherName");
		$txtFormMode = $this->input->post("txtFormMode");
		$txtBookTitle = $this->input->post("txtBookTitle");
		$ntxtFrontCoverDir = $this->input->post("ntxtFrontCoverTxtDir");
		$txtFrontCoverFileName = $this->input->post("txtFrontCoverFileName");
		$ntxtBackCoverDir = $this->input->post("ntxtBackCoverDir");
		$txtBackCoverFileName = $this->input->post("txtBackCoverFileName");
		

		$ntxtImageFront = "";
		if(!empty($ntxtFrontCoverDir)){
			///echo $ntxtFrontCoverDir;
			$imgFrontCover = $this->input->post("imgFrontCover");
			$extensionFile = getExtensionFile($imgFrontCover);
			$txtFrontCoverFileName = urlGenerator("front-".$txtPublisherName."-".$txtBookTitle."-1.".$extensionFile); 
			if(($txtFormMode=="update") & (empty($imgFrontCover))){
				$imageFront = explode("/" , $ntxtFrontCoverDir);
				$imageFrontName = explode("-" , $txtFrontCoverFileName);
				$imgFrontCover = end($imageFront);
				$numberImage = end($imageFrontName) + 1; 
				$txtFrontCoverFileName = urlGenerator("front-".$txtPublisherName."-".$txtBookTitle."-".$numberImage);  
			}
			$ntxtImageFront = file_get_contents($ntxtFrontCoverDir);
			$ntxtImageFront = "image/".$extensionFile.";".base64_encode($ntxtImageFront);
			
		}

		$ntxtImageBack = "";
		if(!empty($ntxtBackCoverDir)){
			$imgBackCover = $this->input->post("imgBackCover");
			$extensionFile = getExtensionFile($imgBackCover);
			$txtBackCoverFileName = urlGenerator("back-".$txtPublisherName."-".$txtBookTitle."-1".$extensionFile);
			if(($txtFormMode=="update") & (empty($imgBackCover))){
				$imageBack = explode("/" , $ntxtBackCoverDir);
				$imageBackName = explode("-" , $txtBackCoverFileName);
				$imgBackCover = end($imageBack);
				$numberImage = end($imageBackName) + 1;
				$txtBackCoverFileName = urlGenerator("back-".$txtPublisherName."-".$txtBookTitle."-".$numberImage);
			}
			$ntxtImageBack = file_get_contents($ntxtBackCoverDir);
			$ntxtImageBack = "image/".$extensionFile.";".base64_encode($ntxtImageBack);
		}
		
		$bitAvailableOnline = !empty($this->input->post("bitAvailableOnline")) ? $_POST["bitAvailableOnline"][0] : 0; 
		$bitAvailableOffline = !empty($this->input->post("bitAvailableOffline")) ? $_POST["bitAvailableOffline"][0] : 0;

		$mode = $this->input->post("txtFormMode");
		if($mode=="update"){
			$arrPost = array(
				"intPublisherID" => $this->input->post("intPublisherID"),
				"intPublisherBookID" => $this->input->post("intPublisherBookID"),
				"txtBookTitle" => $this->input->post("txtBookTitle"),
				"txtSubBookTitle" => $this->input->post("txtSubBookTitle"),
				"txtLanguage" => $this->input->post("txtLanguage"),
				"txtPublisherName" => $this->input->post("txtAuthor"),
				"txtImprint" => $this->input->post("txtImprint"),
				"intYear" => $this->input->post("txtYear"),
				"txtEdition" => $this->input->post("txtEdition"),
				"intBookPage" => !empty($this->input->post("intBookPage")) ? $this->input->post("intBookPage") : 0,
				"txtISBN" => $this->input->post("txtISBN"),
				"txtShortDescription" => $this->input->post("txtShortDescription"),
				"txtSynopsis" => $this->input->post("txtSynopsis"),
				"txtKeyWords" => $this->input->post("txtKeyWords"),
				///"curBookPrice" => !empty($this->input->post("curBookPrice")) ? $this->input->post("curBookPrice") : 0,
				"bitAvailableOnline" => $bitAvailableOnline,
				"curBookPriceOnline" => !empty($this->input->post("curBookPriceOnline")) ? $this->input->post("curBookPriceOnline") : 0,
				"bitAvailableOffline" => $bitAvailableOffline,
				"curBookPriceOffline" => !empty($this->input->post("curBookPriceOffline")) ? $this->input->post("curBookPriceOffline") : 0,
				"intCatalogID1" => $this->input->post("intCatalogID1"),
				"intCategoryID1" => $this->input->post("intCategoryID1"),
				"intCatalogID2" => $this->input->post("intCatalogID2"),
				"intCategoryID2" => $this->input->post("intCategoryID2"),
				"intCatalogID3" => $this->input->post("intCatalogID3"),
				"intCategoryID3" => $this->input->post("intCategoryID3"),
				"ntxtFrontCover" => $ntxtImageFront,
				"ntxtBackCover" => $ntxtImageBack,
				"txtCodeInternalPublisher" => $this->input->post("txtCodeInternalPublisher"),
				"txtCodeInternalBook" => $this->input->post("txtCodeInternalBook"),
				"txtEmailAdmin" => $this->session->userdata("pcw_backoffice_penerbit_email"),
				"txtFrontCoverFileName" => $txtFrontCoverFileName,
				"txtBackCoverFileName" => $txtBackCoverFileName,
				"intBookRentDays"=> $this->input->post("intBookRentDays"),
			);
			$resVal = $this->buku_model->updateBooks($arrPost);
		}else{
			$arrPost = array(
				"intPublisherID" => $intPublisherID,
				"txtBookTitle" => $this->input->post("txtBookTitle"),
				"txtSubBookTitle" => $this->input->post("txtSubBookTitle"),
				"txtLanguage" => $this->input->post("txtLanguage"),
				"txtPublisherName" => $this->input->post("txtPublisherName"),
				"txtImprint" => $this->input->post("txtImprint"),
				"intYear" => $this->input->post("txtYear"),
				"txtEdition" => $this->input->post("txtEdition"),
				"intBookPage" => !empty($this->input->post("intBookPage")) ? $this->input->post("intBookPage") : 0,
				"txtISBN" => $this->input->post("txtISBN"),
				"txtShortDescription" => $this->input->post("txtShortDescription"),
				"txtSynopsis" => $this->input->post("txtSynopsis"),
				"txtKeyWords" => $this->input->post("txtKeyWords"),
				"bitAvailableOnline" => $bitAvailableOnline,
				"curBookPriceOnline" => !empty($this->input->post("curBookPriceOnline")) ? $this->input->post("curBookPriceOnline") : 0,
				"bitAvailableOffline" => $bitAvailableOffline,
				"curBookPriceOffline" => !empty($this->input->post("curBookPriceOffline")) ? $this->input->post("curBookPriceOffline") : 0,
				///"curBookPrice" => !empty($this->input->post("curBookPrice")) ? $this->input->post("curBookPrice") : 0,
				"intCatalogID1" => $this->input->post("intCatalogID1"),
				"intCategoryID1" => $this->input->post("intCategoryID1"),
				"intCatalogID2" => $this->input->post("intCatalogID2"),
				"intCategoryID2" => $this->input->post("intCategoryID2"),
				"intCatalogID3" => $this->input->post("intCatalogID3"),
				"intCategoryID3" => $this->input->post("intCategoryID3"),
				"ntxtFrontCover" => $ntxtImageFront,
				"ntxtBackCover" => $ntxtImageBack,
				"txtCodeInternalPublisher" => $this->input->post("txtCodeInternalPublisher"),
				"txtCodeInternalBook" => $this->input->post("txtCodeInternalBook"),
				"txtFrontCoverFileName" => $txtFrontCoverFileName,
				"txtBackCoverFileName" => $txtBackCoverFileName,
				"intBookRentDays"=> $this->input->post("intBookRentDays"),
			);
			$resVal = $this->buku_model->BookRegistration($arrPost);
		}

		$status = $resVal[0]['bitSuccess']==1 ? true : false;
		if($status){
			$message = "Data Berhasil Di Simpan";
			// Hapus Temporary File Front
			if(!empty($ntxtFrontCoverDir)){
				unlink($ntxtFrontCoverDir);
			}
			// Hapus Temporary File Back
			if(!empty($ntxtBackCoverDir)){
				unlink($ntxtBackCoverDir);
			}
		}else{
			$info = $resVal[0]['txtInfo'];
			$message = "Data Gagal Di Simpan ".$info;
		}

		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	function updateBookStatus(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegalll";die;
		}

		$intPublisherID = $this->input->post("intPublisherID");
		$intPublisherBookID = $this->input->post("intPublisherBookID");
		$txtStatusBuku = $this->input->post("txtStatusBuku");

		$resQuery = $this->buku_model->updateBookStatus($intPublisherID , $intPublisherBookID , $txtStatusBuku);
		///echopre($resQuery);
		$resVal = $resQuery[0];
		$status = $resVal['bitSucces']==1 ? true : false;
		$message = $resVal['bitSucces']==1 ? "Status Berhasil Di Update ".$resVal['txtStatus'] : "Status Gagal Di Update ";
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
	}

	function uploadBookProofs(){
		if(!$this->input->is_ajax_request()){
		//	echo "Ilegalll";die;
		}
		$status = false;
		$message = "File gagal di upload";
		$ntxtBookProofQualification1 = "";
		$ntxtBookProofQualification2 = "";
		$ntxtBookProofQualification3 = "";
		$intPublisherBookID = $this->input->post("intPublisherBookID");
		$intPublisherID = $this->input->post("intPublisherID");

		if(!empty($_FILES['fileProof1'])){
			$resUpload1 = $this->uploadFileProofs('fileProof1');
			////echopre($resUpload1);
			if(!empty($resUpload1)){
				$status = $resUpload1['status'];
				if($status){
					$ntxtBookProofQualification1 = $this->generateToBase64($resUpload1['temp_image_dir'] , $resUpload1['extension'] , true);
				}
			}
		}

		if(!empty($_FILES['fileProof2'])){
			$resUpload2 = $this->uploadFileProofs('fileProof2');
			//echopre($resUpload2);
			if(!empty($resUpload2)){
				$status = $resUpload2['status'];
				if($status){
					$ntxtBookProofQualification2 = $this->generateToBase64($resUpload2['temp_image_dir'] , $resUpload2['extension'] , true);
				}
			}
		}

		if(!empty($_FILES['fileProof3'])){
			$resUpload3 = $this->uploadFileProofs('fileProof1');
			///echopre($resUpload3);
			if(!empty($resUpload3)){
				$status = $resUpload3['status'];
				if($status){
					$ntxtBookProofQualification3 = $this->generateToBase64($resUpload3['temp_image_dir'] , $resUpload3['extension'] , true);
				}
			}
		}
		
		if(!empty($ntxtBookProofQualification1) || !empty($ntxtBookProofQualification2) || !empty($ntxtBookProofQualification3)){
			$arrPost = array(
				"intPublisherID" => $intPublisherID,
				"intPublisherBookID" => $intPublisherBookID,
				"ntxtBookProofQualification1" => $ntxtBookProofQualification1,
				"ntxtBookProofQualification2" => $ntxtBookProofQualification2,
				"ntxtBookProofQualification3" => $ntxtBookProofQualification3,
			);
			$resQuery = $this->buku_model->uploadBookProof($arrPost);
			$resVal = $resQuery[0];
			$status = $resVal['bitSuccess']==1 ? true : false;
			$message = $status==true ? "File Berhasil Di Upload" : "File Gagal Di Upload";
		}

		$retVal = array("status" => $status, "message" => $message);
		echo json_encode($retVal);
	}

	private function generateToBase64($file_dir , $extension = "jpg" , $is_delete_temp = false){
		$ntxtImage = file_get_contents($file_dir);
		$resImage = "image/".$extension.";".base64_encode($ntxtImage);
		if($is_delete_temp){
			unlink($file_dir);
		}
		return $resImage;
	}

	private function uploadFileProofs($fileImageName){
		$fileImage = $_FILES[$fileImageName];
		$retVal = array();
		if(!empty($fileImage)){
			$nameFile = $fileImage["name"];
			$postName = getNameFile($nameFile);
			$postName = $postName."proof".date("YmdHis");
			$config['upload_path'] = ASSETS_UPLOAD_DIR;
			$config['allowed_types'] = 'jpg|png';
			$config['file_name'] = $postName;
			$this->upload->initialize($config);
			$resUpload = $this->upload->do_upload($fileImageName);
			if(!$resUpload){
				$retVal['status'] = $resUpload;
				$retVal['message'] = $this->upload->display_errors();
			}else{
				$data_upload = $this->upload->data();
				$file_dir = $data_upload['full_path'];
				$image_temp_url = str_replace(TEMP_UPLOAD_DIR , URL_UPLOAD_DIR , $file_dir);
				$retVal['status'] = true;
				$retVal['message'] = "File Berhasil Di Upload";
				$retVal['temp_image_url'] = $image_temp_url;
				$retVal['temp_image_dir'] = $file_dir;
				$retVal['extension'] = str_replace(".","",$data_upload['file_ext']);

			}
		}
		return $retVal;
	}
}