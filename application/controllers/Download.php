<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller {
    
    var $meta_title = "Image";
    var $meta_desc = "Image";
	var $main_title = "Image";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "K01";

    public function __construct(){
        parent::__construct();
        $this->load->model("buku_model");
        $this->load->library('ftp');
        $this->base_url = $this->base_url_site."image/";
    } 
    
	public function downloadBuku($txtTypeBuku , $intPublisherID,$intPublisherBookID){

        $resData = $this->buku_model->getBookDetail($intPublisherID , $intPublisherBookID);
        $data = $resData[0];
        if(empty($data)){
            show_404();  
        }
        $msgError = "";
        $fileMode = $txtTypeBuku=="parsial" ? "contoh" : "lengkap";
        
        $ftpFolderUpload = $this->config->item("ftp_folder_upload");
		$destinationFolder = $ftpFolderUpload[$fileMode]."/";
		$config['hostname'] = $data['txtFTPHost'];
		$config['username'] = $data['txtFTPUsername'];
		$config['password'] = $data['txtFTPPassword'];
		$config['debug'] = TRUE;
		
		$this->ftp->connect($config);
        $txtBookTitle = $data['txtBookTitle'];
        $txtPublisherName = $data['txtPublisherName'];        
        $pdfFileName = $txtTypeBuku=='parsial' ? $data['txtPdfParsialName'] : $data['txtPdfCompleteName'];
        if(empty($pdfFileName)){
            $msgError = "[Code Error:101][File Name Empty]";
            echo $msgError;die;
        }
        $titleFile = urlGenerator($txtBookTitle) . "_" . urlGenerator($txtPublisherName);
        $linkToDownload = $destinationFolder."/".$pdfFileName;
		header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=".$titleFile.".pdf");
        $resFtp = $this->ftp->download($linkToDownload,'php://output');
        
        if(!$resFtp){
            $msgError = "[Code Error:100][Download Gagal]";
            echo $msgError;die;
        }
        
    }

    function checkDataBuku(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $status = false;
        $message = "Data Buku Tidak Ada";
        $txtTypeBuku = $this->input->post("txtTypeBuku");
        $intPublisherID = $this->input->post("intPublisherID");
        $intPublisherBookID = $this->input->post("intPublisherBookID");

        $resData = $this->buku_model->getBookDetail($intPublisherID , $intPublisherBookID);
        $data = $resData[0];
        $pdfFileName = $txtTypeBuku=='parsial' ? $data['txtPdfParsialName'] : $data['txtPdfCompleteName'];

        $retVal = array();
        if(!empty($pdfFileName)){
            $status = true;
            $message = "Data Buku Ada";
        }
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }
	
}