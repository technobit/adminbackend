<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Katalog extends MY_Controller {
    var $meta_title = "Katalog Buku";
    var $meta_desc = "Katalog Buku";
	var $main_title = "Katalog";
    var $base_url = "";
	var $limit = "10";
	var $status_list = "";
    var $arrBreadcrumbs = "";
    var $response = "";
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."master/import-data-buku/";
        $this->load->model("buku_model");
		$this->load->model("penerbit_model");
        $this->load->model("katalog_model");
		$this->lang->load('id_site', 'id');

        $this->arrBreadcrumbs = array(
                        "Master Data"=> $this->base_url_site,
                        "Katalog"=> $this->base_url
                                ); 
    }

    public function index(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_katalog(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/validate/jquery.validate-add.js",
				ASSETS_JS_URL."master/katalog/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);
    }

    public function form($intCatalogID = ""){

        $detailKatalog = array();
        $titleForm = "Tambah Katalog";
        if(!empty($intCatalogID)){
            $detailKatalog = $this->katalog_model->detail($intCatalogID);
            $titleForm = "Update Katalog";
        }
        ////echopre($detailKatalog);
        $arrInput = array(
            "intCatalogID",
            "txtCatalog"
        );

        foreach($arrInput as $rowInput) {
            $$rowInput = isset($detailKatalog[$rowInput]) ? $detailKatalog[$rowInput] : ""; 
        }

        $dt['intCatalogID'] = $this->form_builder->inputHidden("intCatalogID" , $intCatalogID );
        $dt['txtCatalog'] = $this->form_builder->inputText("Katalog" , "txtCatalog" , $txtCatalog );
        $retVal['form'] = $this->load->view("master/katalog/form" , $dt , true);
        $retVal['title'] = $titleForm;
        echo json_encode($retVal);
    }

    private function _build_katalog(){
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
		//$dt['cmbPublisher'] = form_dropdown("intPublisherID" , $arrListPublisher , "" , "id='intPublisherID' class='form-control'");
        $ret = $this->load->view("master/katalog/content" , $dt , true);
        return $ret;
    }

    public function getCatalog(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }

        $data = $this->katalog_model->retrieve();
        $retVal = array();
        $retVal['data'] = array();
        foreach($data as $rowData) {
            $intCatalogID = $rowData['intCatalogID'];
            $txtCatalog = $rowData['txtCatalog'];
            $btnEdit = '<button type="button" class="btn btn-primary btn-flat" onclick="editCatalog('.$intCatalogID.')"><i class="fa fa-edit"></i> Edit</button>';
            $btnHapus = '<button type="button" class="btn btn-danger btn-flat" onclick="hapusCatalog('.$intCatalogID.' , \''.$txtCatalog.'\')"><i class="fa fa-trash"></i> Hapus</button>';
            $btnAksi = $btnEdit.$btnHapus;
            $arrData = array($txtCatalog , $btnAksi);
            $retVal['data'][] = $arrData;
        }
        echo json_encode($retVal);
    }

    public function saveCatalog(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }
        
        $intCatalogID = $this->input->post("intCatalogID");
        $txtCatalog = $this->input->post("txtCatalog");

        if(!empty($intCatalogID)){
            /// Update 
            $resVal = $this->katalog_model->update($intCatalogID , $txtCatalog);
        }else{
            $resVal = $this->katalog_model->insert($txtCatalog);
        }

        $status = $resVal[0]['bitSuccess'];
        $message = $status==1 ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan (".$resVal[0]['txtInfo'].")";
        $retVal['status'] = $status;  
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }

    public function deleteCatalog(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }
        
        $intCatalogID = $this->input->post("intCatalogID");
        $resVal = $this->katalog_model->delete($intCatalogID);
        $status = $resVal[0]['bitSuccess'];
        $message = $status==1 ? "Data Berhasil Di Hapus" : "Data Gagal Di Hapus (".$resVal[0]['txtInfo'].")";
        $retVal['status'] = $status;  
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }
}