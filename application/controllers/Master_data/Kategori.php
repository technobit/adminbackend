<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kategori extends MY_Controller {
    var $meta_title = "Kategori Buku";
    var $meta_desc = "Kategori Buku";
	var $main_title = "Kategori";
    var $base_url = "";
	var $limit = "10";
	var $status_list = "";
    var $arrBreadcrumbs = "";
    var $response = "";
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."master-data/kategori/";
        $this->load->model("buku_model");
		$this->load->model("penerbit_model");
        $this->load->model("katalog_model");
        $this->load->model("kategori_model");
		$this->lang->load('id_site', 'id');

        $this->arrBreadcrumbs = array(
                        "Master Data"=> $this->base_url_site,
                        "Kategori"=> $this->base_url
                                ); 
    }

    public function index(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_content(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/validate/jquery.validate-add.js",
				ASSETS_JS_URL."master/kategori/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);
    }

    public function form($intCatalogID = ""  , $intCategoryID = ""){

        $detail = array();
        $titleForm = "Tambah Kategori";
        if(!empty($intCategoryID)){
            $detail = $this->kategori_model->detail($intCatalogID , $intCategoryID);
            $titleForm = "Update Kategori";
        }
        ////echopre($detailKatalog);
        $arrInput = array(
            "intCatalogID",
            "txtCategory",
            
        );

        foreach($arrInput as $rowInput) {
            $$rowInput = isset($detail[$rowInput]) ? $detail[$rowInput] : ""; 
        }

        $dataKatalog = $this->katalog_model->retrieve();
        $arrKatalog = array();
        foreach($dataKatalog as $rowData) { 
            $arrKatalog[$rowData['intCatalogID']] = $rowData['txtCatalog'];
        }

        $dt['intCategoryID'] = $this->form_builder->inputHidden("intCategoryID" , $intCategoryID );
        $dt['intCatalogID'] = $this->form_builder->inputDropdown("Katalog" , "intCatalogID" , $intCatalogID , $arrKatalog);
        $dt['txtCatalog'] = $this->form_builder->inputText("Kategori" , "txtCategory" , $txtCategory );
        $retVal['form'] = $this->load->view("master/kategori/form" , $dt , true);
        $retVal['title'] = $titleForm;
        echo json_encode($retVal);
    }

    private function _build_content(){
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
        $arrKatalog = array("0" => "-Pilih Katalog-");
        $dataKatalog = $this->katalog_model->retrieve();
        foreach($dataKatalog as $rowData) { 
            $intCatalogID = $rowData['intCatalogID'];
            $txtCatalog = $rowData['txtCatalog'];
            $arrKatalog[$intCatalogID] = $txtCatalog;
        }
		//$dt['cmbPublisher'] = form_dropdown("intPublisherID" , $arrListPublisher , "" , "id='intPublisherID' class='form-control'");
        $dt['txtCatalog'] = $this->form_builder->inputDropdown("Katalog" , "intCatalogID" , "" , $arrKatalog );
        $ret = $this->load->view("master/kategori/content" , $dt , true);
        return $ret;
    }

    public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }

        $intCatalogID = $this->input->post("intCatalogID");
        $data = $this->kategori_model->retrieve($intCatalogID);
        
        $retVal = array();
        $retVal['data'] = array();
        foreach($data as $rowData) {
            $intCatalogID = $rowData['intCatalogID'];
            $intCategoryID = $rowData['intCategoryID'];
            $txtCatalog = $rowData['txtCatalog'];
            $txtCategory = $rowData['txtCategory'];
            $btnEdit = '<button type="button" class="btn btn-primary btn-flat" onclick="editData('.$intCatalogID.','.$intCategoryID.')"><i class="fa fa-edit"></i> Edit</button>';
            $btnHapus = '<button type="button" class="btn btn-danger btn-flat" onclick="hapusData('.$intCatalogID.','.$intCategoryID.' , \''.$txtCategory.'\')"><i class="fa fa-trash"></i> Hapus</button>';
            $btnAksi = $btnEdit.$btnHapus;
            $arrData = array($txtCatalog , $txtCategory , $btnAksi);
            $retVal['data'][] = $arrData;
        }
        echo json_encode($retVal);
    }

    public function saveData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }
        
        $intCategoryID = $this->input->post("intCategoryID");
        $intCatalogID = $this->input->post("intCatalogID");
        $txtCategory = $this->input->post("txtCategory");

        if(!empty($intCategoryID)){
            /// Update 
            $resVal = $this->kategori_model->update($intCatalogID , $intCategoryID,  $txtCategory);
        }else{
            $resVal = $this->kategori_model->insert($intCatalogID , $txtCategory);
        }

        $status = $resVal[0]['bitSuccess'];
        $message = $status==1 ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan (".$resVal[0]['txtInfo'].")";
        $retVal['status'] = $status;  
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";
            die;
        }
        
        $intCategoryID = $this->input->post("intCategoryID");
        $intCatalogID = $this->input->post("intCatalogID");
        $resVal = $this->kategori_model->delete($intCatalogID , $intCategoryID);
        $status = $resVal[0]['bitSuccess'];
        $message = $status==1 ? "Data Berhasil Di Hapus" : "Data Gagal Di Hapus (".$resVal[0]['txtInfo'].")";
        $retVal['status'] = $status;  
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }
}