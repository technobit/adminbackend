<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller {
    var $meta_title = "Import Data Buku";
    var $meta_desc = "Invoice Perpustakaan";
	var $main_title = "Import";
    var $base_url = "";
	var $upload_dir = TEMP_UPLOAD_DIR;
	var $upload_url = URL_UPLOAD_DIR;
	var $limit = "10";
	var $status_list = "";
    var $arrBreadcrumbs = "";
    var $response = "";
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."master/import-data-buku/";
        $this->load->model("buku_model");
		$this->load->model("penerbit_model");
		$this->lang->load('id_site', 'id');
		$this->load->library('upload');
        $this->load->library('excel');
        ///$this->load->library('PHPExcel/iofactory');

        $this->arrBreadcrumbs = array(
                        "Master Data"=> $this->base_url_site,
                        "Import Data Buku"=> $this->base_url
                                ); 
    }

    public function index(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_import(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/validate/jquery.validate-add.js",
				ASSETS_JS_URL."master/import/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);
    }

    private function _build_import(){
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
        $listPublisher = $this->penerbit_model->getDropdownPublisher();
		$arrListPublisher = array("0" => "-Semua Penerbit-");
        
		foreach ($listPublisher as $valuePublisher) {
			# code...
			$arrListPublisher[$valuePublisher['intPublisherID']] = $valuePublisher['txtPublisherName'];
		}
        
		//$dt['cmbPublisher'] = form_dropdown("intPublisherID" , $arrListPublisher , "" , "id='intPublisherID' class='form-control'");
		$dt['cmbPublisher'] = $this->form_builder->inputDropdown("Penerbit" , "intPublisherID" , "" , $arrListPublisher);
        $dt['linkTemplateDownload'] = $this->base_url_site."assets/doc/TemplateImportBuku.xlsx";
        $ret = $this->load->view("master/import/content" , $dt , true);
        return $ret;
    }

    public function uploadImport(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }
        //echopre($_FILES);
        ///echopre($_POST);
        $txtPublisherID = $this->input->post("txtPublisherID");
        $intPublisherID = $this->input->post("intPublisherID");

        $file_path = "";
        $config['upload_path'] = TEMP_UPLOAD_DIR;
        $config['allowed_types'] = 'xls|xlsx';
        ///echopre($config);  
        $this->upload->initialize($config);
		$resUpload = $this->upload->do_upload("fileExcel");

        if(!$resUpload){
            $retVal['html'] = $this->upload->display_errors();
			$retVal['status'] = $resUpload;
            echo json_encode($retVal);die;
        }else{
            $data_upload = $this->upload->data(); 
            $file_path = $data_upload['full_path'];
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            
            $rowHeader = $allDataInSheet[1];
            unset($allDataInSheet[1]);
            $message = "";
            foreach($allDataInSheet as $rowSheet){
                $message .= "<tr>";
                $txtBookTitle = $rowSheet['A'];
                if(!empty($txtBookTitle)){
                    $arrDataInsert = array(
                    "intPublisherID"=>$intPublisherID,
                    "txtBookTitle"=>$txtBookTitle,
                    "txtSubBookTitle"=>$rowSheet['B'],
                    "txtLanguage"=>$rowSheet['C'],
                    "txtPublisherName"=>$rowSheet['D'],
                    "txtImprint"=>$rowSheet['E'],
                    "intYear"=>$rowSheet['F'],
                    "txtEdition"=>$rowSheet['G'],
                    "intBookPage"=>$rowSheet['H'],
                    "txtISBN"=>$rowSheet['I'],
                    "txtShortDescription"=>$rowSheet['O'],
                    "txtSynopsis"=>$rowSheet['P'],
                    "txtKeyWords"=>$rowSheet['Q'],
                    "bitAvailableOnline"=>$rowSheet['K'],
                    "curBookPriceOnline"=>$rowSheet['L'],
                    "bitAvailableOffline"=>$rowSheet['M'],
                    "curBookPriceOffline"=>$rowSheet['N'],
                    "txtCatalog1"=>$rowSheet['R'],
                    "txtCategory1"=>$rowSheet['S'],
                    "txtCatalog2"=>$rowSheet['T'],
                    "txtCategory2"=>$rowSheet['U'],
                    "txtCatalog3"=>$rowSheet['V'],
                    "txtCategory3"=>$rowSheet['W'],
                    "intBookRentDays"=>$rowSheet['J'],
                    "txtPDFType"=>$rowSheet['X'],
                    );
                    
                    $resVal = $this->buku_model->importBook($arrDataInsert);
                    $statusResult = $resVal[0]['bitSuccess'];
                    $statusMessage = $statusResult == 1 ? "Berhasil Di Masukan" : "Data Gagal Di Simpan (".$resVal[0]['txtInfo'].")";
                    $alertLabel = $statusResult==1 ? "label-success" : "label-danger";
                    $message .= "<td>".$txtBookTitle."</td>";
                    $message .= "<td><div class='label ".$alertLabel."'>".$statusMessage."</div></td>";
                    $message .= "</tr>";
                    $status = true;
                }else{
                    break;
                }
                 
            }
        }
        $retVal = array();
        $retVal['html'] = $message;
        $retVal['status'] = $status;
        echo json_encode($retVal);
    }
}