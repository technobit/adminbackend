<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    var $meta_title = "Dashboard";
    var $meta_desc = "Dashboard";
	var $main_title = "Dashboard";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "D01";
    public function __construct(){
        parent::__construct();
        $this->load->model("dashboard_model");
        $this->base_url = $this->base_url_site."dashboard/";
        
    } 
    
	public function index()
	{
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_home(),
			"custom_js" => array(
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
	
    private function _home(){
        
        $dt = array();
        $dt['BukuDiAjukan'] = $this->dashboard_model->getBukuDiajukan();
        $dt['BukuSiapDiReview'] = $this->dashboard_model->getBukuSiapDiReview();
        $dt['BukuDiReview'] = $this->dashboard_model->getBukuSedangDiReview();
        
        $dt['BukuLolosSeleksi'] = $this->dashboard_model->getBukuLolosSeleksi();
        $dt['BukuTakLolosSeleksi'] = $this->dashboard_model->getBukuTakLolosSeleksi();
        $dt['BukuSiapKonversi'] = $this->dashboard_model->getBukuSiapDiKonversi();
        
        $dt['BukuSedangDiKonversi'] = $this->dashboard_model->getBukuSedangDiKonversi();
        $dt['BukuSiapDiBeli'] = $this->dashboard_model->getBukuSiapDiBeli();
        $dt['InvoiceOnProcess'] = $this->dashboard_model->getInvoiceOnProcess();

        $dt['InvoicePaid'] = $this->dashboard_model->getPaidInvoice();
        $dt['TotalPublisher'] = $this->dashboard_model->getTotalJoinedPublisher();

        $ret = $this->load->view("dashboard/content" , $dt , true);
        return $ret;
    }
	
	

	
	
}
