<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    
    var $meta_title = "User";
    var $meta_desc = "User";
	var $main_title = "User";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "B01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."user/";
        $this->load->model("user_model");
    } 
    
	public function index(){
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_user_list(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
                ASSETS_JS_URL."user/content.js"
                ),
            "custom_css" => array(),
		);	
		$this->_render("default",$dt);	
	}

    private function _build_user_list(){
                
        $arrBreadcrumbs = array(
								"User" => $this->base_url,
								"List User" => "#",
								);
		$dt['url'] = $this->base_url;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("user/list" , $dt , true);
        return $ret;
    }

    public function getUserList(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $retVal = array();
        $retVal['data'] = array();
        $listUser = $this->user_model->getAdminList();
        foreach ($listUser as $rowUser) {
            # code...
            $btnAksi = "";
            $status = $rowUser['bitActive'] == 1 ? "Aktif" : "Non Aktif";
            $btnAksi .= $this->form_builder->inputButton("btnNonActive" , $status=="Aktif" ? "Non Aktifkan" : "Aktifkan" , "btn-sm btn-flat" , "success" , "nonActiveUser('".$rowUser['intBuquStaffID']."')");
            $btnAksi .= $this->form_builder->inputButton("btnEdit" , "Edit" , "btn-sm btn-flat" , "warning" , "frmUser('".$rowUser['intBuquStaffID']."' , 'update')");
            $btnAksi .= $this->form_builder->inputButton("btnDelete" , "Hapus" , "btn-sm btn-flat" , "danger" , "deletUser('".$rowUser['intBuquStaffID']."')");
            $btnAksi .= $this->form_builder->inputButton("btnChangePassword" , "Ubah Password" , "btn-sm btn-flat" , "default" , "frmUser('".$rowUser['intBuquStaffID']."','password')");
            $status = $rowUser['bitActive'] == 1 ? "Aktif" : "Non Aktif"; 
            $userArray = array(
                $rowUser['txtEmail'],
                $rowUser['txtUserName'],
                $status,
                $btnAksi
            );
            $retVal['data'][] = $userArray;
        }
        echo json_encode($retVal);
    }

    public function deleteDataUser(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }
        $intBuquStaffID = $this->input->post("intBuquStaffID");

        $resQuery = $this->user_model->deleteUser($intBuquStaffID);
        $resVal = $resQuery[0];
        $status = $resVal['bitSuccess'] == 1 ? true : false;
        $message = $status==true ? "Data Berhasil Di Hapus" : "Data Gagal Di Hapus";

        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);

    }

    public function nonActivateDataUser(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }
        $intBuquStaffID = $this->input->post("intBuquStaffID");

        $resQuery = $this->user_model->nonActivateAdmin($intBuquStaffID);
        $resVal = $resQuery[0];
        $status = $resVal['bitSuccess'] == 1 ? true : false;
        $message = $status==true ? "User Di Non Aktifkan" : "User Gagal Di Non Aktifkan";

        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);

    }

    public function saveDataUser(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $intBuquStaffID = $this->input->post("intBuquStaffID");
        $frmMode = $this->input->post("frmMode");
        if($frmMode=="update"){
            $arrPost = array(
                "intBuquStaffID" => $intBuquStaffID,
                "txtUserName" => $this->input->post("txtUserName"),
                "txtEmail" => $this->input->post("txtEmail"),
            );
            $resQuery = $this->user_model->UpdateUser($arrPost);
        }else if($frmMode=="insert"){
            $arrPost = array(
                "txtUserName" => $this->input->post("txtUserName"),
                "txtEmail" => $this->input->post("txtEmail"),
                "txtPassword" => $this->input->post("txtPassword"),
            );

            $resQuery = $this->user_model->InsertUser($arrPost);
        }else if($frmMode=="password"){
            $arrPost = array(
                "intBuquStaffID" => $intBuquStaffID,
                "txtEmail" => $this->input->post("txtEmail"),
                "txtOldPassword" => $this->input->post("txtOldPassword"),
                "txtNewPassword" => $this->input->post("txtNewPassword"),
            );
            $resQuery = $this->user_model->UpdateUserPassword($arrPost);
        }

        $resVal = $resQuery[0];
        $status = $resVal['bitSuccess'] == 1 ? true : false;
        $message = isset($resVal['txtInfo']) ? $resVal['txtInfo'] : "";
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan (".$message.")";

        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal); 
    }

    public function getFormUser(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }
        
        $intBuquStaffID = $this->input->post("intBuquStaffID");
        $mode = $this->input->post("mode");;
        $title = "Form Tambah User";
        $detailUser = array();
        if(($mode=="update") || ($mode=="password")){
            $resQuery = $this->user_model->getDetailUser($intBuquStaffID);
            $detailUser = $resQuery[0];
            $title = $mode=="update" ? "Detail User ".$detailUser['txtUserName'] : "Change Password User ".$detailUser['txtUserName'];
        }
        
        $retVal = array();
        $arrRes = array("txtUserName" , "txtEmail");
        foreach ($arrRes as $rowRes) {
            # code...
            $$rowRes = isset($detailUser[$rowRes]) ? $detailUser[$rowRes] : ""; 
        }

        $dt['frmUsername'] = $this->form_builder->inputText("Username" , "txtUserName" , $txtUserName);
        $dt['frmintBuquStaffID'] = $this->form_builder->inputHidden("intBuquStaffID" , $intBuquStaffID);
        $dt['frmEmail'] = $this->form_builder->inputText("Email" , "txtEmail" , $txtEmail);
        $dt['frmPassword'] = $this->form_builder->inputPassword("Password" , "txtPassword" , "");
        $dt['frmRePassword'] = $this->form_builder->inputPassword("Ulangi Password" , "txtPassword2" , "");

        $dt['txtOldPassword'] = $this->form_builder->inputPassword("Password Lama" , "txtOldPassword" , "");
        $dt['txtNewPassword'] = $this->form_builder->inputPassword("Password Baru" , "txtNewPassword" , "");

        $dt['btnInsert'] = $this->form_builder->inputButton("btnSimpan" , $mode=="insert" ? "Simpan" : "Ubah" , "btn-flat" , "success");
        $dt['frmMode'] = $this->form_builder->inputHidden("frmMode" , $mode);
        $dt['mode'] = $mode;
        $html = $this->load->view("user/form" , $dt , true);
        $retVal['title'] = $title;
        $retVal['htmlRes'] = $html;
        echo json_encode($retVal);
    }

}
