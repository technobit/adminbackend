<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$imageBookProof = "bookproof1|bookproof2|bookproof3";
$imageBookCover = "front|back";
/// Penerbit
$route['penerbit/simpan-data'] = 'penerbit/savePenerbit';
$route['penerbit/update-data'] = 'penerbit/savePenerbit';
$route['penerbit/detail/(:num)'] = 'penerbit/form/$1';
$route['penerbit/insert'] = 'penerbit/form';
$route['penerbit'] = 'penerbit/index';

/// Buku
$route['buku/simpan-data'] = 'buku/saveBooksData';
$route['buku/tambah-buku'] = 'buku/form';
$route['buku/edit-buku/(:num)/(:num)'] = 'buku/form/$1/$2';  
$route['buku/get-list-category'] = 'buku/getListCategory';
$route['buku/validasi-upload'] = 'buku/uploadFileValidation';
$route['buku/form-upload-buku/(contoh|lengkap)/(:num)/(:num)'] = 'buku/uploadBuku/$1/$2/$3';
$route['buku/upload-buku-temp'] = 'buku/uploadTempFilePDF';
$route['buku/send-to-server'] = 'buku/bookSendToServer';
$route['buku/hapus-data-buku'] = 'buku/cancelBooks';
$route['buku/download-buku/(parsial|lengkap)/(:num)/(:num)'] = 'download/downloadBuku/$1/$2/$3';
$route['buku/pengajuan-review-konversi'] = 'buku/filingReviewConversionBooks';


/// Image 
$route['image/get-image-bookprof/('.$imageBookProof.')/(:num)/(:num)/(:any).jpg'] = "image/getImageBookProf/$1/$2/$3/$4";
$route['image/get-image-cover/('.$imageBookCover.')/(:num)/(:num)/(:any).jpg'] = "image/getImageCover/$1/$2/$3/$4";
$route['image/download-image-bookprof/('.$imageBookProof.')/(:num)/(:num)/(:any).jpg'] = "image/downloadImageBookProof/$1/$2/$3/$4";
$route['image/download-image-cover/('.$imageBookCover.')/(:num)/(:num)/(:any).jpg'] = "image/downloadImageCover/$1/$2/$3/$4";
$route['image/download-invoice/(:num)/(:num)/(:num)/(:any).jpg'] = "image/downloadInvoice/$1/$2/$3/$4";

/// Invoice
$route['invoice/detail/(:num)/(:num)/(:num)'] = "invoice/detail/$1/$2/$3";
$route['invoice'] = "invoice/index";

/// Master Data
$route['master-data/import-buku'] = "Master_data/import/index";
$route['master-data/import-buku/upload-import-data'] = "Master_data/import/uploadImport";

$route['master-data/katalog/get-katalog'] = "Master_data/katalog/getCatalog";
$route['master-data/katalog/get-form-katalog'] = "Master_data/katalog/form";
$route['master-data/katalog/get-form-katalog/(:num)'] = "Master_data/katalog/form/$1";
$route['master-data/katalog/hapus-katalog'] = "Master_data/katalog/deleteCatalog";
$route['master-data/katalog/simpan-katalog'] = "Master_data/katalog/saveCatalog";
$route['master-data/katalog'] = "Master_data/katalog/index";

$route['master-data/kategori/get-data'] = "Master_data/kategori/getData";
$route['master-data/kategori/get-form'] = "Master_data/kategori/form";
$route['master-data/kategori/get-form/(:num)/(:num)'] = "Master_data/kategori/form/$1/$2";
$route['master-data/kategori/hapus'] = "Master_data/kategori/deleteData";
$route['master-data/kategori/simpan'] = "Master_data/kategori/saveData";
$route['master-data/kategori'] = "Master_data/kategori/index";


///Laporan
$route['laporan/rekap-status-buku'] = "laporan/rekap_status_buku";
$route['laporan/detail-status-buku'] = "laporan/detail_status_buku";
$route['laporan/rekap-penjualan-buku'] = "laporan/rekap_penjualan_buku";

// Login
$route['logout'] = 'login/logout';
$route['registrasi'] = 'penerbit/registrasi';
$route['login/lupa-password'] = 'login/lupa_password';
$route['login'] = 'login/index';

$route['dashboard'] = 'dashboard/index';
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
