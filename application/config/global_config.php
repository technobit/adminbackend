<?php
$config['implementasi_list'] = array("online"=>"Online" , "offline"=>"Offline");
$config['status_list'] = array("1"=>"Aktif" , "0"=>"Non Aktif");
$config['negara_list'] = array("ID"=>"Indonesia" , "My"=>"Malaysia");
$config['status_buku_list'] = array(
	"2"=>"Data Buku Masuk",
	"3"=>"Siap Di Review",
	"4"=>"Sedang Di Review",
	"5"=>"Lolos Seleksi",
	"6"=>"Tidak Lolos Seleksi",
	"7"=>"Siap Di Konversi",
	"8"=>"Sedang Di Konversi",
	"9"=>"Siap Di Beli",
);

$config['month_list'] = array(
	"1"=>"Januari",
	"2"=>"Februari",
	"3"=>"Maret",
	"4"=>"April",
	"5"=>"Mei",
	"6"=>"Juni",
	"7"=>"Juli",
	"8"=>"Agustus",
	"9"=>"September",
	"10"=>"Oktober",
	"11"=>"November",
	"12"=>"Desember",
);

$config['status_invoice_list'] = array(
	"0"=>"Semua",
	"1"=>"Invoice Belum Di Terima",
	"2"=>"Invoice Sedang Di Proses",
	"3"=>"Pembayaran Sudah Di Transfer",	
);

$config['jumlah_deposit_list'] = array(
                                       "100000"=>"100000",
                                       "150000"=>"150000",
                                       "200000"=>"200000",
                                       );

$config['day_month'] = array('1' => '1',
'2' => '2',
'3' => '3',
'4' => '4',
'5' => '5',
'6' => '6',
'7' => '7',
'8' => '8',
'9' => '9',
'10' => '10',
'11' => '11',
'12' => '12',
'13' => '13',
'14' => '14',
'15' => '15',
'16' => '16',
'17' => '17',
'18' => '18',
'19' => '19',
'20' => '20',
'21' => '21',
'22' => '22',
'23' => '23',
'24' => '24',
'25' => '25',
'26' => '26',
'27' => '27',
'28' => '28',
'29' => '29',
'30' => '30'
);

$config['menu_side'] = array("dashboard" =>
									array("class" => "fa fa-home",
										  "desc" => "Dashboard",
										  "main_url" => "dashboard/",
										  "sub_menu" => ""
										  ),
									"penerbit" =>
									array("class" => "fa fa-bank",
										  "desc" => "Penerbit",
										  "main_url" => "penerbit/",
										  "sub_menu" => ""
										  ),
							
                                    "daftar" =>
									array("class" => "fa fa-book",
										  "desc" => "Data Buku",
										  "main_url" => "buku/",
										  "sub_menu" => ""
										  ),
									
									"invoice" =>
									array("class" => "fa fa-money",
										  "desc" => "Invoice",
										  "main_url" => "invoice/",
										  "sub_menu" => ""
										  ),
                                    "admin_user" =>
									array("class" => "fa fa-users",
										  "desc" => "User",
										  "main_url" => "user/",
										  "sub_menu" => ""
										  ),
									"master_data" =>
									array("class" => "fa fa-database",
										  "desc" => "Master Data",
										  "main_url" => "master-data/",
										  "sub_menu" => "sub_master"
										  ),
									"laporan" =>
									array("class" => "fa fa-file-text",
										  "desc" => "Laporan",
										  "main_url" => "laporan/",
										  "sub_menu" => "sub_laporan"
										  ),
									
									
									);
$config['submenu_side'] = array("sub_laporan" => array(
										"penjualan_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "rekap-status-buku/",
											"desc" => "Rekap Status Buku",
										 ),
										"rekap_status_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "detail-status-buku/",
											"desc" => "Detail Status Data Buku",
										),
                                         "rekap_penjualan_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "rekap-penjualan-buku/",
											"desc" => "Rekap Penjualan Buku",
										 ),
									),
								"sub_master" => array(
									"import_buku" => array(
										"class" => "fa fa-book",
										"url" => "import-buku",
										"desc" => "Import Data Buku",
									),
									"katalog_buku" => array(
										"class" => "fa fa-book",
										"url" => "katalog",
										"desc" => "Katalog Buku",
									),
									"kategori_buku" => array(
										"class" => "fa fa-book",
										"url" => "kategori",
										"desc" => "Kategori Buku",
									)
								) 
								);

/// Config Upload Directory
$config['upload_image_path'] = TEMP_UPLOAD_DIR;
$config['extension_image_path'] = 'gif|jpg|png';
$config['ftp_folder_upload'] = array("contoh" => "Parsial" , "lengkap" =>"Complete");