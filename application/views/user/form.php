<div class="clearfix">

<form class="form-horizontal" id="frm-detail-user">
    <?=$frmintBuquStaffID?>
    <?=$frmMode?>
    <?=$frmUsername?>
    <?=$frmEmail?>
    <?php 
        if($mode=="insert") : 
    ?>
    <?=$frmPassword?>
    <?=$frmRePassword?>
    <?php 
        endif;
    ?>

    <?php 
        if($mode=="password") : 
    ?>
    <?=$txtOldPassword?>
    <?=$txtNewPassword?>
    <?=$frmRePassword?>
    <?php 
        endif;
    ?>
    <div id="result-user">

    </div>
    <div class="col-sm-12 text-center">
        <?=$btnInsert?>
    </div>
</form>
</div>


<script>
var modeForm = '<?=$mode?>';
    $(function(){
        
        $('#btnSimpan').click(function(){
            if($('#frm-detail-user').valid()){
                saveDataUser();
            }
        });
        <?php 
        if($mode=="insert") : 
        ?>
         $('#frm-detail-user').validate({
                ignore : "",
                rules : {
                    txtUserName : {
                        required : true,
                    },
                    txtEmail : {
                        required : true,
                    },
                    txtPassword : {
                        required : true,
                    },
                    txtPassword2 : {
                        equalTo : txtPassword
                    }
                }
            });
        <?php 
        elseif($mode=="password"):
        ?>
           $('#frm-detail-user').validate({
                ignore : "",
                rules : {
                    txtOldPassword : {
                        required : true,
                    },
                    txtNewPassword : {
                        required : true,
                    },
                    txtPassword2 : {
                        equalTo : txtNewPassword
                    }
                }
            });
        <?php 
        elseif($mode=="insert"):
        ?>
            $('#frm-detail-user').validate({
                ignore : "",
                rules : {
                    txtUserName : {
                        required : true,
                    },
                    txtEmail : {
                        required : true,
                    },
                }
            });
        <?php 
        endif; 
        ?>
         
    });

    function saveDataUser(){
        $.ajax({
            url : base_url_user+"saveDataUser/",
            type : "POST",
            data : $('#frm-detail-user').serialize(),
            dataType : "html",
            success : function(response){
                var data = jQuery.parseJSON(response);
                var status = data['status'];
                var message = data['message'];
                var resHtml = alertMessage(message , status);
                $('#result-user').html(resHtml);
                
                if(status==true){
                    $('.bootbox').modal("hide");
                    refreshUser();
                }
            } 
        });
    }
</script>