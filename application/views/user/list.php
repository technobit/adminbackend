<section class="content-header">
          <h1 class="title">User</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Pengguna</h3>
                    </div>
                    <div class="box-body">
                    	<div class="pull-left">
                        	<button class="btn btn-success" type="button" id="btnAddUser">Tambah Pengguna</button>
                          <button class="btn btn-info" type="button" id="btnRefreshUser">Refresh</button>
                        </div>
                        <table id="tableUserList" class="table">
                            <thead>
                                <tr>
                                  <th>Email</th>
                                  <th>Nama</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="listUser">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
