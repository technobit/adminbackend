<section class="content-header">
          <h1 class="title">Detail Status Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                    </div>
                    <div class="box-body">
                    <form class="form-horizontal">
                        <?=$cmbPublisher?>
                        <?=$cmbStatus?>
                        <div class="col-md-3 col-md-offset-3">
                            <button class="btn btn-primary btn-flat" type="button" id="btnRekapStatus"><i class="fa fa-send"></i> Cari</button>
                        </div>
                    </form>
                    
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Detail Status Buku</h3>
                        <div class="box-tools">
                            <button class="btn btn-danger btn-flat" type="button" id="btnExportExcel"><i class="fa fa-download"></i> Export Excel</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tableBookStatus" class="table">
                            <thead>
                                <tr>
                                  <th>Penerbit</th>
                                  <th>ISBN</th>
                                  <th>Judul Buku</th>
                                  <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                    </div>
                </div>
                
            </div>
        </div>
</section>
