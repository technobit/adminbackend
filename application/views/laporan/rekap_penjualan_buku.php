<section class="content-header">
          <h1 class="title">Rekap Penjualan Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                    </div>
                    <div class="box-body">
                    <form class="form-horizontal">
                        <?=$cmbPublisher?>
                        <div class="form-group">
                        <label class="col-sm-3 control-label form-label" for="intPublisherID">Tanggal</label>
                            <div class="col-sm-3">
                            	<div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" value="<?=date('Y-m-d')?>" name="dtStart" class="form-control pull-right" id="dtStart">
                                </div>
                            </div>
                            <div class="col-sm-1">
                        		<label class="control-label form-label">s.d.</label>
                            </div>
                            <div class="col-sm-3">
                            	<div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" value="<?=date('Y-m-d')?>" name="dtEnd" class="form-control pull-left" id="dtEnd">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-md-offset-3">
                            <button class="btn btn-primary btn-flat" type="button" id="btnRekapStatus"><i class="fa fa-send"></i> Cari</button>
                        </div>
                    </form>
                    
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Penjualan Buku</h3>
                        <div class="box-tools">
                            <button class="btn btn-danger btn-flat" type="button" id="btnExportExcel"><i class="fa fa-download"></i> Export Excel</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tableBookStatus" class="table">
                            <thead>
                                <tr>
                                  <th>Penerbit</th>
                                  <th>Tahun</th>
                                  <th>Bulan</th>
                                  <th>Jumlah DRM Di Beli</th>
                                  <th>Sub Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                    </div>
                </div>
                
            </div>
        </div>
</section>
