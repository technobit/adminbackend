<style>
.dl-horizontal dt {
  text-align : left;
}
</style>
<div class="clearfix">
<form class="form-horizontal">
    <?=$frmInternalCode?>
    <?=$frmISBN?>
    <?=$frmJudulBuku?>
    <div class="col-sm-3 col-sm-offset-3">
    <button type="button" class="btn btn-success" id="btnCariBuku"> <i class="fa fa-search"></i>
        Cari
    </button>
    </div>
</form>
</div>
<div class="box box-success" style="margin-top: 20px;">
    <div class="box-header">
        <h3 class="box-title">Data Buku</h3>
    </div>
    <div class="box-body">
        <ul class="products-list product-list-in-box" id="list-buku">
            
        </ul>
        <div class="text-center">
            <div style="display:none;" id="progress-bar">
                <h4>Loading</h4>
                <div class="progress progress-sm active" >           
                        <div style="width: 100%" role="progressbar" class="progress-bar progress-bar-success progress-bar-striped">
                        <span class="sr-only">20% Complete</span>
                </div>
                </div>
            </div>
            <input type="hidden" id="noPage" name="noPage" value="1" />
            <button type="button" class="btn btn-flat btn-success" id="btnPrevious" style="display:none;"> <i class="fa fa-arrow-left"></i>
                Sebelumnya
            </button>
            <button type="button" class="btn btn-flat btn-success" id="btnNext" style="display:none;"> <i class="fa fa-arrow-right"></i>
                Selanjutnya
            </button>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#btnCariBuku').click(function(){
            getDataBuku("first");
        });
        $('#btnPrevious').click(function(){
            getDataBuku("before");
        });
        $('#btnNext').click(function(){
            getDataBuku("next");
        });

    });

    function getDataBuku(mode){
            $('#progress-bar').css("display","block");
            var noPage = parseInt($('#noPage').val());
            if(mode=='first'){
                var page = 1;
            }else if(mode=="before"){
                var page = noPage - 1;
            }else{
                var page = noPage + 1;
            }
            
            var txtPublisherServicesID = $('#txtPublisherServicesIDInternalCode').val();
            var txtISBN = $('#txtISBNInternalCode').val();
            var txtBookTitle = $('#txtBookTitleInternalCode').val();

            $.ajax({
                url : global_url + "buku/getBooksList/",
                type : "POST",
                data : "page="+page+"&txtPublisherServicesIDInternalCode="+txtPublisherServicesID+"&txtISBNInternalCode="+txtISBN+"&txtBookTitleInternalCode="+txtBookTitle,
                dataType : "html",
                success : function msg(response){
                    var data = jQuery.parseJSON(response);
                    var status = data['status'];
                    if(status==true){
                        var html = data['htmlRes'];
                        var page_next = data['page_next'];
                        $('#list-buku').html(html);
                        $('#noPage').val(page_next);
                        var jumlah = data['count_data'];
                        if(jumlah > 0){
                            $('#btnNext').css("display","inline");
                            $('#btnPrevious').css("display","inline");
                        }
                        
                    }
                    $('#progress-bar').css("display","none");
                }
            });
    }
    function setInternalCode(txtCodeInternalPublisher , txtCodeInternalBook){
    $('#txtCodeInternalPublisher').val(txtCodeInternalPublisher);
    $('#txtCodeInternalBook').val(txtCodeInternalBook);
    $('.bootbox').modal('hide')
    }
</script>