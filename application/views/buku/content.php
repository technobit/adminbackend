<section class="content-header">
          <h1 class="title">Daftar Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Penerbit <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <?=$cmbPublisher?>
                            </div>
                		</div>
                        <div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Katalog</label>
                            <div class="col-sm-4">
                                <?=$cmbDropdownCatalog?>
                            </div>
                		</div>
						<div class="form-group">    
                  			<label class="col-sm-2 control-label form-label">Kategori</label>
                            <div class="col-sm-4" id="optCategory">
                                <?=$cmbDropdownCategory?>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Status</label>
                            <div class="col-sm-4">
                                <?=$cmbDropdownStatus?>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Judul Buku</label>
                            <div class="col-sm-4">
	                            <input type="text" placeholder="Masukkan Judul Buku" class="form-control" id="txtBookTitle" name="txtBookTitle">
                            </div>
                		</div>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
	                            <button class="btn btn-success" type="button" id="btnSearchBuku"><i class="fa fa-send"></i> Tampilkan</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="box box-primary" id="box-list-buku">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Buku</h3>
                        <div class="box-tools pull-right">
                        <a class="btn btn-primary" type="button" href="<?=$link_registrasi_buku?>"><i class="fa fa-plus"></i> Registrasi Buku Baru</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Cover</th>
                                  <th>Katalog</th>
                                  <th>Kategori</th>
                                  <th>Penerbit</th>
                                  <th>Judul Buku</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="listBookData">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
</section>
