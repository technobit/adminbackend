<section class="content-header">
          <h1 class="title"><?=$title?></h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <?php 
            if($mode=="update") : 
            ?>
            <div class="col-md-12">
                <div class="box box-primary" id="box-detail-buku">
                    <div class="box-header">
                        <h3 class="box-title">Cover Buku</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-1">
                            <?php
                            if(!empty($imageUrlFront)) :  
                            ?>
                            <img src="<?=$imageUrlFront?>" class="img-responsive pull-left" height="100" width="100" />
                            <?php 
                            endif;
                            ?>
                        </div>
                        <?php
                            if(!empty($link_donwload_pdf)) :  
                        ?>
                        <div class="col-md-4">
                            <?=$btnDownload?>
                        </div>
                        <?php 
                            endif;
                        ?>
                    </div>
                </div>
            </div>
            <?php 
            endif; 
            ?>
            <div class="col-md-12">
                <form class="form-horizontal" id="form-tambah-buku">
                <div class="box box-success" id="box-form-tambah-buku">
                    <div class="box-header">
                        <h3 class="box-title">Form Data Buku</h3>
                    </div>
                    <div class="box-body">
                            <?=$intPublisherBookID?>
                            <?=$intPublisherID?>
                            <?=$txtBookTitle?>
                            <?=$txtSubBookTitle?>
                            <?=$txtLanguage?>
                            <?=$txtPublisherName?>
                            <?=$txtAuthor?>
                            <?=$txtImprint?>
                            <?=$txtYear?>
                            <?=$txtEdition?>
                            <?=$intBookPage?>
                            <?=$txtISBN?>
                            <?=$intBookRentDays?>
                            <?=$bitAvailableOnline?>
                            <?=$curBookPriceOnline?>
                            <?=$bitAvailableOffline?>
                            <?=$curBookPriceOffline?>
                    </div>
                </div>
                <div class="box box-primary" id="box-form-deskripsi-buku">
                    <div class="box-header">
                        <h3 class="box-title">Deskripsi Buku</h3>
                    </div>
                    <div class="box-body">
                    <?=$txtShortDescription?>
                    <?=$txtSynopsis?>
                    <?=$txtKeyWords?>
                    </div>
                </div>
                <div class="box box-warning" id="box-form-kategori-buku">
                    <div class="box-header">
                        <h3 class="box-title">Kategori Buku</h3>
                    </div>
                    <div class="box-body">
                    <?=$intCatalogID1?>
                    <?=$intCategoryID1?>
                    <?=$intCatalogID2?>
                    <?=$intCategoryID2?>
                    <?=$intCatalogID3?>
                    <?=$intCategoryID3?>
                    </div>
                </div>
                <div class="box box-danger" id="box-form-tambah-buku">
                    <div class="box-header">
                        <h3 class="box-title">Lampiran File</h3>
                    </div>
                    <div class="box-body">
                    <div class="form-group" id="frontCover">
                                <label class="col-sm-3 control-label form-label">Sampul Depan</label>
                                <div class="col-sm-9">
                                    <?=$ntxtFrontCover?>
                                    <div class="input-group">
                                        <?=form_input("imgFrontCover" , "","class='form-control' id='imgFrontCover' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id="btnImgFrontCover"><i class="fa fa-upload"></i> Upload</button>
                                        </span>
                                    </div>
                                    <input type="file" id="ntxtFrontCover" name="ntxtFrontCover" style="display:none;">
                                    <?=$txtFrontCoverFileName?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label form-label">Sampul Belakang</label>
                                <div class="col-sm-9">
                                    <?=$ntxtBackCover?>
                                    <div class="input-group">
                                        <?=form_input("imgBackCover" , "","class='form-control' id='imgBackCover' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id="btnImgBackCover"><i class="fa fa-upload"></i> Upload</button>
                                        </span>
                                    </div>
                                    <input type="file" id="ntxtBackCover" name="ntxtBackCover" style="display:none;">
                                    <?=$txtBackCoverFileName?>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="box box-default" id="box-form-tambah-buku">
                    <div class="box-header">
                        <h3 class="box-title">Relasi Kode Internal Buku</h3>
                        <div class="box-tools">
                            <button type="button" id="btnInternalCode" class="btn btn-info"><i class="fa fa-search"></i> Cari Kode Internal Buku</button>
                        </div>
                    </div>
                    <div class="box-body">
                    
                    <?=$txtCodeInternalPublisher?>
                    <?=$txtCodeInternalBook?>
                    
                    </div>
                </div>
                <div class="text-center clearfix" style="margin-bottom:12px;">
                    <?=$frmMode?>
                    <button class="btn btn-success" id="btnDaftar" type="button"><i class="fa fa-save"></i> <?=$btnUpdate?></button>
                </div>
                </form>
                <?php 
                    if($mode=="update") : 
                ?>
                <div class="box box-danger" id="box-update-status-buku">
                    <div class="box-header">
                        <h3 class="box-title">Ubah Status Buku</h3>
                    </div>
                    <div class="box-body">
                    <form class="form-horizontal" id="form-update-status-buku">
                    <?=$intPublisherBookID?>
                    <?=$intPublisherID?>
                    <?=$txtCurrentStatus?>
                    <?=$cmbDropdownStatus?>
                    <div class="text-center">
                        <button class="btn btn-primary" id="btnUpdateStatusBuku" type="button"><i class="fa fa-save"></i> Update Status</button>
                    </div>
                    </form>
                    </div>
                </div>
                <div class="box box-danger" id="box-upload-bukti-buku">
                    <div class="box-header">
                        <h3 class="box-title">Upload Bukti Seleksi Buku</h3>
                    </div>
                    <div class="box-body">
                    <form class="form-horizontal" id="form-upload-bukti">
                    <div class="form-group">
                        <label class="col-sm-3 control-label form-label">File 1</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <?=form_input("imgTxtBookProofQualification1" , "","class='form-control' id='imgTxtBookProofQualification1' readonly = 'true'")?>
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-flat" type="button" id="btnBookPoop1"><i class="fa fa-upload"></i> Upload File 1</button>
                                    <?php 
                                        if(!empty($linkDownloadBookProof1)) : 
                                    ?>
                                        <a class="btn btn-warning" href="<?=$linkDownloadBookProof1?>" target="_blank"><i class="fa fa-download"></i> Download File 1</a>
                                    <?php 
                                        endif;
                                    ?>
                                </span>
                                
                            </div>
                            <input type="file" id="ntxtBookProofQualification1" name="ntxtBookProofQualification1" style="display:none;">
                            <p class="help-block">File Harus Berupa Gambar Dengan Ekstensi (png | jpg | jpeg)</p>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label form-label">File 2</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <?=form_input("imgTxtBookProofQualification2" , "","class='form-control' id='imgTxtBookProofQualification2' readonly = 'true'")?>
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-flat" type="button" id="btnBookPoop2"><i class="fa fa-upload"></i> Upload File 2</button>
                                    <?php 
                                        if(!empty($linkDownloadBookProof2)) : 
                                    ?>
                                        <a class="btn btn-warning" href="<?=$linkDownloadBookProof2?>" target="_blank"><i class="fa fa-download"></i> Download File 2</a>
                                    <?php 
                                        endif;
                                    ?>
                                </span>
                                
                            </div>
                            <input type="file" id="ntxtBookProofQualification2" name="ntxtBookProofQualification2" style="display:none;">
                            <p class="help-block">File Harus Berupa Gambar Dengan Ekstensi (png | jpg | jpeg)</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label form-label">File 3</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <?=form_input("imgTxtBookProofQualification3" , "","class='form-control' id='imgTxtBookProofQualification3' readonly = 'true'")?>
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-flat" type="button" id="btnBookPoop3"><i class="fa fa-upload"></i> Upload File 3</button>
                                    <?php 
                                        if(!empty($linkDownloadBookProof3)) : 
                                    ?>
                                        <a class="btn btn-warning" href="<?=$linkDownloadBookProof3?>" target="_blank"><i class="fa fa-download"></i> Download File 3</a>
                                    <?php 
                                        endif;
                                    ?>
                                </span>
                                
                            </div>
                            <input type="file" id="ntxtBookProofQualification3" name="ntxtBookProofQualification1" style="display:none;">
                            <p class="help-block">File Harus Berupa Gambar Dengan Ekstensi (png | jpg | jpeg)</p>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-info" id="btnUploadBukti" type="button"><i class="fa fa-upload"></i> Upload File</button>
                    </div>
                    </form>
                    </div>
                </div>
                <?php 
                    endif;
                ?>
            </div>
        </div>
</section>
