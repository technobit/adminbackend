<!-- Start Page Header -->
  <section class="content-header">
          <h1>
            Dashboard
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
  </section>
  <!-- End Page Header -->
<section class="content">
  <div class="row">
            <div class="col-lg-6 col-xs-4" >
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuDiAjukan?></h3>
                  <p>Buku Di Ajukan</p>
                </div>
                <div class="icon">
                  <i class="fa fa-book"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuSiapDiReview?></h3>
                  <p>Buku Siap Di Review</p>
                </div>
                <div class="icon">
                  <i class="fa fa-check-square-o"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4" >
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuDiReview?></h3>
                  <p>Buku Sedang Di Review</p>
                </div>
                <div class="icon">
                  <i class="fa fa-pencil-square-o"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuLolosSeleksi?></h3>
                  <p>Buku Lolos Seleksi</p>
                </div>
                <div class="icon">
                  <i class="fa fa-check-square"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4" >
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuTakLolosSeleksi?></h3>
                  <p>Buku Tidak Lolos Seleksi</p>
                </div>
                <div class="icon">
                  <i class="fa fa-ban"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuSiapKonversi?></h3>
                  <p>Buku Siap Di Konversi</p>
                </div>
                <div class="icon">
                  <i class="fa fa-sign-out"></i>
                </div>
                
              </div>
            </div><!-- ./col -->

            <div class="col-lg-6 col-xs-4" >
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuSedangDiKonversi?></h3>
                  <p>Buku Sedang Di Konversi</p>
                </div>
                <div class="icon">
                  <i class="fa fa-clone"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$BukuSiapDiBeli?></h3>
                  <p>Buku Siap Di Beli</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-cart"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$InvoiceOnProcess?></h3>
                  <p>Invoice Dalam Proses</p>
                </div>
                <div class="icon">
                  <i class="fa fa-credit-card"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$InvoicePaid?></h3>
                  <p>Invoice Terbayarkan</p>
                </div>
                <div class="icon">
                  <i class="fa fa-money"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-xs-4">
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h3><?=$TotalPublisher?></h3>
                  <p>Jumlah Penerbit</p>
                </div>
                <div class="icon">
                  <i class="fa fa-building"></i>
                </div>
                
              </div>
            </div><!-- ./col -->
            
          </div>
</section>