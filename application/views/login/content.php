<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login Page | Layanan Backoffice Penerbit Penerbit</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-box-body">
        <div class="login-logo">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/Buqu.png" alt="Buqu" width="200">
            <h4><b>Website Backoffice Penerbit</b></h4>
            <!--<br><h5>Version 1.2.3</h5>-->
        </div><!-- /.login-logo -->
        <form action="#" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="<?=label_lang('username_login' , $this)?>" name="email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="<?=label_lang('password_login' , $this)?>" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">  
            <div class="col-xs-12">
              <button type="submit" name="submit" class="btn btn-success btn-block btn-flat"><i class="fa fa-send"></i> Login</button>
              <a href="<?=$forget_link?>" class="btn btn-warning btn-block btn-flat"><i class="fa fa-question"></i> Lupa Password</a>
            </div><!-- /.col -->
          </div>
        </form>
        <div class"supporterIcon" style="margin:15px auto;text-align:center;">
          <h5>Didukung Oleh :</h5>
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/APPTI.png" alt="APPTI" width="40">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/IKAPI.png" alt="IKAPI" width="40">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/Buqu.png" alt="Buqu" width="65">
        </div>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    
    <!-- alert -->
    <?php
      if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == "success") ? 'success' : 'danger';
        $icon = ($alert['status'] == "success") ? 'check' : 'ban';
    ?>
    <div class="modal modal-<?php echo $class_status ?> fade" id="myModal" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title"><span class="icon fa fa-<?php echo $icon ?>"></span> <?php echo $status?></h4>
          </div>
          <div class="modal-body">
            <p><?php echo $message ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    <?php endif; ?>
    <!-- jQuery 2.1.3 -->
    <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=ASSETS_URL?>custom/js/login/forget.js"></script>
  </body>
</html>