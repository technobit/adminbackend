<form class="form-horizontal" method="POST" id="frm-category">
    <?=$intCategoryID?>
    <?=$intCatalogID?>
    <?=$txtCatalog?>
    <div class="text-center">
        <button type="button" id="btnSimpan" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </div>
</form>
<script>
    $(function(){
        $('#frm-category').validate({
            ignore : "",
            rules : {
                txtCategory : {
                    required : true,
                }
            }
        });

        $('#btnSimpan').click(function(){
            if($('#frm-category').valid()){
                saveData();
            }
        });
    });

    function saveData(){
        $.ajax({
            url : global_url + "master-data/kategori/simpan",
            data : $('#frm-category').serialize(),
            type : "POST",
            success : function(result){
                var data = jQuery.parseJSON(result);
                var status = data['status']; 
                var message = data['message'];
                alertPopUp(status , message , "");
                refreshData();
                $('.bootbox').modal("hide");
            }
        });
    }
</script>