<section class="content-header">
          <h1 class="title">Daftar Kategori</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
                        <?=$txtCatalog?>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
	                            <button class="btn btn-success" type="button" onclick="refreshData()" id="btnSearchKategori"><i class="fa fa-search"></i> Cari</button>
                                <button class="btn btn-primary" type="button" id="btnAddKategori"><i class="fa fa-plus"></i> Tambah Kategori</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="box box-primary" id="box-list-publisher">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Kategori</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableCategory" class="table">
                            <thead>
                                <tr>
                                  <th>Katalog</th>
                                  <th>Kategori</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
</section>
