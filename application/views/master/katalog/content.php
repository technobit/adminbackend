<section class="content-header">
          <h1 class="title">Daftar Katalog</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary" id="box-list-publisher">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Katalog</h3>
                        <div class="box-tools pull-right">
                        <button class="btn btn-primary" type="button" id="btnAddKatalog"><i class="fa fa-plus"></i> Tambah Katalog</button>
                        <button class="btn btn-success" type="button" onclick="refreshData()"  id="btnRefreshKatalog"><i class="fa fa-refresh"></i> Refresh Katalog</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tableCatalog" class="table">
                            <thead>
                                <tr>
                                  <th>Katalog</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
