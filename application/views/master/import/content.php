<section class="content-header">
          <h1 class="title">Impor Data Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-import-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Form Upload</h3>
                    </div>
                    <div class="box-body">
						<?=$cmbPublisher?>
                        <div class="form-group">
                                <label class="col-sm-3 control-label form-label">File Excel</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <?=form_input("fileExcelName" , "","class='form-control' id='fileExcelName' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id="btnFileExcel"><i class="fa fa-upload"></i> Upload</button>
                                        </span>
                                    </div>
                                    <input type="file" id="fileExcel" name="fileExcel" style="display:none;">    
                                </div>
                            </div>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-3">
	                            <button class="btn btn-success" type="button" id="btnStartImport"><i class="fa fa-send"></i> Proses Data</button>
                                <a class="btn btn-warning" href="<?=$linkTemplateDownload?>"><i class="fa fa-file-excel-o"></i> Download Template File</a>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="box box-primary" id="box-list-buku">
                    <div class="box-header">
                    	<h3 class="box-title">Hasil Upload</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Nama Buku</th>
                                  <th>Status Upload</th>
                                </tr>
                            </thead>
                            <tbody id="listBookData">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
</section>
