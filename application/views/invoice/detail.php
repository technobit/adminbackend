<section class="content-header">
          <h1 class="title">Detail Invoice</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Invoice</h3>
                    </div>
                    <div class="box-body">
                    <div class="col-sm-6">    
                        <?php
                            if($statusInvoice!=1) : 
                        ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Tanggal Invoice</label>
                                <div class="col-sm-8">
                                    <input type="hidden" id="intMonth" value="<?=$detailInvoice['intMonth']?>">
                                    <input type="hidden" id="intPublisherID" value="<?=$detailInvoice['intPublisherID']?>">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtIndonesianDate']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">No. Invoice</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtInvoiceNumber']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Tahun</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="intYear" readonly="" value="<?=$detailInvoice['intYear']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Bulan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtMonth']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Total DRM Terjual</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['intCopyLicense']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Total Nilai Penjualan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['curTotalPurchase']?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Status Penagihan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtStatus']?>">
                                </div>
                            </div>
                            
                            <?php 
                            else : 
                            ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Tahun</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['intYear']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Bulan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtMonth']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Status Penagihan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$detailInvoice['txtStatus']?>">
                                </div>
                            </div>
                            <?php
                            endif;
                            ?>
                        </div>    
                        <?php
                            
                            if($statusInvoice!=1) : 
                        ?>
                        <div class="col-sm-12 text-right">
                                <?php if(!empty($detailInvoice['linkDownload'])) : ?>
								<a class="btn btn-warning btn-flat" href="<?=$detailInvoice['linkDownload']?>" target="_blank"><i class="fa fa-download"></i> Download Invoice</a>
                                <?php endif;?>
                                <?php if($statusInvoice==2) : ?>
								<button class="btn btn-info btn-flat" type="button" onclick="updateStatusInvoice('<?=$detailInvoice['intPublisherID']?>','<?=$detailInvoice['intYear']?>','<?=$detailInvoice['intMonth']?>')"><i class="fa fa-money"></i> Sudah Di Bayarkan</button>
                                <?php endif;?> 
                            </div>
                        <?php
                            endif;
                        ?>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Buku Terjual</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableDataBookSold" class="table">
                            <thead>
                                <tr>
                                  <th>Tanggal</th>
                                  <th>Nama Perpustakaan</th>
                                  <th>Judul Buku</th>
                                  <th>Harga DRM</th>
                                  <th>Jumlah DRM Dibeli</th>
                                  <th>Sub Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody id="detailInvoicePurchase">

                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>