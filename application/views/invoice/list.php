<section class="content-header">
          <h1 class="title">Invoice</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title">Invoice</h3>
                        </div>
                        <div class="box-body">
                            <form class="form-horizontal" id="frm-filter-invoice">
                                <?=$frmListPublisher?>
                                <?=$frmListStatus?>
                                <div class="col-sm-offset-3 col-sm-7">
                                    <button class="btn btn-primary" id="btnInvoiceFilter" type="button"><i class="fa fa-search"></i> Cari</button>
                                </div>
                            </form>            
                        </div>
                    </div>
                
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Invoice</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableList" class="table">
                            <thead>
                                <tr>
                                  <th>Tahun</th>
                                  <th>Bulan</th>
                                  <th>Penerbit</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="listInvoice">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                
            </div>
        </div>
</section>
