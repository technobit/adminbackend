<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BuquDispenser | Registration Publisher</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <script>
      var global_url = '<?=base_url()?>';
  </script>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="#"><b>Penerbit.Buqu</b>Dispenser</a>
        <br><h5>Version 1.0.3</h5>
      </div>

      <div class="register-box-body col-md-12">
        <p class="login-box-msg">Registrasi Penerbit</p>
        <form action="" method="post" id="frm-registrasi-penerbit">
          <?=$txtPublisherGroup?>
          <?=$txtPublisherName?>
          <?=$txtCountry?>
          <?=$txtProvince?>
          <?=$txtCity?>
          <?=$txtAddress?>
          <?=$txtPostalCode?>
          <?=$txtEmail?>
          <?=$txtPhoneNumber?>
          <?=$txtFax?>
          <?=$txtURLWebsite?>
          <?=$txtAnggota?>
          <div>
            <p class="login-box-msg">Informasi Administrator</p>
          <?=$txtUsername?>
          <?=$txtEmailAdmin?>
          <?=$txtPassword?>
          </div>
          <div class="row">
            <div id="result-registration-container"></div>
            <div class="col-xs-12">
              <button type="button" id="btnSaveRegistration" class="btn btn-primary btn-block btn-flat"><i class="fa fa-send"></i> Register</button>
              <a href="<?=$cancel_link?>" class="btn btn-warning btn-block btn-flat"><i class="fa fa-ban"></i> Batal</a>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.form-box -->  
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 -->
    
    <script src="<?=ASSETS_URL?>plugins/validate/jquery.validate_1.11.1.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=ASSETS_JS_URL?>penerbit/registrasi.js"></script>
    <!-- iCheck -->
  </body>
</html>

