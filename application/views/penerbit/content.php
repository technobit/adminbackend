<section class="content-header">
          <h1 class="title">Daftar Penerbit</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Nama Penerbit</label>
                            <div class="col-sm-8">
                                <?=$frmPublisherName?>
                            </div>
                		</div>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
	                            <button class="btn btn-success" type="button" id="btnSearchPenerbit"><i class="fa fa-search"></i> Cari</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary" id="box-list-publisher">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Penerbit</h3>
                        <div class="box-tools pull-right">
                        <a class="btn btn-primary" type="button" href="<?=base_url()?>penerbit/insert/"><i class="fa fa-plus"></i> Registrasi Penerbit</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tablePublisherList" class="table">
                            <thead>
                                <tr>
                                  <th>Nama Penerbit</th>
                                  <th>Provinsi</th>
                                  <th>Kota</th>
                                  <th>Kelompok Anggota</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
