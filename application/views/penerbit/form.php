<section class="content-header">
          <h1 class="title">Profil Penerbit</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-success" id="box-form-input">
                    <div class="box-header">
                        <h3 class="box-title">Informasi Penerbit</h3>
                    </div>
                    <div class="box-body">
                            <?=$intPublisherID?>
                            <?=$txtPublisherGroup?>
                            <?=$txtPublisherName?>
                            <?=$txtInternalCode?>
                            <?=$txtAddress?>
                            <?=$txtCountry?>
                            <?=$txtProvince?>
                            <?=$txtCity?>
                            <?=$txtPostalCode?>
                            <?=$txtPhoneNumber?>
                            <?=$txtFax?>
                            <?=$txtEmail?>
                            <?=$txtURLWebsite?>
                            <div class="form-group">
                            <label class="col-sm-3 control-label form-label" for="txtPublisherName">Anggota</label>
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=$bitIKAPICheck?>
                                </label>
                                <label>
                                    <?=$bitAPPTICheck?>
                                </label>
                                <label>
                                    <?=$bitOtherCheck?>
                                </label>
                                </div>
                                
                            </div>
                            <div class="col-sm-3">
                            <div class="input-group">
                                <?=$bitOtherText?>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
                <div class="box box-success" id="box-kontak-pimpinan">
                    <div class="box-header">
                        <h3 class="box-title">Kontak Pimpinan</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtNPWPOwner?>
                        <?=$txtContactOwner?>
                        <?=$txtPhoneNumberOwner?>
                        <?=$txtEmailOwner?>
                    </div>
                </div>
                <div class="box box-success" id="box-kontak-1">
                    <div class="box-header">
                        <h3 class="box-title">Kontak 1</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtContact1?>
                        <?=$txtPhoneNumber1?>
                        <?=$txtEmail1?>
                    </div>
                </div>
                <div class="box box-success" id="box-kontak-1">
                    <div class="box-header">
                        <h3 class="box-title">Kontak 2</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtContact2?>
                        <?=$txtPhoneNumber2?>
                        <?=$txtEmail2?>
                    </div>
                </div>
                <div class="box box-success" id="box-rekomendasi-bank">
                    <div class="box-header">
                        <h3 class="box-title">Bank Rekomendasi</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtBankName?>
                        <?=$txtBankAccount?>
                        <?=$txtBankUserName?>
                    </div>
                </div>
                <div class="box box-default" id="box-lain-bank">
                    <div class="box-header">
                        <h3 class="box-title">Bank Lainnya</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtBankNameOps?>
                        <?=$txtNoBankAccountOps?>
                        <?=$txtBankUserNameOps?>
                        <div id="result-update-container">
                            
                        </div>
                        
                    </div>
                </div>
                <?php 
                
                if($txtFormMode=="edit") : 
                ?>
                <div class="box box-warning" id="box-save">
                    <div class="box-header">
                        <h3 class="box-title">Kontrak</h3>
                    </div>
                    <div class="box-body">
                        <?=$bitPublisherContractSign?>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-primary btn-flat" id="btnUpdatePenerbit"><i class="fa fa-send"></i> Simpan</button>
                        </div>
                    </div>
                </div>
                <?php 
                else : 
                ?>
                <div class="box box-warning" id="box-save">
                    <div class="box-header">
                        <h3 class="box-title">Administrator Penerbit</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtUserName?>
                        <?=$txtEmailAdmin?>
                        <?=$txtPassword?>
                        <?=$txtReTypePassword?>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-primary btn-flat" id="btnUpdatePenerbit"><i class="fa fa-send"></i> Simpan</button>
                        </div>
                    </div>
                </div>
                <?php 
                endif;
                ?>
                </form>
            </div>
        </div>
</section>
