<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Soap_Library
{
   public $params_result = array("CallSpExcecution"=>"CallSpExcecutionResult",
                                  "CallSpGetBookList"=>"CallSpGetBookListResult",
                                  "CallSpGetBookDetail"=>"CallSpGetBookDetailResult",
                                  "CallSpBackofficePublisherGetBookList" => "CallSpBackofficePublisherGetBookListResult"
                                  );
   function __construct()
   {
       require_once('lib/nusoap.php');
   }
   
   function soap_request($service , $params , $row_array = false){
        
        $api_url = API_URL;
        $client = new nusoap_client($api_url, 'wsdl');
        $error = $client->getError();
        if($error){
            echo "\nSOAP Error\n".$error."\n";
            return false;
        }else{
            $result = $client->call($service, $params);
            if ($client->fault)
            {
                print_r($result);
                return false;
            }else
            {  
                
                ///$result_arr = json_decode($result[$this->params_result[$service]], true);
                ///$result_arr = json_decode($result['CallSpExcecutionResult'] , true);
                ///echopre($result_arr);
                $result_string = $result[$this->params_result[$service]];
                $result_string = preg_replace("/[\r\n]+/", " ", $result_string);
                $result_string = utf8_encode($result_string);
                $result_arr = json_decode($result_string , true);
                ///echo $result_string;
                if($row_array==true){
                    $retVal = !empty($result_arr) ? $result_arr[0] : array();
                    return $result_arr; 
                }else{
                    $retVal = !empty($result_arr) ? $result_arr : array();
                    return $retVal;
                }
                
            }
        }
   }
   
   function set_parameter( $sp_name , $post){
        $string_params = "";
        $retVal = array();
        if(!empty($post) && !empty($sp_name)){
            foreach($post as $params => $values){
                $string_params .=$params."#".$values."~";
            }
            $string_params = substr($string_params,0,-1);
            $retVal['txtSPName'] = $sp_name;
            $retVal['txtParamValue'] = $string_params;
            return $retVal;
        }else if(!empty($sp_name)){
            $retVal['txtSPName'] = $sp_name;
            $retVal['txtParamValue'] = "";
            return $retVal;
        }else{
            return false;
        }
   }
}