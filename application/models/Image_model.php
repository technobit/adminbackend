<?php 

class Image_model extends MY_Model{
        #code
        
        function __construct(){
            
        }

        function getImageBookProof($intPublisherID, $intPublisherBookID){
            $sp_name = "BackofficePublisher_getImageBookProofQualification";
            $arrPost = array();
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getImageBookCover($intPublisherID, $intPublisherBookID){
            $sp_name = "BackofficePublisher_getImageCoverBook";
            $arrPost = array();
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

}
?>