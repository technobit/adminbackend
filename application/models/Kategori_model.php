<?php

class Kategori_model extends MY_Model{
        #code
        function __construct(){
            
        }
        
        function retrieve($intCatalogID){
            $sp_name = "BackofficePublisher_CategoryRetrieve";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function detail($intCatalogID , $intCategoryID){
            $sp_name = "BackofficePublisher_CategoryRetrieveDetail";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $arrPost['intCategoryID'] = $intCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);
            $retVal = $retVal[0];    
            return $retVal;
        }

        function delete($intCatalogID , $intCategoryID){
            $sp_name = "BackofficePublisher_CategoryDelete";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $arrPost['intCategoryID'] = $intCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function insert($intCatalogID ,$txtCategory){
            $sp_name = "BackofficePublisher_CategoryInsert";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $arrPost['txtCategory'] = $txtCategory;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function update($intCatalogID , $intCategoryID ,$txtCategory){
            $sp_name = "BackofficePublisher_CategoryUpdate";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $arrPost['intCategoryID'] = $intCategoryID;
            $arrPost['txtCatalog'] = $txtCategory;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
}