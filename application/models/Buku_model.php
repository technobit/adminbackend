<?php

    class Buku_model extends MY_Model{
        #code
        
        function __construct(){
            
        }

        function getInternalCodePublisher(){
            $sp_name = "BackOfficePublisher_InternalCodePublisherList";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDataBukuInternalStore($pageNumber , $rowsPerPage , $txtPublisherServicesID , $txtISBN , $txtBookTitle){
            $sp_name = "BackofficePublisher_GetDataBukuOnInternalStore";
            $arrPost = array();
			$arrPost['intPageNumberPaging'] = $pageNumber;
            $arrPost['intRowsPerPage'] = $rowsPerPage;
            $arrPost['txtPublisherServicesID'] = $txtPublisherServicesID;
            $arrPost['txtISBN'] = $txtISBN;
            $arrPost['txtBookTitle'] = $txtBookTitle;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpBackofficePublisherGetBookList");    
            return $retVal;
        }

        function getListPublisher(){
            $sp_name = "BackOfficePublisher_DropDownPublisherList";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        
        function getListCatalog(){
            $sp_name = "PublisherDisp_CatalogRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListCategory($intCatalogID){
            $sp_name = "PublisherDisp_CategoryRetrieve";
            $arrPost = array();
			$arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListBookStatus(){
            $sp_name = "PublisherDisp_BookStatusRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getBookList($intPublisherID,$intCatalogID,$intCategoryID,$intStatusID,$txtBookTitle){
            $sp_name = "BackOfficePublisher_BookRegistrationList";
            $arrPost = array();
			$arrPost['intPublisherID'] = !empty($intPublisherID) ? $intPublisherID : 0;
			$arrPost['intCatalogID'] = !empty($intCatalogID) ? $intCatalogID : 0;
			$arrPost['intCategoryID'] = !empty($intCategoryID) ? $intCategoryID : 0;
			$arrPost['intStatusID'] = !empty($intStatusID) ? $intStatusID : 0;
			$arrPost['txtBookTitle'] = $txtBookTitle;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getBookDetail($intPublisherID,$intPublisherBookID){
            $sp_name = "BackofficePublisher_BookDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function BookRegistration($arrPost){
            $sp_name = "BackofficePublisher_BookRegistration";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateBooks($arrPost){
            $sp_name = "BackofficePublisher_BookUpdate";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getBookStatus($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_BookDetailStatusAndInfo";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function checkPartialUploadPDF($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_ValidationBookParsialUpload";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function checkCompleteUploadPDF($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_ValidationBookCompleteUpload";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updatePartialUploadPDF($intPublisherID , $intPublisherBookID , $txtFileNamePDF){
            $sp_name = "PublisherDisp_UpdateBookPDFParsialName";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $arrPost['txtPDFParsialName'] = $txtFileNamePDF;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateCompleteUploadPDF($intPublisherID , $intPublisherBookID , $txtFileNamePDF){
            $sp_name = "PublisherDisp_UpdateBookPDFCompleteName";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $arrPost['txtPDFCompleteName'] = $txtFileNamePDF;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateStatusToReview($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_UpdateBookStatusToAlreadyReview";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateStatusToConvert($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_UpdateBookStatusToAlreadyKonversi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function deleteBooks($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_DeletePublisherBook";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateBookStatus($intPublisherID , $intPublisherBookID , $intStatusID){
            $sp_name = "BackofficePublisher_BookUpdateStatus";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $arrPost['intStatusID'] = $intStatusID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function uploadBookProof($arrPost){
            $sp_name = "BackofficePublisher_UploadBookProofQualification";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function importBook($arrPost){
            $sp_name = "PublisherDisp_BookRegistrationImport";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

	}