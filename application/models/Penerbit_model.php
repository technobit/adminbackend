<?php

    class Penerbit_model extends MY_Model{
        #code
        
        function __construct(){
            
        }

        function getDropdownPublisher(){
            $sp_name = "BackOfficePublisher_DropDownPublisherList";
            $arrPost =  array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }

        function getListPublisher($publisherName){
            $sp_name = "BackOfficePublisher_PublisherList";
            $arrPost['txtPublisherName'] = $publisherName;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }

        function getListInternalCode(){
            $sp_name = "BackOfficePublisher_InternalCodePublisherList";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }

        function setStatusActivePublisher($intPublisherID , $status = true){
            $sp_name = $status==true ? "BackOfficePublisher_PublisherSetStatusActive" : "BackOfficePublisher_PublisherSetStatusNonAktif"; 
            ///$sp_name = "BackOfficePublisher_PublisherSetStatusNonAktif";
            $arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }

        function getDetailPublisher($intPublisherID){
            $sp_name = "BackOfficePublisher_PublisherProfileRetrieve";
            $arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function registrationPubliser($arrPost){
            $sp_name = "BackOfficePublisher_Registration";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function detailPenerbit($publisherId){
            $sp_name = "PublisherDisp_PublihserProfileRetrieve";
            $arrPost['intPublisherID'] = $publisherId;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        
        function updatePenerbit($arrPost){
            $sp_name = "BackOfficePublisher_UpdateProfile";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function getListBank(){
            $sp_name = "PublisherDisp_ListOfBank";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        
    }
    
