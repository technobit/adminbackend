<?php

class Katalog_Model extends MY_Model{
        #code
        function __construct(){
            
        }
        
        function DetailKatalogLibrary($id){
            $sp_name = "LibDisp_Internal_LibraryCatalogAndCategoryList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function BookRestrictionCategory($intLibraryID , $intLibraryCategoryID){
            $sp_name = "LibDisp_Internal_CategoryBookRestricionList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['intLibraryCategoryID'] = $intLibraryCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function BookRestrictionList($intLibraryID , $txtPublisherServicesID, $txtBookID){
            $sp_name = "LibDisp_Internal_BookRestricionList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['txtPublisherServicesID'] = $txtPublisherServicesID;
            $arrPost['txtBookID'] = $txtBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListBukuByKatalog($intLibraryID, $intCatalogID , $intLibraryCategoryID){
            $sp_name = "LibDisp_Internal_ListBookOnCategoryInLibrary";
            $service_name = "CallSpGetBookList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['intLibraryCatalogID'] = $intCatalogID;
            $arrPost['intLibraryCategoryID'] = $intLibraryCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , $service_name);    
            return $retVal;
        }
        
        function DetailBukuByID($txtPublisherServicesID, $txtBookID){
            $sp_name = "LibDisp_Internal_DetailBook";
            $service_name = "CallSpGetBookDetail";
            $arrPost = array();
            $arrPost['txtPublisherServicesID'] = $txtPublisherServicesID;
            $arrPost['txtBookID'] = $txtBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , $service_name);    
            return $retVal;
        }
        
        function HistoryLicenseBook($post){
            $sp_name = "LibDisp_Internal_BuyLicensiBookHistory";
            $arrPost = array();
            $arrPost['intPageNumber'] = $post['intPageNumber'];
            $arrPost['intRowsPerPage'] = $post['intRowsPerPage'];
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtPublisherServicesID'] = $post['txtPublisherServicesID'];
            $arrPost['txtBookID'] = $post['txtBookID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function retrieve(){
            $sp_name = "BackofficePublisher_CatalogRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function detail($intCatalogID){
            $sp_name = "BackofficePublisher_CatalogRetrieveDetail";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);
            $retVal = $retVal[0];    
            return $retVal;
        }

        function delete($intCatalogID){
            $sp_name = "BackofficePublisher_CatalogDelete";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function insert($txtCatalog){
            $sp_name = "BackofficePublisher_CatalogInsert";
            $arrPost = array();
            $arrPost['txtCatalog'] = $txtCatalog;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }

        function update($intCatalogID , $txtCatalog){
            $sp_name = "BackofficePublisher_CatalogUpdate";
            $arrPost = array();
            $arrPost['intCatalogID'] = $intCatalogID;
            $arrPost['txtCatalog'] = $txtCatalog;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
}