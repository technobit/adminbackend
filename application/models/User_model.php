<?php

    class User_Model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function get_user_login($username,$password ){
            $sp_name = "backOfficePublisher_Login";
            $arrPost['txtEmail'] = $username;
            $arrPost['txtPassword'] = $password;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function get_forget_password($email , $publisher){
            $sp_name = "BackOfficePublisher_ResetPassword";
            $arrPost['txtEmail'] = $email;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getAdminList(){
            $sp_name = "BackofficePublisher_AdminList";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailUser($intBuquStaffID){
            $sp_name = "BackofficePublisher_AdminDetail";
            $arrPost = array();
            $arrPost['intBuquStaffID'] = $intBuquStaffID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function nonActivateAdmin($intBuquStaffID){
            $sp_name = "BackofficePublisher_UpdateAdminStatusNonAktif";
            $arrPost = array();
            $arrPost['intBuquStaffID'] = $intBuquStaffID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function deleteUser($intBuquStaffID){
            $sp_name = "BackofficePublisher_AdminDelete";
            $arrPost = array();
            $arrPost['intBuquStaffID'] = $intBuquStaffID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function InsertUser($arrPost){
            $sp_name = "BackofficePublisher_UserAdminInsert";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function UpdateUser($arrPost){
            $sp_name = "BackofficePublisher_UpdateAdminInfo";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function UpdateUserPassword($arrPost){
            $sp_name = "BackofficePublisher_ChangeAdminPassword";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        

    }
?>