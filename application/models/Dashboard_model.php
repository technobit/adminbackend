<?php

    class Dashboard_model extends MY_Model{
        #code
        
        function __construct(){
            
        }

        public function getBukuDiajukan(){
            $sp_name = "BackOfficePublisher_DashboardBukuDiajukan";
            $arrPost = array();
			///$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuDiajukan'];
            return $retVal;
        }

        public function getBukuSiapDiReview(){
            $sp_name = "BackOfficePublisher_DashboardBukuSiapDireview";
            $arrPost = array();
			///$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiReview'];
            //echopre($retVal);
            return $retVal;
        }

        public function getBukuSedangDiReview(){
            $sp_name = "BackOfficePublisher_DashboardBukuSedangDireview";
            $arrPost = array();
			///$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSedangReview'];
            return $retVal;
        }

        public function getBukuLolosSeleksi(){
            $sp_name = "BackOfficePublisher_DashboardBukuLolosSeleksi";
            $arrPost = array();
			
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuLolosSeleksi'];
            ///echopre($retVal);
            return $retVal;
        }

        public function getBukuTakLolosSeleksi(){
            $sp_name = "BackOfficePublisher_DashboardBukuTidakLolosSeleksi";
            $arrPost = array();
			////$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            ///echopre($retVal);
            $retVal = $retVal[0]['intBukuTidakLolosSeleksi'];
            return $retVal;
        }

        public function getBukuSedangDiKonversi(){
            $sp_name = "BackOfficePublisher_DashboardBukuSedangDiKonversi";
            $arrPost = array();
			///$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            ///echopre($retVal);
            $retVal = $retVal[0]['intBukuSedangDiKonversi'];
            return $retVal;
        }

        public function getBukuSiapDiKonversi(){
            $sp_name = "BackOfficePublisher_DashboardBukuSiapDiKonversi";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiKonversi'];
            //echopre($retVal);
            return $retVal;
        }

        public function getBukuSiapDiBeli(){
            $sp_name = "BackOfficePublisher_DashboardBukuSiapDiBeli";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiBeli'];
            ///echopre($retVal);
            return $retVal;
        }

        public function getInvoiceOnProcess(){
            $sp_name = "BackOfficePublisher_DashboardInvoiceDalamProses";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intInvoiceDalamProses'];
            return $retVal;
        }

        public function getPaidInvoice(){
            $sp_name = "BackOfficePublisher_DashboardInvoiceTerbayarkan";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intInvoiceTerbayarkan'];
            
            return $retVal;
        }

        public function getTotalJoinedPublisher(){
            $sp_name = "BackOfficePublisher_DashboardPenerbitBergabung";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intPublisherCount'];
            //echopre($retVal);
            return $retVal;
        }
    }