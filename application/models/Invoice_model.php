<?php

    class Invoice_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getListInvoice($intPublisherID,$intStatusID){
            $sp_name = "BackofficePublisher_PublisherInvoiceList";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intStatus'] = $intStatusID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailInvoice($intPublisherID, $intYear, $intMonth){
            $sp_name = "BackofficePublisher_PublisherInvoiceDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateInvoice($intPublisherID, $intYear, $intMonth){
            $sp_name = "BackofficePublisher_UpdateStatusInvoiceTerbayarkan";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getInvoicePurchaseDetail($intPublisherID, $intYear, $intMonth){
            $sp_name = "BackofficePublisher_InvoicePurchaseDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

	} 
