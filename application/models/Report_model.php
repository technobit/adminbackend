<?php

    class Report_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getRecapStatusBuku($intPublisherID){
            $sp_name = "BackofficePublisher_RptRecapStatusDataBuku";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailStatusBuku($intPublisherID , $intStatusID){
            $sp_name = "BackofficePublisher_RptDetailStatusDataBuku";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intStatusID'] = $intStatusID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getRekapPenjualanBuku($intPublisherID , $dtStart , $dtEnd){
            $sp_name = "BackofficePublisher_RptRecapBookSelling";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['dtStart'] = $dtStart;
            $arrPost['dtEnd'] = $dtEnd;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

     
	}


